package com.atrums.compras.montos.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class ATECCO_Identificacion_Orig extends HttpSecureAppServlet {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strEmCoTipoIdentificacion = vars.getStringParameter("inpemCoTipoIdentificacion");

      try {
        printPage(response, vars, strEmCoTipoIdentificacion);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      } catch (ParseException ex) {
        // TODO Auto-generated catch block
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strEmCoTipoIdentificacion) throws IOException, ServletException, ParseException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/compras/montos/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='ATECCO_Identificacion_Orig';\n\n");

    result.append("var respuesta = new Array(");

    if (strEmCoTipoIdentificacion != null) {
      result.append(
          "new Array(\"inpemAteccoTipoIdentificacion\", \"" + strEmCoTipoIdentificacion + "\")");
      result.append(");");

    } else {
      result.append("new Array(\"inpemAteccoTipoIdentificacion\", \"" + null + "\")");
      result.append(");");
    }

    // inject the generated code xmlDocument.setParameter("array",
    // result.toString());
    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}

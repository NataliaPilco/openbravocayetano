<?xml version="1.0" encoding="UTF-8" ?>
<!--
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2012 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
-->


<SqlClass name="ATECCOOrderProductData" package="com.atrums.compras.montos.ad_callouts">
  <SqlMethod name="select" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT C_ORDER_ID AS ID, DATEORDERED, C_BPARTNER_ID, M_PRICELIST_ID, 
        '' AS DATEINVOICED, 
        '' AS STOCKTIENDA, 
        '' AS STOCKCLIENTE, 
        '' AS PRECIOEFECTIVO, 
        '' AS TOTALEFECTIVO,
        '' AS PRECIOTARJETA, 
        '' AS TOTALTARJETA
        FROM C_ORDER WHERE C_ORDER_ID = ?
      ]]>
    </Sql>
    <Parameter name="cOrderId"/>
  </SqlMethod>
  <SqlMethod name="selectStockTienda" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      select sum(coalesce(msd.qtyonhand,0)) AS STOCKTIENDA 
        from m_storage_detail msd 
        inner join m_locator ml on (ml.m_locator_id=msd.m_locator_id)
        inner join m_warehouse mw on (mw.m_warehouse_id=ml.m_warehouse_id) 
		inner join ad_org_warehouse aow on (aow.m_warehouse_id = mw.m_warehouse_id) 
        where msd.m_product_id=? 
        and aow.ad_org_id IN (SELECT o.ad_org_id FROM c_order AS o WHERE o.c_order_id=?)
      ]]>
    </Sql>
    <Parameter name="mProductId"/>
    <Parameter name="cOrderId"/>
  </SqlMethod>
  <SqlMethod name="selectPrecioEfectivo" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT MAX(CASE WHEN (cor1.em_atecco_docstatus IN ('CO','--')) THEN 
        CASE WHEN (upper(fps1.name) IN (upper('Efectivo'),upper('Transferencia/Depósito'),upper('Cheque'))) THEN CAST(? AS NUMERIC) ELSE (CAST(? AS NUMERIC) / 1.05) END
         ELSE CAST(? AS NUMERIC) END) AS PRECIOEFECTIVO
        FROM c_order cor1 
        INNER JOIN fin_paymentmethod fps1 ON (cor1.fin_paymentmethod_id = fps1.fin_paymentmethod_id)
        WHERE cor1.c_order_id=?
      ]]>
    </Sql>
    <Parameter name="priceactual1"/>
    <Parameter name="priceactual2"/>
    <Parameter name="priceactual3"/>
    <Parameter name="cOrderId"/>
  </SqlMethod>
  <SqlMethod name="selectPrecioTarjeta" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT MAX(CASE WHEN (cor1.em_atecco_docstatus IN ('CO','--')) THEN 
        CASE WHEN (upper(fps1.name) IN (upper('Efectivo'),upper('Transferencia/Depósito'),upper('Cheque'))) THEN round((CAST(? AS NUMERIC) * 1.05), 2) ELSE CAST(? AS NUMERIC) END
         ELSE round((CAST(? AS NUMERIC) * 1.05), 2) END) AS PRECIOTARJETA 
        FROM c_order cor1 
        INNER JOIN fin_paymentmethod fps1 ON (cor1.fin_paymentmethod_id = fps1.fin_paymentmethod_id)
        WHERE cor1.c_order_id=?
      ]]>
    </Sql>
    <Parameter name="priceactual1"/>
    <Parameter name="priceactual2"/>
    <Parameter name="priceactual3"/>
    <Parameter name="cOrderId"/>
  </SqlMethod>
  <SqlMethod name="selectTotalEfectivo" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT MAX(CASE WHEN (cor1.em_atecco_docstatus IN ('CO','--')) THEN 
        CASE WHEN (upper(fps1.name) IN (upper('Efectivo'),upper('Transferencia/Depósito'),upper('Cheque'))) THEN (CAST(? AS NUMERIC) * CAST(? AS NUMERIC)) ELSE ((CAST(? AS NUMERIC) / 1.05) * CAST(? AS NUMERIC)) END
         ELSE (CAST(? AS NUMERIC) * CAST(? AS NUMERIC)) END) AS TOTALEFECTIVO 
        FROM c_order cor1 
        INNER JOIN fin_paymentmethod fps1 ON (cor1.fin_paymentmethod_id = fps1.fin_paymentmethod_id)
        WHERE cor1.c_order_id=?
      ]]>
    </Sql>
    <Parameter name="priceactual1"/>
    <Parameter name="qtyorder1"/>
    <Parameter name="priceactual2"/>
    <Parameter name="qtyorder2"/>
    <Parameter name="priceactual3"/>
    <Parameter name="qtyorder3"/>
    <Parameter name="cOrderId"/>
  </SqlMethod>
  <SqlMethod name="selectTotalTarjeta" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT MAX(CASE WHEN (cor1.em_atecco_docstatus IN ('CO','--')) THEN 
        CASE WHEN (upper(fps1.name) IN (upper('Efectivo'),upper('Transferencia/Depósito'),upper('Cheque'))) THEN (round((CAST(? AS NUMERIC) * 1.05), 2) * CAST(? AS NUMERIC)) ELSE (CAST(? AS NUMERIC) * CAST(? AS NUMERIC)) END
         ELSE (round((CAST(? AS NUMERIC) * 1.05) , 2) * CAST(? AS NUMERIC)) END) AS TOTALTARJETA 
        FROM c_order cor1 
        INNER JOIN fin_paymentmethod fps1 ON (cor1.fin_paymentmethod_id = fps1.fin_paymentmethod_id)
        WHERE cor1.c_order_id=?
      ]]>
    </Sql>
    <Parameter name="priceactual1"/>
    <Parameter name="qtyorder1"/>
    <Parameter name="priceactual2"/>
    <Parameter name="qtyorder2"/>
    <Parameter name="priceactual3"/>
    <Parameter name="qtyorder3"/>
    <Parameter name="cOrderId"/>
  </SqlMethod>
  <SqlMethod name="selectStockCliente" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      select sum(coalesce(msd.qtyonhand,0)) AS STOCKCLIENTE 
        from m_storage_detail msd 
        inner join m_locator ml on (ml.m_locator_id=msd.m_locator_id)
        where msd.m_product_id=?
      ]]>
    </Sql>
    <Parameter name="mProductId"/>
  </SqlMethod>
  <SqlMethod name="selectInvoice" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT C_INVOICE_ID AS ID, DATEINVOICED, C_BPARTNER_ID, M_PRICELIST_ID
      	FROM C_INVOICE WHERE C_INVOICE_ID = ?
      ]]>
    </Sql>
    <Parameter name="cInvoiceId"/>
  </SqlMethod>
  <!--SqlMethod name="strMProductUOMID" type="preparedStatement" return="String" default="">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      SELECT M_PRODUCT_UOM_ID FROM M_PRODUCT_UOM WHERE M_PRODUCT_ID = ? AND C_UOM_ID = ?
    </Sql>
    <Parameter name="mProductId"/>
    <Parameter name="cUOMId"/>
  </SqlMethod-->
  <SqlMethod name="hasSecondaryUOM" type="preparedStatement" return="String" default="0">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      SELECT (CASE COUNT(*) WHEN 0 THEN 0 ELSE 1 END) AS TOTAL 
      FROM M_PRODUCT_UOM 
      WHERE M_PRODUCT_ID = ?
      AND ISACTIVE = 'Y'
    </Sql>
    <Parameter name="mProductId"/>
  </SqlMethod>

  <SqlMethod name="getOrgLocationId" type="preparedStatement" return="String" default="0">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
	  	SELECT C_LOCATION_ID 
	  		FROM AD_ORGINFO 
	  		WHERE AD_Client_ID IN ('1') 
	  		AND AD_Org_ID IN ('1')
	 </Sql>
     <Parameter name="adUserClient" type="replace" optional="true" after="AD_Client_ID IN (" text="'1'"/>
     <Parameter name="adUserOrg" type="replace" optional="true" after="AD_Org_ID IN (" text="'1'"/>
  </SqlMethod>

  <SqlMethod name="getWarehouseOrg" type="preparedStatement" return="String">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
	  SELECT AD_ORG_ID
      FROM M_WAREHOUSE
      WHERE M_WAREHOUSE_ID = ?
	 </Sql>
    <Parameter name="cWarehouseID"/>
  </SqlMethod>

  <SqlMethod name="getWarehouseOfOrg" type="preparedStatement" return="String">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
        select min(w.m_warehouse_id)
        from m_warehouse w
        where w.ad_client_id=?
          and (ad_isorgincluded(?,w.ad_org_id,?)<>-1
            or ad_isorgincluded(w.ad_org_id,?,?)<>-1)
      ]]>
    </Sql>
    <Parameter name="adClientId"/>
    <Parameter name="adOrgId"/>
    <Parameter name="adClientId"/>
    <Parameter name="adOrgId"/>
    <Parameter name="adClientId"/>
  </SqlMethod>

</SqlClass>

package com.atrums.compras.montos.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

import com.atrums.compras.montos.data.ateccoCierrecaja;
import com.atrums.compras.montos.model.ATECCO_Temporal;

public class ATECCO_Retiro extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  boolean continuar = true;
  String auxPagos = null;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strTab = vars.getStringParameter("inpTabId");
      String strKey = vars.getGlobalVariable("inpateccoCierrecajaId",
          strWindow + "|Atecco_Cierrecaja_ID");
      printPage(response, vars, strKey, strWindow, strTab, strProcessId);
    } else if (vars.commandIn("SAVE")) {
      String strWindow = vars.getStringParameter("inpWindowId");
      String strCierreCajaId = vars.getStringParameter("inpateccoCierrecajaId");
      String strKey = vars.getRequestGlobalVariable("inpKey", strWindow + "|Atecco_Cierrecaja_ID");
      String strTab = vars.getStringParameter("inpTabId");

      List<ATECCO_Temporal> temp = new ArrayList<ATECCO_Temporal>();

      String strPagoEfectivo = vars.getStringParameter("inpMontoPagoEfectivo");

      if (strPagoEfectivo != null) {
        if (Double.parseDouble(strPagoEfectivo.equals("") ? "0" : strPagoEfectivo) > 0) {
          ATECCO_Temporal newTemp = new ATECCO_Temporal();
          newTemp.setAmount(Double.parseDouble(strPagoEfectivo));
          newTemp.setTipoPago("Efectivo".toUpperCase());
          temp.add(newTemp);
        }
      }

      String strPagoElectronico = vars.getStringParameter("inpMontoPagoElectronico");

      if (strPagoElectronico != null) {
        if (Double.parseDouble(strPagoElectronico.equals("") ? "0" : strPagoElectronico) > 0) {
          ATECCO_Temporal newTemp = new ATECCO_Temporal();
          newTemp.setAmount(Double.parseDouble(strPagoElectronico));
          newTemp.setTipoPago("Dinero Electrónico".toUpperCase());
          temp.add(newTemp);
        }
      }

      String strPagoCheque = vars.getStringParameter("inpMontoPagoCheque");

      if (strPagoCheque != null) {
        if (Double.parseDouble(strPagoCheque.equals("") ? "0" : strPagoCheque) > 0) {
          ATECCO_Temporal newTemp = new ATECCO_Temporal();
          newTemp.setAmount(Double.parseDouble(strPagoCheque));
          newTemp.setTipoPago("Cheque".toUpperCase());
          temp.add(newTemp);
        }
      }

      String strPagoTransferencia = vars.getStringParameter("inpMontoPagoTransferencia");

      if (strPagoTransferencia != null) {
        if (Double.parseDouble(strPagoTransferencia.equals("") ? "0" : strPagoTransferencia) > 0) {
          ATECCO_Temporal newTemp = new ATECCO_Temporal();
          newTemp.setAmount(Double.parseDouble(strPagoTransferencia));
          newTemp.setTipoPago("Transferencia/Depósito".toUpperCase());
          temp.add(newTemp);
        }
      }

      String strPagoTarjeta = vars.getStringParameter("inpMontoPagoTarjeta");

      if (strPagoTarjeta != null) {
        if (Double.parseDouble(strPagoTarjeta.equals("") ? "0" : strPagoTarjeta) > 0) {
          ATECCO_Temporal newTemp = new ATECCO_Temporal();
          newTemp.setAmount(Double.parseDouble(strPagoTarjeta));
          newTemp.setTipoPago("Tarjeta de Crédito".toUpperCase());
          temp.add(newTemp);
        }
      }

      String strPagoRetencion = vars.getStringParameter("inpMontoPagoRetencion");

      if (strPagoTarjeta != null) {
        if (Double.parseDouble(strPagoRetencion.equals("") ? "0" : strPagoRetencion) > 0) {
          ATECCO_Temporal newTemp = new ATECCO_Temporal();
          newTemp.setAmount(Double.parseDouble(strPagoRetencion));
          newTemp.setTipoPago("Con Retención".toUpperCase());
          temp.add(newTemp);
        }
      }

      String strWindowPath = Utility.getTabURL(strTab, "R", true);
      if (strWindowPath.equals("")) {
        strWindowPath = strDefaultServlet;
      }

      OBError myError = processButton(vars, strKey, strCierreCajaId, temp);
      log4j.debug(myError.getMessage());
      vars.setMessage(strTab, myError);
      printPageClosePopUp(response, vars, strWindowPath);
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId) throws IOException, ServletException {

    try {
      XmlDocument xmlDocument = xmlEngine
          .readXmlTemplate("com/atrums/compras/montos/process/ATECCO_Retiro").createXmlDocument();
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("theme", vars.getTheme());
      xmlDocument.setParameter("key", strKey);
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("tab", strTab);

      xmlDocument.setParameter("montoPagoEfectivo", "0.00");
      xmlDocument.setParameter("montoPagoElectronico", "0.00");
      xmlDocument.setParameter("montoPagoCheque", "0.00");
      xmlDocument.setParameter("montoPagoTransferencia", "0.00");
      xmlDocument.setParameter("montoPagoTarjeta", "0.00");
      xmlDocument.setParameter("montoPagoRetencion", "0.00");

      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception ex) {
      // TODO: handle exception
      throw new ServletException(ex);
    }
  }

  private OBError processButton(VariablesSecureApp vars, String strKey, String strCierreCajaId,
      List<ATECCO_Temporal> temp) {
    OBError myError = null;
    Connection conn = null;
    String sqlQuery = null;

    OBContext.setAdminMode(true);
    try {
      ateccoCierrecaja cierrecaja = OBDal.getInstance().get(ateccoCierrecaja.class,
          strCierreCajaId);

      PreparedStatement ps = null;
      ResultSet rs = null;

      conn = OBDal.getInstance().getConnection();

      for (int i = 0; i < temp.size(); i++) {

        String auxFinMethoId = "";

        sqlQuery = "SELECT fin_paymentmethod_id " + "FROM fin_paymentmethod "
            + "WHERE upper(name) LIKE '" + temp.get(i).getTipoPago() + "' "
            + "AND isactive = 'Y' AND ad_org_id = '0';";

        ps = conn.prepareStatement(sqlQuery);
        rs = ps.executeQuery();

        while (rs.next()) {
          auxFinMethoId = rs.getString("fin_paymentmethod_id");
        }

        rs.close();
        ps.close();

        sqlQuery = "INSERT INTO atecco_retiro("
            + "atecco_retiro_id, atecco_cierrecaja_id, ad_client_id, ad_org_id, "
            + "isactive, created, createdby, updated, updatedby, hora, nro_deposito, "
            + "total_deposito, fin_paymentmethod_id, sales_id) " + "VALUES (get_uuid(), '"
            + strCierreCajaId + "', '" + cierrecaja.getClient().getId() + "', '"
            + cierrecaja.getOrganization().getId() + "', "
            + "'Y', now(), '100', now(), '100', now(), null, " + "'" + temp.get(i).getAmount()
            + "', '" + auxFinMethoId + "', null);";

        ps = conn.prepareStatement(sqlQuery);
        ps.executeUpdate();

        ps.close();
      }

      OBDal.getInstance().commitAndClose();

      myError = new OBError();
      myError.setType("Success");
      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
      myError.setMessage("Retiro Procesado");
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();

      e.printStackTrace();
      log4j.warn("Rollback in transaction");
      myError = new OBError();
      myError.setType("Error");
      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }

    return myError;
  }
}

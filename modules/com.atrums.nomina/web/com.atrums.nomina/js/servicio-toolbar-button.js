OB.NO = OB.NO || {};

OB.NO.Process = {
	execute: function (params, view) {
		//alert('You clicked InCorrecto');
	var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
		servicioLinea = [], messageBar = view.getView(params.adTabId).messageBar, 
		callback;
		
		callback = function (rpcResponse, data, rpcRequest) {
			var status = rpcResponse.status,
			view = rpcRequest.clientContext.view.getView(params.adTabId);
			view.messageBar.setMessage(data.message.severity, null, data.message.text);
			
			params.button.closeProcessPopup();
		};
		
		for (i = 0; i < selection.length; i++) {
		    servicioLinea.push(selection[i].id);
		};
		
		OB.RemoteCallManager.call('com.atrums.nomina.ad_process.PagoServicioEnvioMailHandler', {
		    servicioLinea: servicioLinea,
		    action: params.action
		}, {}, callback, {
      view: view
    });
	},
	
	servicio: function (params, view) {
		params.action = 'servicio';
		params.adTabId = 'C018CCAD1E4F435EB18BF1CCA45604CC';		
		OB.NO.Process.execute(params, view);
	}
};

package org.openbravo.erpWindows.com.atums.decoraciones.PedidoCliente;


import org.openbravo.erpCommon.reference.*;


import org.openbravo.erpCommon.ad_actionButton.*;


import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;

// Generated old code, not worth to make i.e. java imports perfect
@SuppressWarnings("unused")
public class PedidoCliente294F4FDD3CB849138A093832B628F92F extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static final String windowId = "C1176D728CF145319CA757A153305ED1";
  private static final String tabId = "294F4FDD3CB849138A093832B628F92F";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 1;
  private static final String moduleId = "5D88833B8BAF49B8AF43746B446481C7";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
     List<String> explicitAccess = Arrays.asList( "");
    
     SessionInfo.setUserId(vars.getSessionValue("#AD_User_ID"));
     SessionInfo.setSessionId(vars.getSessionValue("#AD_Session_ID"));
     SessionInfo.setQueryProfile("manualProcess");
     
      if (command.contains("FF80808133362F6A013336781FCE0066")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("FF80808133362F6A013336781FCE0066");
        SessionInfo.setModuleId("0");
      }
     
      if (command.contains("23D1B163EC0B41F790CE39BF01DA320E")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("23D1B163EC0B41F790CE39BF01DA320E");
        SessionInfo.setModuleId("0");
      }
     
      if (command.contains("9EB2228A60684C0DBEC12D5CD8D85218")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("9EB2228A60684C0DBEC12D5CD8D85218");
        SessionInfo.setModuleId("0");
      }
     
      if (command.contains("A3FE1F9892394386A49FB707AA50A0FA")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("A3FE1F9892394386A49FB707AA50A0FA");
        SessionInfo.setModuleId("0");
      }
     
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
     
      if (command.contains("800022")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("800022");
        SessionInfo.setModuleId("0");
        if (securedProcess || explicitAccess.contains("800022")) {
          classInfo.type = "P";
          classInfo.id = "800022";
        }
      }
     

     
      if (explicitAccess.contains("FF80808133362F6A013336781FCE0066") || (securedProcess && command.contains("FF80808133362F6A013336781FCE0066"))) {
        classInfo.type = "P";
        classInfo.id = "FF80808133362F6A013336781FCE0066";
      }
     
      if (explicitAccess.contains("23D1B163EC0B41F790CE39BF01DA320E") || (securedProcess && command.contains("23D1B163EC0B41F790CE39BF01DA320E"))) {
        classInfo.type = "P";
        classInfo.id = "23D1B163EC0B41F790CE39BF01DA320E";
      }
     
      if (explicitAccess.contains("9EB2228A60684C0DBEC12D5CD8D85218") || (securedProcess && command.contains("9EB2228A60684C0DBEC12D5CD8D85218"))) {
        classInfo.type = "P";
        classInfo.id = "9EB2228A60684C0DBEC12D5CD8D85218";
      }
     
      if (explicitAccess.contains("A3FE1F9892394386A49FB707AA50A0FA") || (securedProcess && command.contains("A3FE1F9892394386A49FB707AA50A0FA"))) {
        classInfo.type = "P";
        classInfo.id = "A3FE1F9892394386A49FB707AA50A0FA";
      }
     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strcOrderId = request.getParameter("inpcOrderId");
        
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strcOrderId.equals(""))
              total = saveRecord(vars, myError, 'U');
          else
              total = saveRecord(vars, myError, 'I');
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {

      String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
      

      String strView = vars.getSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strC_Order_ID.equals("")) strC_Order_ID = firstElement(vars, tableSQL);
          if (strC_Order_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

      else printPageDataSheet(response, vars, strC_Order_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strC_Order_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strC_Order_ID.equals("")) strC_Order_ID = vars.getRequiredGlobalVariable("inpcOrderId", windowId + "|C_Order_ID");
      else vars.setSessionValue(windowId + "|C_Order_ID", strC_Order_ID);
      
      vars.setSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.view", "EDIT");

      printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {


      String strView = vars.getSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.view");
      String strC_Order_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strC_Order_ID = firstElement(vars, tableSQL);
          if (strC_Order_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strC_Order_ID.equals("")) strC_Order_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

      } else printPageDataSheet(response, vars, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamAD_Org_ID", tabId + "|paramAD_Org_ID");
vars.getRequestGlobalVariable("inpParamDocumentNo", tabId + "|paramDocumentNo");
vars.getRequestGlobalVariable("inpParamPOReference", tabId + "|paramPOReference");
vars.getRequestGlobalVariable("inpParamDateOrdered", tabId + "|paramDateOrdered");
vars.getRequestGlobalVariable("inpParamC_DocTypeTarget_ID", tabId + "|paramC_DocTypeTarget_ID");
vars.getRequestGlobalVariable("inpParamC_BPartner_ID", tabId + "|paramC_BPartner_ID");
vars.getRequestGlobalVariable("inpParamInvoiceRule", tabId + "|paramInvoiceRule");
vars.getRequestGlobalVariable("inpParamGrandTotal", tabId + "|paramGrandTotal");
vars.getRequestGlobalVariable("inpParamDocStatus", tabId + "|paramDocStatus");
vars.getRequestGlobalVariable("inpParamDateOrdered_f", tabId + "|paramDateOrdered_f");
vars.getRequestGlobalVariable("inpParamGrandTotal_f", tabId + "|paramGrandTotal_f");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
      
      
      vars.removeSessionValue(windowId + "|C_Order_ID");
      String strC_Order_ID="";

      String strView = vars.getSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strC_Order_ID = firstElement(vars, tableSQL);
        if (strC_Order_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

      else printPageDataSheet(response, vars, strC_Order_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
      

      String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
      vars.setSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.view", "RELATION");
      printPageDataSheet(response, vars, strC_Order_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {


      printPageEdit(response, request, vars, true, "", tableSQL);

    } else if (vars.commandIn("EDIT")) {

      String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
      vars.setSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {

      String strC_Order_ID = vars.getRequiredStringParameter("inpcOrderId");
      
      String strNext = nextElement(vars, strC_Order_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {

      String strC_Order_ID = vars.getRequiredStringParameter("inpcOrderId");
      
      String strPrevious = previousElement(vars, strC_Order_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {

      vars.setSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|C_Order_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|C_Order_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {

      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {

      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {

      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I');      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {

      String strC_Order_ID = vars.getRequiredGlobalVariable("inpcOrderId", windowId + "|C_Order_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U');      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strC_Order_ID, tableSQL);
          vars.setSessionValue(windowId + "|C_Order_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {

      String strC_Order_ID = vars.getRequiredStringParameter("inpcOrderId");
      //PedidoCliente294F4FDD3CB849138A093832B628F92FData data = getEditVariables(vars);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = PedidoCliente294F4FDD3CB849138A093832B628F92FData.delete(this, strC_Order_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|cOrderId");
        vars.setSessionValue(tabId + "|PedidoCliente294F4FDD3CB849138A093832B628F92F.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONGeneratetemplate800022")) {
        vars.setSessionValue("button800022.strgeneratetemplate", vars.getStringParameter("inpgeneratetemplate"));
        vars.setSessionValue("button800022.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button800022.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button800022.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button800022.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "800022", request.getServletPath());    
     } else if (vars.commandIn("BUTTON800022")) {
        String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
        String strgeneratetemplate = vars.getSessionValue("button800022.strgeneratetemplate");
        String strProcessing = vars.getSessionValue("button800022.strProcessing");
        String strOrg = vars.getSessionValue("button800022.strOrg");
        String strClient = vars.getSessionValue("button800022.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonGeneratetemplate800022(response, vars, strC_Order_ID, strgeneratetemplate, strProcessing);
        }

    } else if (vars.commandIn("BUTTONRM_CreateInvoiceFF80808133362F6A013336781FCE0066")) {
        vars.setSessionValue("buttonFF80808133362F6A013336781FCE0066.strrmCreateinvoice", vars.getStringParameter("inprmCreateinvoice"));
        vars.setSessionValue("buttonFF80808133362F6A013336781FCE0066.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonFF80808133362F6A013336781FCE0066.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonFF80808133362F6A013336781FCE0066.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonFF80808133362F6A013336781FCE0066.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "FF80808133362F6A013336781FCE0066", request.getServletPath());
      } else if (vars.commandIn("BUTTONFF80808133362F6A013336781FCE0066")) {
        String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
        String strrmCreateinvoice = vars.getSessionValue("buttonFF80808133362F6A013336781FCE0066.strrmCreateinvoice");
        String strProcessing = vars.getSessionValue("buttonFF80808133362F6A013336781FCE0066.strProcessing");
        String strOrg = vars.getSessionValue("buttonFF80808133362F6A013336781FCE0066.strOrg");
        String strClient = vars.getSessionValue("buttonFF80808133362F6A013336781FCE0066.strClient");

        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonRM_CreateInvoiceFF80808133362F6A013336781FCE0066(response, vars, strC_Order_ID, strrmCreateinvoice, strProcessing);
        }
    } else if (vars.commandIn("BUTTONRM_AddOrphanLine23D1B163EC0B41F790CE39BF01DA320E")) {
        vars.setSessionValue("button23D1B163EC0B41F790CE39BF01DA320E.strrmAddorphanline", vars.getStringParameter("inprmAddorphanline"));
        vars.setSessionValue("button23D1B163EC0B41F790CE39BF01DA320E.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button23D1B163EC0B41F790CE39BF01DA320E.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button23D1B163EC0B41F790CE39BF01DA320E.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        p.put("IsSOTrx", vars.getStringParameter("inpissotrx"));

        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button23D1B163EC0B41F790CE39BF01DA320E.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "23D1B163EC0B41F790CE39BF01DA320E", request.getServletPath());
      } else if (vars.commandIn("BUTTON23D1B163EC0B41F790CE39BF01DA320E")) {
        String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
        String strrmAddorphanline = vars.getSessionValue("button23D1B163EC0B41F790CE39BF01DA320E.strrmAddorphanline");
        String strProcessing = vars.getSessionValue("button23D1B163EC0B41F790CE39BF01DA320E.strProcessing");
        String strOrg = vars.getSessionValue("button23D1B163EC0B41F790CE39BF01DA320E.strOrg");
        String strClient = vars.getSessionValue("button23D1B163EC0B41F790CE39BF01DA320E.strClient");

        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonRM_AddOrphanLine23D1B163EC0B41F790CE39BF01DA320E(response, vars, strC_Order_ID, strrmAddorphanline, strProcessing);
        }
    } else if (vars.commandIn("BUTTONCalculate_Promotions9EB2228A60684C0DBEC12D5CD8D85218")) {
        vars.setSessionValue("button9EB2228A60684C0DBEC12D5CD8D85218.strcalculatePromotions", vars.getStringParameter("inpcalculatePromotions"));
        vars.setSessionValue("button9EB2228A60684C0DBEC12D5CD8D85218.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button9EB2228A60684C0DBEC12D5CD8D85218.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button9EB2228A60684C0DBEC12D5CD8D85218.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button9EB2228A60684C0DBEC12D5CD8D85218.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "9EB2228A60684C0DBEC12D5CD8D85218", request.getServletPath());
      } else if (vars.commandIn("BUTTON9EB2228A60684C0DBEC12D5CD8D85218")) {
        String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
        String strcalculatePromotions = vars.getSessionValue("button9EB2228A60684C0DBEC12D5CD8D85218.strcalculatePromotions");
        String strProcessing = vars.getSessionValue("button9EB2228A60684C0DBEC12D5CD8D85218.strProcessing");
        String strOrg = vars.getSessionValue("button9EB2228A60684C0DBEC12D5CD8D85218.strOrg");
        String strClient = vars.getSessionValue("button9EB2228A60684C0DBEC12D5CD8D85218.strClient");

        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonCalculate_Promotions9EB2228A60684C0DBEC12D5CD8D85218(response, vars, strC_Order_ID, strcalculatePromotions, strProcessing);
        }
    } else if (vars.commandIn("BUTTONConvertquotationA3FE1F9892394386A49FB707AA50A0FA")) {
        vars.setSessionValue("buttonA3FE1F9892394386A49FB707AA50A0FA.strconvertquotation", vars.getStringParameter("inpconvertquotation"));
        vars.setSessionValue("buttonA3FE1F9892394386A49FB707AA50A0FA.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonA3FE1F9892394386A49FB707AA50A0FA.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonA3FE1F9892394386A49FB707AA50A0FA.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonA3FE1F9892394386A49FB707AA50A0FA.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "A3FE1F9892394386A49FB707AA50A0FA", request.getServletPath());
      } else if (vars.commandIn("BUTTONA3FE1F9892394386A49FB707AA50A0FA")) {
        String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
        String strconvertquotation = vars.getSessionValue("buttonA3FE1F9892394386A49FB707AA50A0FA.strconvertquotation");
        String strProcessing = vars.getSessionValue("buttonA3FE1F9892394386A49FB707AA50A0FA.strProcessing");
        String strOrg = vars.getSessionValue("buttonA3FE1F9892394386A49FB707AA50A0FA.strOrg");
        String strClient = vars.getSessionValue("buttonA3FE1F9892394386A49FB707AA50A0FA.strClient");

        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonConvertquotationA3FE1F9892394386A49FB707AA50A0FA(response, vars, strC_Order_ID, strconvertquotation, strProcessing);
        }

    } else if (vars.commandIn("SAVE_BUTTONGeneratetemplate800022")) {
        String strC_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|C_Order_ID", "");
        String strgeneratetemplate = vars.getStringParameter("inpgeneratetemplate");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "800022", (("C_Order_ID".equalsIgnoreCase("AD_Language"))?"0":strC_Order_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);

    } else if (vars.commandIn("SAVE_BUTTONRM_CreateInvoiceFF80808133362F6A013336781FCE0066")) {
        String strC_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|C_Order_ID", "");
        
        ProcessBundle pb = new ProcessBundle("FF80808133362F6A013336781FCE0066", vars).init(this);
        HashMap<String, Object> params= new HashMap<String, Object>();
       
        params.put("C_Order_ID", strC_Order_ID);
        params.put("adOrgId", vars.getStringParameter("inpadOrgId"));
        params.put("adClientId", vars.getStringParameter("inpadClientId"));
        params.put("tabId", tabId);
        
        
        
        pb.setParams(params);
        OBError myMessage = null;
        try {
          new ProcessRunner(pb).execute(this);
          myMessage = (OBError) pb.getResult();
          myMessage.setMessage(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getMessage()));
          myMessage.setTitle(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getTitle()));
        } catch (Exception ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          log4j.error(ex);
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONRM_AddOrphanLine23D1B163EC0B41F790CE39BF01DA320E")) {
        String strC_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|C_Order_ID", "");
        
        ProcessBundle pb = new ProcessBundle("23D1B163EC0B41F790CE39BF01DA320E", vars).init(this);
        HashMap<String, Object> params= new HashMap<String, Object>();
       
        params.put("C_Order_ID", strC_Order_ID);
        params.put("adOrgId", vars.getStringParameter("inpadOrgId"));
        params.put("adClientId", vars.getStringParameter("inpadClientId"));
        params.put("tabId", tabId);
        
        String strmProductId = vars.getStringParameter("inpmProductId");
params.put("mProductId", strmProductId);
String strmAttributesetinstanceId = vars.getStringParameter("inpmAttributesetinstanceId");
params.put("mAttributesetinstanceId", strmAttributesetinstanceId);
String strreturned = vars.getNumericParameter("inpreturned");
params.put("returned", strreturned);
String strpricestd = vars.getNumericParameter("inppricestd");
params.put("pricestd", strpricestd);
String strcTaxId = vars.getStringParameter("inpcTaxId");
params.put("cTaxId", strcTaxId);
String strcReturnReasonId = vars.getStringParameter("inpcReturnReasonId");
params.put("cReturnReasonId", strcReturnReasonId);

        
        pb.setParams(params);
        OBError myMessage = null;
        try {
          new ProcessRunner(pb).execute(this);
          myMessage = (OBError) pb.getResult();
          myMessage.setMessage(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getMessage()));
          myMessage.setTitle(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getTitle()));
        } catch (Exception ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          log4j.error(ex);
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONCalculate_Promotions9EB2228A60684C0DBEC12D5CD8D85218")) {
        String strC_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|C_Order_ID", "");
        
        ProcessBundle pb = new ProcessBundle("9EB2228A60684C0DBEC12D5CD8D85218", vars).init(this);
        HashMap<String, Object> params= new HashMap<String, Object>();
       
        params.put("C_Order_ID", strC_Order_ID);
        params.put("adOrgId", vars.getStringParameter("inpadOrgId"));
        params.put("adClientId", vars.getStringParameter("inpadClientId"));
        params.put("tabId", tabId);
        
        
        
        pb.setParams(params);
        OBError myMessage = null;
        try {
          new ProcessRunner(pb).execute(this);
          myMessage = (OBError) pb.getResult();
          myMessage.setMessage(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getMessage()));
          myMessage.setTitle(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getTitle()));
        } catch (Exception ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          log4j.error(ex);
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONConvertquotationA3FE1F9892394386A49FB707AA50A0FA")) {
        String strC_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|C_Order_ID", "");
        
        ProcessBundle pb = new ProcessBundle("A3FE1F9892394386A49FB707AA50A0FA", vars).init(this);
        HashMap<String, Object> params= new HashMap<String, Object>();
       
        params.put("C_Order_ID", strC_Order_ID);
        params.put("adOrgId", vars.getStringParameter("inpadOrgId"));
        params.put("adClientId", vars.getStringParameter("inpadClientId"));
        params.put("tabId", tabId);
        
        String strrecalculateprices = vars.getStringParameter("inprecalculateprices", "N");
params.put("recalculateprices", strrecalculateprices);

        
        pb.setParams(params);
        OBError myMessage = null;
        try {
          new ProcessRunner(pb).execute(this);
          myMessage = (OBError) pb.getResult();
          myMessage.setMessage(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getMessage()));
          myMessage.setTitle(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getTitle()));
        } catch (Exception ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          log4j.error(ex);
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);


    } else if (vars.commandIn("BUTTONPosted")) {
        String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
        String strTableId = "259";
        String strPosted = vars.getStringParameter("inpposted");
        String strProcessId = "";
        log4j.debug("Loading Posted button in table: " + strTableId);
        String strOrg = vars.getStringParameter("inpadOrgId");
        String strClient = vars.getStringParameter("inpadClientId");
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{
          vars.setSessionValue("Posted|key", strC_Order_ID);
          vars.setSessionValue("Posted|tableId", strTableId);
          vars.setSessionValue("Posted|tabId", tabId);
          vars.setSessionValue("Posted|posted", strPosted);
          vars.setSessionValue("Posted|processId", strProcessId);
          vars.setSessionValue("Posted|path", strDireccion + request.getServletPath());
          vars.setSessionValue("Posted|windowId", windowId);
          vars.setSessionValue("Posted|tabName", "PedidoCliente294F4FDD3CB849138A093832B628F92F");
          response.sendRedirect(strDireccion + "/ad_actionButton/Posted.html");
        }



    } else if (vars.commandIn("SAVE_XHR")) {
      
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private PedidoCliente294F4FDD3CB849138A093832B628F92FData getEditVariables(Connection con, VariablesSecureApp vars) throws IOException,ServletException {
    PedidoCliente294F4FDD3CB849138A093832B628F92FData data = new PedidoCliente294F4FDD3CB849138A093832B628F92FData();
    ServletException ex = null;
    try {
    data.cBpartnerId = vars.getRequiredGlobalVariable("inpcBpartnerId", windowId + "|C_BPartner_ID");     data.cBpartnerIdr = vars.getStringParameter("inpcBpartnerId_R");     data.dateordered = vars.getRequiredGlobalVariable("inpdateordered", windowId + "|DateOrdered");     data.emDecTipoEvento = vars.getRequiredGlobalVariable("inpemDecTipoEvento", windowId + "|em_dec_tipo_evento");     data.emDecTipoEventor = vars.getStringParameter("inpemDecTipoEvento_R");     data.cBpartnerLocationId = vars.getRequiredGlobalVariable("inpcBpartnerLocationId", windowId + "|C_BPartner_Location_ID");     data.cBpartnerLocationIdr = vars.getStringParameter("inpcBpartnerLocationId_R");     data.description = vars.getStringParameter("inpdescription");     data.datepromised = vars.getRequiredGlobalVariable("inpdatepromised", windowId + "|DatePromised");     data.emDecTipoContratacion = vars.getRequiredStringParameter("inpemDecTipoContratacion");     data.emDecTipoContratacionr = vars.getStringParameter("inpemDecTipoContratacion_R");     data.emDecFechaMontaje = vars.getRequiredStringParameter("inpemDecFechaMontaje");     data.emDecHoraMontaje = vars.getRequiredStringParameter("inpemDecHoraMontaje");     data.emDecFechaDesmontaje = vars.getRequiredStringParameter("inpemDecFechaDesmontaje");     data.emDecHoraDesmontaje = vars.getRequiredStringParameter("inpemDecHoraDesmontaje");     data.emDecRequiereTransporte = vars.getStringParameter("inpemDecRequiereTransporte", "N");     data.emDecRequiereMontaje = vars.getStringParameter("inpemDecRequiereMontaje", "N");     data.salesrepId = vars.getRequiredStringParameter("inpsalesrepId");     data.salesrepIdr = vars.getStringParameter("inpsalesrepId_R");     data.dateacct = vars.getRequiredStringParameter("inpdateacct");     data.billtoId = vars.getRequiredStringParameter("inpbilltoId");     data.billtoIdr = vars.getStringParameter("inpbilltoId_R");     data.poreference = vars.getStringParameter("inpporeference");     data.isdiscountprinted = vars.getStringParameter("inpisdiscountprinted", "N");     data.cCurrencyId = vars.getRequiredGlobalVariable("inpcCurrencyId", windowId + "|C_Currency_ID");     data.cCurrencyIdr = vars.getStringParameter("inpcCurrencyId_R");     data.paymentrule = vars.getRequiredStringParameter("inppaymentrule");     data.paymentruler = vars.getStringParameter("inppaymentrule_R");     data.cPaymenttermId = vars.getRequiredStringParameter("inpcPaymenttermId");     data.cPaymenttermIdr = vars.getStringParameter("inpcPaymenttermId_R");     data.invoicerule = vars.getRequiredStringParameter("inpinvoicerule");     data.invoiceruler = vars.getStringParameter("inpinvoicerule_R");     data.deliveryrule = vars.getRequiredStringParameter("inpdeliveryrule");     data.deliveryruler = vars.getStringParameter("inpdeliveryrule_R");     data.freightcostrule = vars.getRequiredGlobalVariable("inpfreightcostrule", windowId + "|FreightCostRule");     data.freightcostruler = vars.getStringParameter("inpfreightcostrule_R");    try {   data.freightamt = vars.getRequiredNumericParameter("inpfreightamt");  } catch (ServletException paramEx) { ex = paramEx; }     data.deliveryviarule = vars.getRequiredGlobalVariable("inpdeliveryviarule", windowId + "|DeliveryViaRule");     data.deliveryviaruler = vars.getStringParameter("inpdeliveryviarule_R");     data.mShipperId = vars.getRequestGlobalVariable("inpmShipperId", windowId + "|M_Shipper_ID");     data.mShipperIdr = vars.getStringParameter("inpmShipperId_R");     data.cChargeId = vars.getStringParameter("inpcChargeId");     data.cChargeIdr = vars.getStringParameter("inpcChargeId_R");    try {   data.chargeamt = vars.getNumericParameter("inpchargeamt");  } catch (ServletException paramEx) { ex = paramEx; }     data.priorityrule = vars.getRequiredStringParameter("inppriorityrule");     data.priorityruler = vars.getStringParameter("inppriorityrule_R");    try {   data.totallines = vars.getRequiredNumericParameter("inptotallines");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.grandtotal = vars.getRequiredNumericParameter("inpgrandtotal");  } catch (ServletException paramEx) { ex = paramEx; }     data.mWarehouseId = vars.getRequiredGlobalVariable("inpmWarehouseId", windowId + "|M_Warehouse_ID");     data.mPricelistId = vars.getRequiredGlobalVariable("inpmPricelistId", windowId + "|M_PriceList_ID");     data.mPricelistIdr = vars.getStringParameter("inpmPricelistId_R");     data.istaxincluded = vars.getStringParameter("inpistaxincluded", "N");     data.cCampaignId = vars.getStringParameter("inpcCampaignId");     data.cCampaignIdr = vars.getStringParameter("inpcCampaignId_R");     data.cProjectId = vars.getStringParameter("inpcProjectId");     data.cProjectIdr = vars.getStringParameter("inpcProjectId_R");     data.cActivityId = vars.getStringParameter("inpcActivityId");     data.cActivityIdr = vars.getStringParameter("inpcActivityId_R");     data.posted = vars.getRequiredGlobalVariable("inpposted", windowId + "|Posted");     data.adUserId = vars.getStringParameter("inpadUserId");     data.adUserIdr = vars.getStringParameter("inpadUserId_R");     data.copyfrom = vars.getStringParameter("inpcopyfrom");     data.dropshipBpartnerId = vars.getStringParameter("inpdropshipBpartnerId");     data.dropshipLocationId = vars.getStringParameter("inpdropshipLocationId");     data.dropshipLocationIdr = vars.getStringParameter("inpdropshipLocationId_R");     data.dropshipUserId = vars.getStringParameter("inpdropshipUserId");     data.dropshipUserIdr = vars.getStringParameter("inpdropshipUserId_R");     data.isselfservice = vars.getStringParameter("inpisselfservice", "N");     data.adOrgtrxId = vars.getStringParameter("inpadOrgtrxId");     data.adOrgtrxIdr = vars.getStringParameter("inpadOrgtrxId_R");     data.user1Id = vars.getStringParameter("inpuser1Id");     data.user2Id = vars.getStringParameter("inpuser2Id");     data.deliverynotes = vars.getStringParameter("inpdeliverynotes");     data.cIncotermsId = vars.getStringParameter("inpcIncotermsId");     data.cIncotermsIdr = vars.getStringParameter("inpcIncotermsId_R");     data.incotermsdescription = vars.getStringParameter("inpincotermsdescription");     data.generatetemplate = vars.getStringParameter("inpgeneratetemplate");     data.deliveryLocationId = vars.getStringParameter("inpdeliveryLocationId");     data.deliveryLocationIdr = vars.getStringParameter("inpdeliveryLocationId_R");     data.copyfrompo = vars.getStringParameter("inpcopyfrompo");     data.finPaymentmethodId = vars.getRequiredStringParameter("inpfinPaymentmethodId");     data.finPaymentmethodIdr = vars.getStringParameter("inpfinPaymentmethodId_R");     data.finPaymentPriorityId = vars.getStringParameter("inpfinPaymentPriorityId");     data.finPaymentPriorityIdr = vars.getStringParameter("inpfinPaymentPriorityId_R");     data.rmPickfromshipment = vars.getStringParameter("inprmPickfromshipment");     data.rmReceivematerials = vars.getStringParameter("inprmReceivematerials");     data.rmCreateinvoice = vars.getStringParameter("inprmCreateinvoice");     data.cReturnReasonId = vars.getStringParameter("inpcReturnReasonId");     data.cReturnReasonIdr = vars.getStringParameter("inpcReturnReasonId_R");     data.rmAddorphanline = vars.getStringParameter("inprmAddorphanline");     data.aAssetId = vars.getStringParameter("inpaAssetId");     data.calculatePromotions = vars.getStringParameter("inpcalculatePromotions");     data.cCostcenterId = vars.getStringParameter("inpcCostcenterId");     data.convertquotation = vars.getStringParameter("inpconvertquotation");     data.cRejectReasonId = vars.getStringParameter("inpcRejectReasonId");     data.cRejectReasonIdr = vars.getStringParameter("inpcRejectReasonId_R");     data.validuntil = vars.getStringParameter("inpvaliduntil");     data.quotationId = vars.getStringParameter("inpquotationId");     data.quotationIdr = vars.getStringParameter("inpquotationId_R");     data.soResStatus = vars.getStringParameter("inpsoResStatus");     data.soResStatusr = vars.getStringParameter("inpsoResStatus_R");     data.createPolines = vars.getStringParameter("inpcreatePolines");     data.iscashvat = vars.getStringParameter("inpiscashvat", "N");     data.rmPickfromreceipt = vars.getStringParameter("inprmPickfromreceipt");     data.emAprmAddpayment = vars.getStringParameter("inpemAprmAddpayment");     data.emAteccoFechaPago = vars.getStringParameter("inpemAteccoFechaPago");     data.emAteccoBorra1 = vars.getStringParameter("inpemAteccoBorra1");     data.emAteccoBorra2 = vars.getStringParameter("inpemAteccoBorra2");     data.emAteccoTransporte = vars.getStringParameter("inpemAteccoTransporte", "N");     data.emAteccoDocaction = vars.getRequiredStringParameter("inpemAteccoDocaction");     data.emAteccoImprimir = vars.getStringParameter("inpemAteccoImprimir");     data.emAteccoTipoTarjeta = vars.getStringParameter("inpemAteccoTipoTarjeta");     data.emAteccoTipoTarjetar = vars.getStringParameter("inpemAteccoTipoTarjeta_R");     data.emAteccoBanco = vars.getStringParameter("inpemAteccoBanco");     data.emAteccoBancor = vars.getStringParameter("inpemAteccoBanco_R");     data.emAteccoDocstatus = vars.getRequestGlobalVariable("inpemAteccoDocstatus", windowId + "|EM_Atecco_Docstatus");     data.emAteccoDocstatusr = vars.getStringParameter("inpemAteccoDocstatus_R");     data.emAteccoIniciar = vars.getRequiredStringParameter("inpemAteccoIniciar");     data.emAteccoProcesar = vars.getRequiredStringParameter("inpemAteccoProcesar");     data.emAteccoValidar = vars.getStringParameter("inpemAteccoValidar");     data.emAteccoPagodescuento = vars.getStringParameter("inpemAteccoPagodescuento", "N");     data.emAteccoAnticipo = vars.getRequiredStringParameter("inpemAteccoAnticipo");     data.emAteccoAnticipostatus = vars.getRequiredGlobalVariable("inpemAteccoAnticipostatus", windowId + "|EM_Atecco_Anticipostatus");     data.emAteccoAnticipostatusr = vars.getStringParameter("inpemAteccoAnticipostatus_R");     data.dateprinted = vars.getStringParameter("inpdateprinted");     data.cDoctypetargetId = vars.getRequiredStringParameter("inpcDoctypetargetId");     data.isdelivered = vars.getStringParameter("inpisdelivered", "N");     data.isinvoiced = vars.getStringParameter("inpisinvoiced", "N");     data.isprinted = vars.getStringParameter("inpisprinted", "N");     data.isselected = vars.getStringParameter("inpisselected", "N");     data.docstatus = vars.getRequiredGlobalVariable("inpdocstatus", windowId + "|DocStatus");     data.issotrx = vars.getRequiredInputGlobalVariable("inpissotrx", windowId + "|IsSOTrx", "N");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.documentno = vars.getRequiredStringParameter("inpdocumentno");     data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.docaction = vars.getRequiredStringParameter("inpdocaction");     data.processing = vars.getStringParameter("inpprocessing");     data.cOrderId = vars.getRequestGlobalVariable("inpcOrderId", windowId + "|C_Order_ID");     data.processed = vars.getRequiredInputGlobalVariable("inpprocessed", windowId + "|Processed", "N");     data.cDoctypeId = vars.getRequiredGlobalVariable("inpcDoctypeId", windowId + "|C_DocType_ID"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");



    
         if (data.documentno.startsWith("<")) data.documentno = Utility.getDocumentNo(con, this, vars, windowId, "C_Order", data.cDoctypetargetId, data.cDoctypeId, false, true);

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }




    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|C_BPartner_ID", data[0].getField("cBpartnerId"));    vars.setSessionValue(windowId + "|DateOrdered", data[0].getField("dateordered"));    vars.setSessionValue(windowId + "|em_dec_tipo_evento", data[0].getField("emDecTipoEvento"));    vars.setSessionValue(windowId + "|C_BPartner_Location_ID", data[0].getField("cBpartnerLocationId"));    vars.setSessionValue(windowId + "|DatePromised", data[0].getField("datepromised"));    vars.setSessionValue(windowId + "|C_Currency_ID", data[0].getField("cCurrencyId"));    vars.setSessionValue(windowId + "|FreightCostRule", data[0].getField("freightcostrule"));    vars.setSessionValue(windowId + "|DeliveryViaRule", data[0].getField("deliveryviarule"));    vars.setSessionValue(windowId + "|M_Shipper_ID", data[0].getField("mShipperId"));    vars.setSessionValue(windowId + "|M_Warehouse_ID", data[0].getField("mWarehouseId"));    vars.setSessionValue(windowId + "|M_PriceList_ID", data[0].getField("mPricelistId"));    vars.setSessionValue(windowId + "|Posted", data[0].getField("posted"));    vars.setSessionValue(windowId + "|EM_Atecco_Docstatus", data[0].getField("emAteccoDocstatus"));    vars.setSessionValue(windowId + "|EM_Atecco_Anticipostatus", data[0].getField("emAteccoAnticipostatus"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));    vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|IsSOTrx", data[0].getField("issotrx"));    vars.setSessionValue(windowId + "|Processed", data[0].getField("processed"));    vars.setSessionValue(windowId + "|C_DocType_ID", data[0].getField("cDoctypeId"));    vars.setSessionValue(windowId + "|C_Order_ID", data[0].getField("cOrderId"));    vars.setSessionValue(windowId + "|DocStatus", data[0].getField("docstatus"));
    }

    private void refreshSessionNew(VariablesSecureApp vars) throws IOException,ServletException {
      PedidoCliente294F4FDD3CB849138A093832B628F92FData[] data = PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), vars.getStringParameter("inpcOrderId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamAD_Org_ID = vars.getSessionValue(tabId + "|paramAD_Org_ID");
String strParamDocumentNo = vars.getSessionValue(tabId + "|paramDocumentNo");
String strParamPOReference = vars.getSessionValue(tabId + "|paramPOReference");
String strParamDateOrdered = vars.getSessionValue(tabId + "|paramDateOrdered");
String strParamC_DocTypeTarget_ID = vars.getSessionValue(tabId + "|paramC_DocTypeTarget_ID");
String strParamC_BPartner_ID = vars.getSessionValue(tabId + "|paramC_BPartner_ID");
String strParamInvoiceRule = vars.getSessionValue(tabId + "|paramInvoiceRule");
String strParamGrandTotal = vars.getSessionValue(tabId + "|paramGrandTotal");
String strParamDocStatus = vars.getSessionValue(tabId + "|paramDocStatus");
String strParamDateOrdered_f = vars.getSessionValue(tabId + "|paramDateOrdered_f");
String strParamGrandTotal_f = vars.getSessionValue(tabId + "|paramGrandTotal_f");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamAD_Org_ID) && ("").equals(strParamDocumentNo) && ("").equals(strParamPOReference) && ("").equals(strParamDateOrdered) && ("").equals(strParamC_DocTypeTarget_ID) && ("").equals(strParamC_BPartner_ID) && ("").equals(strParamInvoiceRule) && ("").equals(strParamGrandTotal) && ("").equals(strParamDocStatus) && ("").equals(strParamDateOrdered_f) && ("").equals(strParamGrandTotal_f)) || !(("").equals(strParamAD_Org_ID) || ("%").equals(strParamAD_Org_ID))  || !(("").equals(strParamDocumentNo) || ("%").equals(strParamDocumentNo))  || !(("").equals(strParamPOReference) || ("%").equals(strParamPOReference))  || !(("").equals(strParamDateOrdered) || ("%").equals(strParamDateOrdered))  || !(("").equals(strParamC_DocTypeTarget_ID) || ("%").equals(strParamC_DocTypeTarget_ID))  || !(("").equals(strParamC_BPartner_ID) || ("%").equals(strParamC_BPartner_ID))  || !(("").equals(strParamInvoiceRule) || ("%").equals(strParamInvoiceRule))  || !(("").equals(strParamGrandTotal) || ("%").equals(strParamGrandTotal))  || !(("").equals(strParamDocStatus) || ("%").equals(strParamDocStatus))  || !(("").equals(strParamDateOrdered_f) || ("%").equals(strParamDateOrdered_f))  || !(("").equals(strParamGrandTotal_f) || ("%").equals(strParamGrandTotal_f)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strC_Order_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strC_Order_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atums/decoraciones/PedidoCliente/PedidoCliente294F4FDD3CB849138A093832B628F92F_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "PedidoCliente294F4FDD3CB849138A093832B628F92F", false, "document.frmMain.inpcOrderId", "grid", "..", "".equals("Y"), "PedidoCliente", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());



    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "cOrderId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "PedidoCliente294F4FDD3CB849138A093832B628F92F_Relation.html", "PedidoCliente", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "PedidoCliente294F4FDD3CB849138A093832B628F92F_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }


    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean _boolNew, String strC_Order_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    // copy param to variable as will be modified later
    boolean boolNew = _boolNew;

    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    PedidoCliente294F4FDD3CB849138A093832B628F92FData[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamAD_Org_ID = vars.getSessionValue(tabId + "|paramAD_Org_ID");
String strParamDocumentNo = vars.getSessionValue(tabId + "|paramDocumentNo");
String strParamPOReference = vars.getSessionValue(tabId + "|paramPOReference");
String strParamDateOrdered = vars.getSessionValue(tabId + "|paramDateOrdered");
String strParamC_DocTypeTarget_ID = vars.getSessionValue(tabId + "|paramC_DocTypeTarget_ID");
String strParamC_BPartner_ID = vars.getSessionValue(tabId + "|paramC_BPartner_ID");
String strParamInvoiceRule = vars.getSessionValue(tabId + "|paramInvoiceRule");
String strParamGrandTotal = vars.getSessionValue(tabId + "|paramGrandTotal");
String strParamDocStatus = vars.getSessionValue(tabId + "|paramDocStatus");
String strParamDateOrdered_f = vars.getSessionValue(tabId + "|paramDateOrdered_f");
String strParamGrandTotal_f = vars.getSessionValue(tabId + "|paramGrandTotal_f");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamAD_Org_ID) && ("").equals(strParamDocumentNo) && ("").equals(strParamPOReference) && ("").equals(strParamDateOrdered) && ("").equals(strParamC_DocTypeTarget_ID) && ("").equals(strParamC_BPartner_ID) && ("").equals(strParamInvoiceRule) && ("").equals(strParamGrandTotal) && ("").equals(strParamDocStatus) && ("").equals(strParamDateOrdered_f) && ("").equals(strParamGrandTotal_f)) || !(("").equals(strParamAD_Org_ID) || ("%").equals(strParamAD_Org_ID))  || !(("").equals(strParamDocumentNo) || ("%").equals(strParamDocumentNo))  || !(("").equals(strParamPOReference) || ("%").equals(strParamPOReference))  || !(("").equals(strParamDateOrdered) || ("%").equals(strParamDateOrdered))  || !(("").equals(strParamC_DocTypeTarget_ID) || ("%").equals(strParamC_DocTypeTarget_ID))  || !(("").equals(strParamC_BPartner_ID) || ("%").equals(strParamC_BPartner_ID))  || !(("").equals(strParamInvoiceRule) || ("%").equals(strParamInvoiceRule))  || !(("").equals(strParamGrandTotal) || ("%").equals(strParamGrandTotal))  || !(("").equals(strParamDocStatus) || ("%").equals(strParamDocStatus))  || !(("").equals(strParamDateOrdered_f) || ("%").equals(strParamDateOrdered_f))  || !(("").equals(strParamGrandTotal_f) || ("%").equals(strParamGrandTotal_f)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strC_Order_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strC_Order_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new PedidoCliente294F4FDD3CB849138A093832B628F92FData[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("cOrderId") == null || dataField.getField("cOrderId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars);
        data = PedidoCliente294F4FDD3CB849138A093832B628F92FData.set(Utility.getDefault(this, vars, "EM_Atecco_Borra_2", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Borra_1", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Convertquotation", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "em_dec_hora_montaje", "@#Date@", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "validuntil", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "A_Asset_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Transporte", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Docstatus", "NV", "C1176D728CF145319CA757A153305ED1", "", dataField), "", Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "AD_Org_ID", "@#AD_Org_ID@", "C1176D728CF145319CA757A153305ED1", "", dataField), "Y", Utility.getDefault(this, vars, "CreatedBy", "", "C1176D728CF145319CA757A153305ED1", "", dataField), PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectDef2166_0(this, Utility.getDefault(this, vars, "CreatedBy", "", "C1176D728CF145319CA757A153305ED1", "", dataField)), Utility.getDefault(this, vars, "UpdatedBy", "", "C1176D728CF145319CA757A153305ED1", "", dataField), PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectDef2168_1(this, Utility.getDefault(this, vars, "UpdatedBy", "", "C1176D728CF145319CA757A153305ED1", "", dataField)), Utility.getDefault(this, vars, "DocumentNo", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "DocStatus", "DR", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "DocAction", "CO", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "C_DocType_ID", "0", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "C_DocTypeTarget_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Description", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "IsDelivered", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "IsInvoiced", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "IsPrinted", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "DateOrdered", "@#Date@", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "DatePromised", "@#Date@", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "DateAcct", "@#Date@", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "SalesRep_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "C_PaymentTerm_ID", "EC001", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "BillTo_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "C_Currency_ID", "@C_Currency_ID@", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "InvoiceRule", "D", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "FreightAmt", "", "C1176D728CF145319CA757A153305ED1", "0", dataField), Utility.getDefault(this, vars, "DeliveryViaRule", "P", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "M_Shipper_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "PriorityRule", "5", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "TotalLines", "", "C1176D728CF145319CA757A153305ED1", "0", dataField), Utility.getDefault(this, vars, "GrandTotal", "", "C1176D728CF145319CA757A153305ED1", "0", dataField), Utility.getDefault(this, vars, "M_Warehouse_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "M_PriceList_ID", "DCABDCB91813412CA731174E94BD2ED9", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Processing", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "C_Campaign_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "C_BPartner_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectDef2762_2(this, Utility.getDefault(this, vars, "C_BPartner_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField)), Utility.getDefault(this, vars, "AD_User_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "em_dec_fecha_montaje", "@#Date@", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "em_dec_tipo_evento", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "POReference", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "C_Charge_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "ChargeAmt", "0", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Tipo_Tarjeta", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Processed", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "C_BPartner_Location_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "C_Project_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectDef3402_3(this, Utility.getDefault(this, vars, "C_Project_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField)), Utility.getDefault(this, vars, "C_Activity_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Quotation_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectDef367DCAA9CF4442ADB9A76F6539102217_4(this, Utility.getDefault(this, vars, "Quotation_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField)), Utility.getDefault(this, vars, "IsSOTrx", "@IsSOTrx@", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "DatePrinted", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "DeliveryRule", "A", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "FreightCostRule", "I", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Iniciar", "NV", "C1176D728CF145319CA757A153305ED1", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "00143645AE464184A81DA4AD0D22EF09", Utility.getDefault(this, vars, "EM_Atecco_Iniciar", "NV", "C1176D728CF145319CA757A153305ED1", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "00143645AE464184A81DA4AD0D22EF09", Utility.getDefault(this, vars, "EM_Atecco_Iniciar", "NV", "C1176D728CF145319CA757A153305ED1", "N", dataField))), Utility.getDefault(this, vars, "PaymentRule", "B", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "IsDiscountPrinted", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "Posted", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "234", Utility.getDefault(this, vars, "Posted", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "234", Utility.getDefault(this, vars, "Posted", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField))), Utility.getDefault(this, vars, "IsTaxIncluded", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "IsSelected", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Fecha_Pago", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "em_dec_hora_desmontaje", "@#Date@", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "C_Costcenter_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Deliverynotes", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "C_Incoterms_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Incotermsdescription", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Generatetemplate", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "Delivery_Location_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "CopyFromPO", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Pagodescuento", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField), PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectDef828EE0AE802C5FA1E040007F010067C7(this), PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectDef831B272EE4364C208EFA70098EDF29B4(this, Utility.getContext(this, vars, "#AD_Org_ID", windowId), Utility.getContext(this, vars, "AD_Role_ID", "C1176D728CF145319CA757A153305ED1"), Utility.getContext(this, vars, "#AD_Client_ID", windowId)), Utility.getDefault(this, vars, "DropShip_User_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "DropShip_BPartner_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "CopyFrom", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "DropShip_Location_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "IsSelfService", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Anticipostatus", "SA", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Validar", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "AD_OrgTrx_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "User2_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "User1_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Calculate_Promotions", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Docaction", "CO", "C1176D728CF145319CA757A153305ED1", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "FF80818130217A35013021A672400035", Utility.getDefault(this, vars, "EM_Atecco_Docaction", "CO", "C1176D728CF145319CA757A153305ED1", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "FF80818130217A35013021A672400035", Utility.getDefault(this, vars, "EM_Atecco_Docaction", "CO", "C1176D728CF145319CA757A153305ED1", "N", dataField))), Utility.getDefault(this, vars, "EM_Atecco_Banco", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "RM_PickFromShipment", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "RM_ReceiveMaterials", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "RM_CreateInvoice", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "RM_Pickfromreceipt", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "em_dec_requiere_montaje", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Imprimir", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "C_Return_Reason_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "em_dec_requiere_transporte", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "RM_AddOrphanLine", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "SO_Res_Status", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Anticipo", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "em_dec_tipo_contratacion", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Create_POLines", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "C_Reject_Reason_ID", "", "C1176D728CF145319CA757A153305ED1", "", dataField), Utility.getDefault(this, vars, "Iscashvat", "N", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Procesar", "CO", "C1176D728CF145319CA757A153305ED1", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "C2075E669BA7489CA61B85FA29883F1F", Utility.getDefault(this, vars, "EM_Atecco_Procesar", "CO", "C1176D728CF145319CA757A153305ED1", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "C2075E669BA7489CA61B85FA29883F1F", Utility.getDefault(this, vars, "EM_Atecco_Procesar", "CO", "C1176D728CF145319CA757A153305ED1", "N", dataField))), Utility.getDefault(this, vars, "EM_APRM_AddPayment", "", "C1176D728CF145319CA757A153305ED1", "N", dataField), Utility.getDefault(this, vars, "em_dec_fecha_desmontaje", "@#Date@", "C1176D728CF145319CA757A153305ED1", "", dataField));
             data[0].documentno = "<" + Utility.getDocumentNo( this, vars, windowId, "C_Order", data[0].cDoctypetargetId, data[0].cDoctypeId, false, false) + ">";
      }
     }
      
    
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atums/decoraciones/PedidoCliente/PedidoCliente294F4FDD3CB849138A093832B628F92F_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atums/decoraciones/PedidoCliente/PedidoCliente294F4FDD3CB849138A093832B628F92F_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "PedidoCliente294F4FDD3CB849138A093832B628F92F", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpcOrderId", "", "..", "".equals("Y"), "PedidoCliente", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strC_Order_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strC_Order_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "PedidoCliente294F4FDD3CB849138A093832B628F92F_Relation.html", "PedidoCliente", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "PedidoCliente294F4FDD3CB849138A093832B628F92F_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
xmlDocument.setParameter("DateOrdered_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "18", "em_dec_tipo_evento", "E570AF8F89A04F069736BC3C2953197D", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("emDecTipoEvento"):dataField.getField("emDecTipoEvento")));
xmlDocument.setData("reportem_dec_tipo_evento","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_BPartner_Location_ID", "", "167", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cBpartnerLocationId"):dataField.getField("cBpartnerLocationId")));
xmlDocument.setData("reportC_BPartner_Location_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("DatePromised_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "18", "em_dec_tipo_contratacion", "960068DEF7974A1D9AD3609C1DEEC91B", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("emDecTipoContratacion"):dataField.getField("emDecTipoContratacion")));
xmlDocument.setData("reportem_dec_tipo_contratacion","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("em_dec_fecha_montaje_Format", vars.getSessionValue("#AD_SqlDateFormat"));
xmlDocument.setParameter("em_dec_fecha_desmontaje_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "18", "SalesRep_ID", "190", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("salesrepId"):dataField.getField("salesrepId")));
xmlDocument.setData("reportSalesRep_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("DateAcct_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "18", "BillTo_ID", "159", "119", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("billtoId"):dataField.getField("billtoId")));
xmlDocument.setData("reportBillTo_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_Currency_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cCurrencyId"):dataField.getField("cCurrencyId")));
xmlDocument.setData("reportC_Currency_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "PaymentRule", "195", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("paymentrule"):dataField.getField("paymentrule")));
xmlDocument.setData("reportPaymentRule","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_PaymentTerm_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cPaymenttermId"):dataField.getField("cPaymenttermId")));
xmlDocument.setData("reportC_PaymentTerm_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "InvoiceRule", "150", "1000200003", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("invoicerule"):dataField.getField("invoicerule")));
xmlDocument.setData("reportInvoiceRule","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "DeliveryRule", "151", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("deliveryrule"):dataField.getField("deliveryrule")));
xmlDocument.setData("reportDeliveryRule","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "FreightCostRule", "153", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("freightcostrule"):dataField.getField("freightcostrule")));
xmlDocument.setData("reportFreightCostRule","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonFreightAmt", Utility.messageBD(this, "Calc", vars.getLanguage()));
comboTableData = new ComboTableData(vars, this, "17", "DeliveryViaRule", "152", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("deliveryviarule"):dataField.getField("deliveryviarule")));
xmlDocument.setData("reportDeliveryViaRule","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "M_Shipper_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("mShipperId"):dataField.getField("mShipperId")));
xmlDocument.setData("reportM_Shipper_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "C_Charge_ID", "200", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cChargeId"):dataField.getField("cChargeId")));
xmlDocument.setData("reportC_Charge_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonChargeAmt", Utility.messageBD(this, "Calc", vars.getLanguage()));
comboTableData = new ComboTableData(vars, this, "17", "PriorityRule", "154", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("priorityrule"):dataField.getField("priorityrule")));
xmlDocument.setData("reportPriorityRule","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonTotalLines", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonGrandTotal", Utility.messageBD(this, "Calc", vars.getLanguage()));
comboTableData = new ComboTableData(vars, this, "19", "M_PriceList_ID", "", "DB85A30739C3487988921CE3FFFD3BAD", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("mPricelistId"):dataField.getField("mPricelistId")));
xmlDocument.setData("reportM_PriceList_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_Campaign_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cCampaignId"):dataField.getField("cCampaignId")));
xmlDocument.setData("reportC_Campaign_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_Activity_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cActivityId"):dataField.getField("cActivityId")));
xmlDocument.setData("reportC_Activity_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Posted_BTNname", Utility.getButtonName(this, vars, "234", (dataField==null?data[0].getField("posted"):dataField.getField("posted")), "Posted_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalPosted = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("Posted_Modal", modalPosted?"true":"false");
comboTableData = new ComboTableData(vars, this, "19", "AD_User_ID", "", "123", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adUserId"):dataField.getField("adUserId")));
xmlDocument.setData("reportAD_User_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("CopyFrom_BTNname", Utility.getButtonName(this, vars, "74658C0EE18A4777B162E83E984AB357", "CopyFrom_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalCopyFrom = org.openbravo.erpCommon.utility.Utility.isModalProcess("211"); 
xmlDocument.setParameter("CopyFrom_Modal", modalCopyFrom?"true":"false");
comboTableData = new ComboTableData(vars, this, "18", "DropShip_Location_ID", "159", "120", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("dropshipLocationId"):dataField.getField("dropshipLocationId")));
xmlDocument.setData("reportDropShip_Location_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "DropShip_User_ID", "110", "168", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("dropshipUserId"):dataField.getField("dropshipUserId")));
xmlDocument.setData("reportDropShip_User_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "AD_OrgTrx_ID", "130", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adOrgtrxId"):dataField.getField("adOrgtrxId")));
xmlDocument.setData("reportAD_OrgTrx_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_Incoterms_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cIncotermsId"):dataField.getField("cIncotermsId")));
xmlDocument.setData("reportC_Incoterms_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Generatetemplate_BTNname", Utility.getButtonName(this, vars, "EA755830B7C14758851FA4B92AA1EE3A", "Generatetemplate_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalGeneratetemplate = org.openbravo.erpCommon.utility.Utility.isModalProcess("800022"); 
xmlDocument.setParameter("Generatetemplate_Modal", modalGeneratetemplate?"true":"false");
comboTableData = new ComboTableData(vars, this, "18", "Delivery_Location_ID", "159", "167", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("deliveryLocationId"):dataField.getField("deliveryLocationId")));
xmlDocument.setData("reportDelivery_Location_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("CopyFromPO_BTNname", Utility.getButtonName(this, vars, "2F8BBE34C5314670B0FD2F51A0C05714", "CopyFromPO_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalCopyFromPO = org.openbravo.erpCommon.utility.Utility.isModalProcess("800165"); 
xmlDocument.setParameter("CopyFromPO_Modal", modalCopyFromPO?"true":"false");
comboTableData = new ComboTableData(vars, this, "19", "FIN_Paymentmethod_ID", "", "FF80808130B107670130B1115F22000D", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("finPaymentmethodId"):dataField.getField("finPaymentmethodId")));
xmlDocument.setData("reportFIN_Paymentmethod_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "FIN_Payment_Priority_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("finPaymentPriorityId"):dataField.getField("finPaymentPriorityId")));
xmlDocument.setData("reportFIN_Payment_Priority_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("RM_PickFromShipment_BTNname", Utility.getButtonName(this, vars, "D25BDEF8333B4278949E99C8D2F8CF58", "RM_PickFromShipment_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalRM_PickFromShipment = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("RM_PickFromShipment_Modal", modalRM_PickFromShipment?"true":"false");
xmlDocument.setParameter("RM_ReceiveMaterials_BTNname", Utility.getButtonName(this, vars, "C5032ACA998442BB90E4E06926976C3B", "RM_ReceiveMaterials_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalRM_ReceiveMaterials = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("RM_ReceiveMaterials_Modal", modalRM_ReceiveMaterials?"true":"false");
xmlDocument.setParameter("RM_CreateInvoice_BTNname", Utility.getButtonName(this, vars, "6E6FCD03040142929B1CAC56538A8C6C", "RM_CreateInvoice_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalRM_CreateInvoice = org.openbravo.erpCommon.utility.Utility.isModalProcess("FF80808133362F6A013336781FCE0066"); 
xmlDocument.setParameter("RM_CreateInvoice_Modal", modalRM_CreateInvoice?"true":"false");
comboTableData = new ComboTableData(vars, this, "19", "C_Return_Reason_ID", "", "88C0B1F928434B4EAAA9D07B2D2F63E1", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cReturnReasonId"):dataField.getField("cReturnReasonId")));
xmlDocument.setData("reportC_Return_Reason_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("RM_AddOrphanLine_BTNname", Utility.getButtonName(this, vars, "768FB0DF936D4BAF8577944A8F7C842E", "RM_AddOrphanLine_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalRM_AddOrphanLine = org.openbravo.erpCommon.utility.Utility.isModalProcess("23D1B163EC0B41F790CE39BF01DA320E"); 
xmlDocument.setParameter("RM_AddOrphanLine_Modal", modalRM_AddOrphanLine?"true":"false");
xmlDocument.setParameter("Calculate_Promotions_BTNname", Utility.getButtonName(this, vars, "EC90501B9F034584858B5E320D2B6B4B", "Calculate_Promotions_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalCalculate_Promotions = org.openbravo.erpCommon.utility.Utility.isModalProcess("9EB2228A60684C0DBEC12D5CD8D85218"); 
xmlDocument.setParameter("Calculate_Promotions_Modal", modalCalculate_Promotions?"true":"false");
xmlDocument.setParameter("Convertquotation_BTNname", Utility.getButtonName(this, vars, "1F0DA9E9A33C4D7DB30F504BA36F7066", "Convertquotation_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalConvertquotation = org.openbravo.erpCommon.utility.Utility.isModalProcess("A3FE1F9892394386A49FB707AA50A0FA"); 
xmlDocument.setParameter("Convertquotation_Modal", modalConvertquotation?"true":"false");
comboTableData = new ComboTableData(vars, this, "19", "C_Reject_Reason_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cRejectReasonId"):dataField.getField("cRejectReasonId")));
xmlDocument.setData("reportC_Reject_Reason_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("validuntil_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "17", "SO_Res_Status", "C3C19DE8AB3B42E78748E20D986FBBC9", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("soResStatus"):dataField.getField("soResStatus")));
xmlDocument.setData("reportSO_Res_Status","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Create_POLines_BTNname", Utility.getButtonName(this, vars, "7773A739A793435F82085214167A8C18", "Create_POLines_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalCreate_POLines = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("Create_POLines_Modal", modalCreate_POLines?"true":"false");
xmlDocument.setParameter("RM_Pickfromreceipt_BTNname", Utility.getButtonName(this, vars, "871D269C119047FA9435DF66C181025A", "RM_Pickfromreceipt_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalRM_Pickfromreceipt = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("RM_Pickfromreceipt_Modal", modalRM_Pickfromreceipt?"true":"false");
xmlDocument.setParameter("EM_APRM_AddPayment_BTNname", Utility.getButtonName(this, vars, "893836DCD1CE47949AFDAE9F6A62CB75", "EM_APRM_AddPayment_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_APRM_AddPayment = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("EM_APRM_AddPayment_Modal", modalEM_APRM_AddPayment?"true":"false");
xmlDocument.setParameter("EM_Atecco_Fecha_Pago_Format", vars.getSessionValue("#AD_SqlDateFormat"));
xmlDocument.setParameter("EM_Atecco_Docaction_BTNname", Utility.getButtonName(this, vars, "FF80818130217A35013021A672400035", (dataField==null?data[0].getField("emAteccoDocaction"):dataField.getField("emAteccoDocaction")), "EM_Atecco_Docaction_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Atecco_Docaction = org.openbravo.erpCommon.utility.Utility.isModalProcess("18462B09D3C24A7E97A9CCE9F4C0851D"); 
xmlDocument.setParameter("EM_Atecco_Docaction_Modal", modalEM_Atecco_Docaction?"true":"false");
xmlDocument.setParameter("EM_Atecco_Imprimir_BTNname", Utility.getButtonName(this, vars, "54D15C7CAC72431CBC6BBE2D8A6F9429", "EM_Atecco_Imprimir_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Atecco_Imprimir = org.openbravo.erpCommon.utility.Utility.isModalProcess("96AB407BA12B45479B84D94D7A5AEC79"); 
xmlDocument.setParameter("EM_Atecco_Imprimir_Modal", modalEM_Atecco_Imprimir?"true":"false");
comboTableData = new ComboTableData(vars, this, "17", "EM_Atecco_Tipo_Tarjeta", "547BCC92B2474F6CA266E838682858C9", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("emAteccoTipoTarjeta"):dataField.getField("emAteccoTipoTarjeta")));
xmlDocument.setData("reportEM_Atecco_Tipo_Tarjeta","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "EM_Atecco_Banco", "00DE8D05B02D41CF9ED325A0552EA5EB", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("emAteccoBanco"):dataField.getField("emAteccoBanco")));
xmlDocument.setData("reportEM_Atecco_Banco","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "EM_Atecco_Docstatus", "2EF8750FEAE14CBD8195D4F0006A03F0", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("emAteccoDocstatus"):dataField.getField("emAteccoDocstatus")));
xmlDocument.setData("reportEM_Atecco_Docstatus","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("EM_Atecco_Iniciar_BTNname", Utility.getButtonName(this, vars, "00143645AE464184A81DA4AD0D22EF09", (dataField==null?data[0].getField("emAteccoIniciar"):dataField.getField("emAteccoIniciar")), "EM_Atecco_Iniciar_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Atecco_Iniciar = org.openbravo.erpCommon.utility.Utility.isModalProcess("498E4525C6EB49168B75399AC301A264"); 
xmlDocument.setParameter("EM_Atecco_Iniciar_Modal", modalEM_Atecco_Iniciar?"true":"false");
xmlDocument.setParameter("EM_Atecco_Procesar_BTNname", Utility.getButtonName(this, vars, "C2075E669BA7489CA61B85FA29883F1F", (dataField==null?data[0].getField("emAteccoProcesar"):dataField.getField("emAteccoProcesar")), "EM_Atecco_Procesar_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Atecco_Procesar = org.openbravo.erpCommon.utility.Utility.isModalProcess("18462B09D3C24A7E97A9CCE9F4C0851D"); 
xmlDocument.setParameter("EM_Atecco_Procesar_Modal", modalEM_Atecco_Procesar?"true":"false");
xmlDocument.setParameter("EM_Atecco_Validar_BTNname", Utility.getButtonName(this, vars, "A76E8501F03E4806B92AF45DE6F80D50", "EM_Atecco_Validar_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Atecco_Validar = org.openbravo.erpCommon.utility.Utility.isModalProcess("1CED15AEA8B742D280860F5A87E68F20"); 
xmlDocument.setParameter("EM_Atecco_Validar_Modal", modalEM_Atecco_Validar?"true":"false");
xmlDocument.setParameter("EM_Atecco_Anticipo_BTNname", Utility.getButtonName(this, vars, "9556135E5C2D49C28BA4D9F3771BA3B9", "EM_Atecco_Anticipo_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Atecco_Anticipo = org.openbravo.erpCommon.utility.Utility.isModalProcess("1382DFDD4EF24D58A33C808E7AB277FC"); 
xmlDocument.setParameter("EM_Atecco_Anticipo_Modal", modalEM_Atecco_Anticipo?"true":"false");
comboTableData = new ComboTableData(vars, this, "17", "EM_Atecco_Anticipostatus", "D4A2CA1BC87B477C9253A232F6A1AE1C", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("emAteccoAnticipostatus"):dataField.getField("emAteccoAnticipostatus")));
xmlDocument.setData("reportEM_Atecco_Anticipostatus","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("DatePrinted_Format", vars.getSessionValue("#AD_SqlDateFormat"));
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonGeneratetemplate800022(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, String strgeneratetemplate, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 800022");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Generatetemplate800022", discard).createXmlDocument();
      xmlDocument.setParameter("key", strC_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "PedidoCliente294F4FDD3CB849138A093832B628F92F_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "800022");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("800022");
        vars.removeMessage("800022");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }


    void printPageButtonRM_CreateInvoiceFF80808133362F6A013336781FCE0066(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, String strrmCreateinvoice, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process FF80808133362F6A013336781FCE0066");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/RM_CreateInvoiceFF80808133362F6A013336781FCE0066", discard).createXmlDocument();
      xmlDocument.setParameter("key", strC_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "PedidoCliente294F4FDD3CB849138A093832B628F92F_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "FF80808133362F6A013336781FCE0066");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("FF80808133362F6A013336781FCE0066");
        vars.removeMessage("FF80808133362F6A013336781FCE0066");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    void printPageButtonRM_AddOrphanLine23D1B163EC0B41F790CE39BF01DA320E(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, String strrmAddorphanline, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 23D1B163EC0B41F790CE39BF01DA320E");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/RM_AddOrphanLine23D1B163EC0B41F790CE39BF01DA320E", discard).createXmlDocument();
      xmlDocument.setParameter("key", strC_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "PedidoCliente294F4FDD3CB849138A093832B628F92F_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "23D1B163EC0B41F790CE39BF01DA320E");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("23D1B163EC0B41F790CE39BF01DA320E");
        vars.removeMessage("23D1B163EC0B41F790CE39BF01DA320E");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    ComboTableData comboTableData = null;
    xmlDocument.setParameter("M_Product_ID", "");
    xmlDocument.setParameter("M_AttributeSetInstance_ID", "");
    xmlDocument.setParameter("M_AttributeSetInstance_IDR", PedidoCliente294F4FDD3CB849138A093832B628F92FData.selectActDefM_AttributeSetInstance_ID(this, ""));
    xmlDocument.setParameter("Returned", "");
    xmlDocument.setParameter("PriceStd", "");
    xmlDocument.setParameter("C_Tax_ID", "");
    comboTableData = new ComboTableData(vars, this, "19", "C_Tax_ID", "", "299FA667CF374AC5ACC74739C3251134", Utility.getContext(this, vars, "#AccessibleOrgTree", ""), Utility.getContext(this, vars, "#User_Client", ""), 0);
    Utility.fillSQLParameters(this, vars, (FieldProvider) vars.getSessionObject("button23D1B163EC0B41F790CE39BF01DA320E.originalParams"), comboTableData, windowId, "");
    xmlDocument.setData("reportC_Tax_ID", "liststructure", comboTableData.select(false));
comboTableData = null;
    xmlDocument.setParameter("C_Return_Reason_ID", "");
    comboTableData = new ComboTableData(vars, this, "19", "C_Return_Reason_ID", "", "", Utility.getContext(this, vars, "#AccessibleOrgTree", ""), Utility.getContext(this, vars, "#User_Client", ""), 0);
    Utility.fillSQLParameters(this, vars, (FieldProvider) vars.getSessionObject("button23D1B163EC0B41F790CE39BF01DA320E.originalParams"), comboTableData, windowId, "");
    xmlDocument.setData("reportC_Return_Reason_ID", "liststructure", comboTableData.select(false));
comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    void printPageButtonCalculate_Promotions9EB2228A60684C0DBEC12D5CD8D85218(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, String strcalculatePromotions, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 9EB2228A60684C0DBEC12D5CD8D85218");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Calculate_Promotions9EB2228A60684C0DBEC12D5CD8D85218", discard).createXmlDocument();
      xmlDocument.setParameter("key", strC_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "PedidoCliente294F4FDD3CB849138A093832B628F92F_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "9EB2228A60684C0DBEC12D5CD8D85218");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("9EB2228A60684C0DBEC12D5CD8D85218");
        vars.removeMessage("9EB2228A60684C0DBEC12D5CD8D85218");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    void printPageButtonConvertquotationA3FE1F9892394386A49FB707AA50A0FA(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, String strconvertquotation, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process A3FE1F9892394386A49FB707AA50A0FA");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/ConvertquotationA3FE1F9892394386A49FB707AA50A0FA", discard).createXmlDocument();
      xmlDocument.setParameter("key", strC_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "PedidoCliente294F4FDD3CB849138A093832B628F92F_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "A3FE1F9892394386A49FB707AA50A0FA");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("A3FE1F9892394386A49FB707AA50A0FA");
        vars.removeMessage("A3FE1F9892394386A49FB707AA50A0FA");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    xmlDocument.setParameter("RecalculatePrices", "Y");
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }


    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type) throws IOException, ServletException {
    PedidoCliente294F4FDD3CB849138A093832B628F92FData data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.cOrderId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (PedidoCliente294F4FDD3CB849138A093832B628F92FData.getCurrentDBTimestamp(this, data.cOrderId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.cOrderId = "";
                }
                else {                    
                    
                        //BUTTON TEXT FILLING
                    data.postedBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "234", data.getField("Posted"));
                    
                        //BUTTON TEXT FILLING
                    data.emAteccoProcesarBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "C2075E669BA7489CA61B85FA29883F1F", data.getField("EM_Atecco_Procesar"));
                    
                        //BUTTON TEXT FILLING
                    data.emAteccoIniciarBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "00143645AE464184A81DA4AD0D22EF09", data.getField("EM_Atecco_Iniciar"));
                    
                        //BUTTON TEXT FILLING
                    data.emAteccoDocactionBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "FF80818130217A35013021A672400035", data.getField("EM_Atecco_Docaction"));
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|C_Order_ID", data.cOrderId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet PedidoCliente294F4FDD3CB849138A093832B628F92F. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}

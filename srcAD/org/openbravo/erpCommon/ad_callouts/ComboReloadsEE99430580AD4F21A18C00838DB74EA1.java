
package org.openbravo.erpCommon.ad_callouts;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.Sqlc;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.xmlEngine.XmlDocument;

public class ComboReloadsEE99430580AD4F21A18C00838DB74EA1 extends CalloutHelper {
  private static final long serialVersionUID = 1L;

  void printPage(HttpServletResponse response, VariablesSecureApp vars, String strTabId, String windowId) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_callouts/CallOut").createXmlDocument();
    
    String parentOrg=vars.getStringParameter("inpParentOrganization");
    StringBuffer resultado = new StringBuffer();
    boolean isFirst=true;
    ComboTableData comboTableData = null;
    resultado.append("var calloutName='ComboReloadsEE99430580AD4F21A18C00838DB74EA1';\n\n");
    resultado.append("var respuesta = new Array(\n");

    // check if call came from searchPopup, if yes remember and adjust command name from popup
    // column name pattern to normal pattern and set search popup frame name to be used
    String resultField;
    String command = vars.getStringParameter("Command", "DEFAULT");
    boolean calledFromSearch = command.startsWith("inpParam");
    if (calledFromSearch) {
      command = command.substring(8);
      command = Sqlc.TransformaNombreColumna(command);
      command = "inp" + command;
      xmlDocument.setParameter("frameName", "mainframe");
      xmlDocument.setParameter("frameName1", "mainframe");
    }

    

    resultado.append("\n);");

    xmlDocument.setParameter("array", resultado.toString());
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}

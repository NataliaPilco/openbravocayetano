
package org.openbravo.erpCommon.ad_callouts;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.Sqlc;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.xmlEngine.XmlDocument;

public class ComboReloads259 extends CalloutHelper {
  private static final long serialVersionUID = 1L;

  void printPage(HttpServletResponse response, VariablesSecureApp vars, String strTabId, String windowId) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_callouts/CallOut").createXmlDocument();
    
    String parentOrg=vars.getStringParameter("inpParentOrganization");
    StringBuffer resultado = new StringBuffer();
    boolean isFirst=true;
    ComboTableData comboTableData = null;
    resultado.append("var calloutName='ComboReloads259';\n\n");
    resultado.append("var respuesta = new Array(\n");

    // check if call came from searchPopup, if yes remember and adjust command name from popup
    // column name pattern to normal pattern and set search popup frame name to be used
    String resultField;
    String command = vars.getStringParameter("Command", "DEFAULT");
    boolean calledFromSearch = command.startsWith("inpParam");
    if (calledFromSearch) {
      command = command.substring(8);
      command = Sqlc.TransformaNombreColumna(command);
      command = "inp" + command;
      xmlDocument.setParameter("frameName", "mainframe");
      xmlDocument.setParameter("frameName1", "mainframe");
    }

    
    try {

    
      if (CalloutHelper.commandInCommandList(command, "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "17", "EM_Atecco_Docstatus", "F09025250F444F4CA184018603F54DC8", "", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamEM_Atecco_Docstatus";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpemAteccoDocstatus";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    
      if (CalloutHelper.commandInCommandList(command, "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "19", "C_Activity_ID", "", "", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamC_Activity_ID";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpcActivityId";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    
      if (CalloutHelper.commandInCommandList(command, "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "19", "C_Campaign_ID", "", "", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamC_Campaign_ID";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpcCampaignId";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    
      if (CalloutHelper.commandInCommandList(command, "inp#adClientId", "inpadOrgId", "inpadOrgId", "inpcInvoiceId", "inpcInvoiceId", "inpcInvoiceId", "inpcInvoiceId", "inpcInvoiceId", "inpcInvoiceId", "inpcInvoiceId", "inpcInvoiceId", "inpcInvoiceId", "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "18", "EM_Co_Doctype_ID", "22F546D49D3A48E1B2B4F50446A8DE58", "1793D0CA3A544000A4D7173ED549CDAB", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamEM_Co_Doctype_ID";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpemCoDoctypeId";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    

    } catch (ServletException ex) {
      OBError myError = Utility.translateError(this, vars, vars.getLanguage(), ex.toString());
      bdErrorHidden(response, myError.getType(), myError.getTitle(), myError.getMessage());
      return;
    } catch (Exception ex1) {
      OBError myError = Utility.translateError(this, vars, vars.getLanguage(), ex1.toString());
      bdErrorHidden(response, myError.getType(), myError.getTitle(), myError.getMessage());
      return;
    }
    

    resultado.append("\n);");

    xmlDocument.setParameter("array", resultado.toString());
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}

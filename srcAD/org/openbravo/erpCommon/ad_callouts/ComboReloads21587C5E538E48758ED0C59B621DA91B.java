
package org.openbravo.erpCommon.ad_callouts;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.Sqlc;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.xmlEngine.XmlDocument;

public class ComboReloads21587C5E538E48758ED0C59B621DA91B extends CalloutHelper {
  private static final long serialVersionUID = 1L;

  void printPage(HttpServletResponse response, VariablesSecureApp vars, String strTabId, String windowId) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_callouts/CallOut").createXmlDocument();
    
    String parentOrg=vars.getStringParameter("inpParentOrganization");
    StringBuffer resultado = new StringBuffer();
    boolean isFirst=true;
    ComboTableData comboTableData = null;
    resultado.append("var calloutName='ComboReloads21587C5E538E48758ED0C59B621DA91B';\n\n");
    resultado.append("var respuesta = new Array(\n");

    // check if call came from searchPopup, if yes remember and adjust command name from popup
    // column name pattern to normal pattern and set search popup frame name to be used
    String resultField;
    String command = vars.getStringParameter("Command", "DEFAULT");
    boolean calledFromSearch = command.startsWith("inpParam");
    if (calledFromSearch) {
      command = command.substring(8);
      command = Sqlc.TransformaNombreColumna(command);
      command = "inp" + command;
      xmlDocument.setParameter("frameName", "mainframe");
      xmlDocument.setParameter("frameName1", "mainframe");
    }

    
    try {

    
      if (CalloutHelper.commandInCommandList(command, "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "19", "A_Asset_ID", "", "", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamA_Asset_ID";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpaAssetId";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    
      if (CalloutHelper.commandInCommandList(command, "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "19", "C_Costcenter_ID", "", "", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamC_Costcenter_ID";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpcCostcenterId";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    
      if (CalloutHelper.commandInCommandList(command, "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "19", "C_AcctSchema_ID", "", "", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamC_AcctSchema_ID";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpcAcctschemaId";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    
      if (CalloutHelper.commandInCommandList(command, "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "19", "C_Project_ID", "", "", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamC_Project_ID";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpcProjectId";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    
      if (CalloutHelper.commandInCommandList(command, "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "19", "C_Period_ID", "", "", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamC_Period_ID";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpcPeriodId";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    
      if (CalloutHelper.commandInCommandList(command, "inpadOrgId")) {
        if (!isFirst) resultado.append(", \n");
        comboTableData = new ComboTableData(vars, this, "19", "C_Currency_ID", "", "", Utility.getReferenceableOrg(vars, vars.getStringParameter("inpadOrgId")), Utility.getContext(this, vars, "#User_Client", windowId), 0);
        if (calledFromSearch) {
          comboTableData.fillParametersFromSearch("", windowId);
          resultField = "inpParamC_Currency_ID";
        } else {
          comboTableData.fillParameters(null, windowId, "");
          resultField = "inpcCurrencyId";
        }
        resultado.append("new Array(\"" + resultField + "\", ");
        resultado.append(generateArray(comboTableData.select(false), vars.getStringParameter(resultField)));
        comboTableData = null;
        resultado.append(")");
        isFirst=false;
      }
    

    } catch (ServletException ex) {
      OBError myError = Utility.translateError(this, vars, vars.getLanguage(), ex.toString());
      bdErrorHidden(response, myError.getType(), myError.getTitle(), myError.getMessage());
      return;
    } catch (Exception ex1) {
      OBError myError = Utility.translateError(this, vars, vars.getLanguage(), ex1.toString());
      bdErrorHidden(response, myError.getType(), myError.getTitle(), myError.getMessage());
      return;
    }
    

    resultado.append("\n);");

    xmlDocument.setParameter("array", resultado.toString());
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}

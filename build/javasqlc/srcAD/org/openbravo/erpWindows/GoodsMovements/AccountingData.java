//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.GoodsMovements;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class AccountingData implements FieldProvider {
static Logger log4j = Logger.getLogger(AccountingData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adClientId;
  public String adOrgId;
  public String adOrgIdr;
  public String isactive;
  public String recordId;
  public String cAcctschemaId;
  public String cAcctschemaIdr;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String cPeriodId;
  public String cPeriodIdr;
  public String dateacct;
  public String seqno;
  public String accountId;
  public String accountIdr;
  public String acctvalue;
  public String acctdescription;
  public String amtacctdr;
  public String amtacctcr;
  public String description;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String mProductId;
  public String mProductIdr;
  public String cProjectId;
  public String cProjectIdr;
  public String cCostcenterId;
  public String cCostcenterIdr;
  public String aAssetId;
  public String aAssetIdr;
  public String user1Id;
  public String user2Id;
  public String cSalesregionId;
  public String cDoctypeId;
  public String docbasetype;
  public String factAcctGroupId;
  public String cLoctoId;
  public String amtsourcedr;
  public String cTaxId;
  public String lineId;
  public String adTableId;
  public String cWithholdingId;
  public String amtsourcecr;
  public String mLocatorId;
  public String adOrgtrxId;
  public String cUomId;
  public String factAcctId;
  public String recordId2;
  public String qty;
  public String postingtype;
  public String cLocfromId;
  public String cActivityId;
  public String glCategoryId;
  public String cCampaignId;
  public String factaccttype;
  public String datetrx;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("record_id") || fieldName.equals("recordId"))
      return recordId;
    else if (fieldName.equalsIgnoreCase("c_acctschema_id") || fieldName.equals("cAcctschemaId"))
      return cAcctschemaId;
    else if (fieldName.equalsIgnoreCase("c_acctschema_idr") || fieldName.equals("cAcctschemaIdr"))
      return cAcctschemaIdr;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("c_period_id") || fieldName.equals("cPeriodId"))
      return cPeriodId;
    else if (fieldName.equalsIgnoreCase("c_period_idr") || fieldName.equals("cPeriodIdr"))
      return cPeriodIdr;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("seqno"))
      return seqno;
    else if (fieldName.equalsIgnoreCase("account_id") || fieldName.equals("accountId"))
      return accountId;
    else if (fieldName.equalsIgnoreCase("account_idr") || fieldName.equals("accountIdr"))
      return accountIdr;
    else if (fieldName.equalsIgnoreCase("acctvalue"))
      return acctvalue;
    else if (fieldName.equalsIgnoreCase("acctdescription"))
      return acctdescription;
    else if (fieldName.equalsIgnoreCase("amtacctdr"))
      return amtacctdr;
    else if (fieldName.equalsIgnoreCase("amtacctcr"))
      return amtacctcr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("m_product_idr") || fieldName.equals("mProductIdr"))
      return mProductIdr;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("c_project_idr") || fieldName.equals("cProjectIdr"))
      return cProjectIdr;
    else if (fieldName.equalsIgnoreCase("c_costcenter_id") || fieldName.equals("cCostcenterId"))
      return cCostcenterId;
    else if (fieldName.equalsIgnoreCase("c_costcenter_idr") || fieldName.equals("cCostcenterIdr"))
      return cCostcenterIdr;
    else if (fieldName.equalsIgnoreCase("a_asset_id") || fieldName.equals("aAssetId"))
      return aAssetId;
    else if (fieldName.equalsIgnoreCase("a_asset_idr") || fieldName.equals("aAssetIdr"))
      return aAssetIdr;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("c_salesregion_id") || fieldName.equals("cSalesregionId"))
      return cSalesregionId;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("docbasetype"))
      return docbasetype;
    else if (fieldName.equalsIgnoreCase("fact_acct_group_id") || fieldName.equals("factAcctGroupId"))
      return factAcctGroupId;
    else if (fieldName.equalsIgnoreCase("c_locto_id") || fieldName.equals("cLoctoId"))
      return cLoctoId;
    else if (fieldName.equalsIgnoreCase("amtsourcedr"))
      return amtsourcedr;
    else if (fieldName.equalsIgnoreCase("c_tax_id") || fieldName.equals("cTaxId"))
      return cTaxId;
    else if (fieldName.equalsIgnoreCase("line_id") || fieldName.equals("lineId"))
      return lineId;
    else if (fieldName.equalsIgnoreCase("ad_table_id") || fieldName.equals("adTableId"))
      return adTableId;
    else if (fieldName.equalsIgnoreCase("c_withholding_id") || fieldName.equals("cWithholdingId"))
      return cWithholdingId;
    else if (fieldName.equalsIgnoreCase("amtsourcecr"))
      return amtsourcecr;
    else if (fieldName.equalsIgnoreCase("m_locator_id") || fieldName.equals("mLocatorId"))
      return mLocatorId;
    else if (fieldName.equalsIgnoreCase("ad_orgtrx_id") || fieldName.equals("adOrgtrxId"))
      return adOrgtrxId;
    else if (fieldName.equalsIgnoreCase("c_uom_id") || fieldName.equals("cUomId"))
      return cUomId;
    else if (fieldName.equalsIgnoreCase("fact_acct_id") || fieldName.equals("factAcctId"))
      return factAcctId;
    else if (fieldName.equalsIgnoreCase("record_id2") || fieldName.equals("recordId2"))
      return recordId2;
    else if (fieldName.equalsIgnoreCase("qty"))
      return qty;
    else if (fieldName.equalsIgnoreCase("postingtype"))
      return postingtype;
    else if (fieldName.equalsIgnoreCase("c_locfrom_id") || fieldName.equals("cLocfromId"))
      return cLocfromId;
    else if (fieldName.equalsIgnoreCase("c_activity_id") || fieldName.equals("cActivityId"))
      return cActivityId;
    else if (fieldName.equalsIgnoreCase("gl_category_id") || fieldName.equals("glCategoryId"))
      return glCategoryId;
    else if (fieldName.equalsIgnoreCase("c_campaign_id") || fieldName.equals("cCampaignId"))
      return cCampaignId;
    else if (fieldName.equalsIgnoreCase("factaccttype"))
      return factaccttype;
    else if (fieldName.equalsIgnoreCase("datetrx"))
      return datetrx;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static AccountingData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static AccountingData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Fact_Acct.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = Fact_Acct.CreatedBy) as CreatedByR, " +
      "        to_char(Fact_Acct.Updated, ?) as updated, " +
      "        to_char(Fact_Acct.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        Fact_Acct.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = Fact_Acct.UpdatedBy) as UpdatedByR," +
      "        Fact_Acct.AD_Client_ID, " +
      "Fact_Acct.AD_Org_ID, " +
      "(CASE WHEN Fact_Acct.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "COALESCE(Fact_Acct.IsActive, 'N') AS IsActive, " +
      "Fact_Acct.Record_ID, " +
      "Fact_Acct.C_AcctSchema_ID, " +
      "(CASE WHEN Fact_Acct.C_AcctSchema_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS C_AcctSchema_IDR, " +
      "Fact_Acct.C_Currency_ID, " +
      "(CASE WHEN Fact_Acct.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "Fact_Acct.C_Period_ID, " +
      "(CASE WHEN Fact_Acct.C_Period_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS C_Period_IDR, " +
      "Fact_Acct.DateAcct, " +
      "Fact_Acct.SeqNo, " +
      "Fact_Acct.Account_ID, " +
      "(CASE WHEN Fact_Acct.Account_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Value), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL5.Name IS NULL THEN TO_CHAR(table5.Name) ELSE TO_CHAR(tableTRL5.Name) END)), ''))),'') ) END) AS Account_IDR, " +
      "Fact_Acct.AcctValue, " +
      "Fact_Acct.AcctDescription, " +
      "Fact_Acct.AmtAcctDr, " +
      "Fact_Acct.AmtAcctCr, " +
      "Fact_Acct.Description, " +
      "Fact_Acct.C_BPartner_ID, " +
      "(CASE WHEN Fact_Acct.C_BPartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'') ) END) AS C_BPartner_IDR, " +
      "Fact_Acct.M_Product_ID, " +
      "(CASE WHEN Fact_Acct.M_Product_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL8.Name IS NULL THEN TO_CHAR(table8.Name) ELSE TO_CHAR(tableTRL8.Name) END)), ''))),'') ) END) AS M_Product_IDR, " +
      "Fact_Acct.C_Project_ID, " +
      "(CASE WHEN Fact_Acct.C_Project_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table10.Value), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table10.Name), ''))),'') ) END) AS C_Project_IDR, " +
      "Fact_Acct.C_Costcenter_ID, " +
      "(CASE WHEN Fact_Acct.C_Costcenter_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table11.Name), ''))),'') ) END) AS C_Costcenter_IDR, " +
      "Fact_Acct.A_Asset_ID, " +
      "(CASE WHEN Fact_Acct.A_Asset_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table12.Name), ''))),'') ) END) AS A_Asset_IDR, " +
      "Fact_Acct.User1_ID, " +
      "Fact_Acct.User2_ID, " +
      "Fact_Acct.C_SalesRegion_ID, " +
      "Fact_Acct.C_Doctype_ID, " +
      "Fact_Acct.DocBaseType, " +
      "Fact_Acct.Fact_Acct_Group_ID, " +
      "Fact_Acct.C_LocTo_ID, " +
      "Fact_Acct.AmtSourceDr, " +
      "Fact_Acct.C_Tax_ID, " +
      "Fact_Acct.Line_ID, " +
      "Fact_Acct.AD_Table_ID, " +
      "Fact_Acct.C_Withholding_ID, " +
      "Fact_Acct.AmtSourceCr, " +
      "Fact_Acct.M_Locator_ID, " +
      "Fact_Acct.AD_OrgTrx_ID, " +
      "Fact_Acct.C_UOM_ID, " +
      "Fact_Acct.Fact_Acct_ID, " +
      "Fact_Acct.Record_ID2, " +
      "Fact_Acct.Qty, " +
      "Fact_Acct.PostingType, " +
      "Fact_Acct.C_LocFrom_ID, " +
      "Fact_Acct.C_Activity_ID, " +
      "Fact_Acct.GL_Category_ID, " +
      "Fact_Acct.C_Campaign_ID, " +
      "Fact_Acct.FactAcctType, " +
      "Fact_Acct.DateTrx, " +
      "        ? AS LANGUAGE " +
      "        FROM Fact_Acct left join (select AD_Org_ID, Name from AD_Org) table1 on (Fact_Acct.AD_Org_ID = table1.AD_Org_ID) left join (select C_AcctSchema_ID, Name from C_AcctSchema) table2 on (Fact_Acct.C_AcctSchema_ID = table2.C_AcctSchema_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table3 on (Fact_Acct.C_Currency_ID = table3.C_Currency_ID) left join (select C_Period_ID, Name from C_Period) table4 on (Fact_Acct.C_Period_ID = table4.C_Period_ID) left join (select C_ElementValue_ID, Value, Name from C_ElementValue) table5 on (Fact_Acct.Account_ID = table5.C_ElementValue_ID) left join (select C_ElementValue_ID,AD_Language, Name from C_ElementValue_TRL) tableTRL5 on (table5.C_ElementValue_ID = tableTRL5.C_ElementValue_ID and tableTRL5.AD_Language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table7 on (Fact_Acct.C_BPartner_ID = table7.C_BPartner_ID) left join (select M_Product_ID, Name from M_Product) table8 on (Fact_Acct.M_Product_ID = table8.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL8 on (table8.M_Product_ID = tableTRL8.M_Product_ID and tableTRL8.AD_Language = ?)  left join (select C_Project_ID, Value, Name from C_Project) table10 on (Fact_Acct.C_Project_ID = table10.C_Project_ID) left join (select C_Costcenter_ID, Name from C_Costcenter) table11 on (Fact_Acct.C_Costcenter_ID = table11.C_Costcenter_ID) left join (select A_Asset_ID, Name from A_Asset) table12 on (Fact_Acct.A_Asset_ID = table12.A_Asset_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND Fact_Acct.Fact_Acct_ID = ? " +
      "        AND Fact_Acct.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND Fact_Acct.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountingData objectAccountingData = new AccountingData();
        objectAccountingData.created = UtilSql.getValue(result, "created");
        objectAccountingData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectAccountingData.updated = UtilSql.getValue(result, "updated");
        objectAccountingData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectAccountingData.updatedby = UtilSql.getValue(result, "updatedby");
        objectAccountingData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectAccountingData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectAccountingData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectAccountingData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectAccountingData.isactive = UtilSql.getValue(result, "isactive");
        objectAccountingData.recordId = UtilSql.getValue(result, "record_id");
        objectAccountingData.cAcctschemaId = UtilSql.getValue(result, "c_acctschema_id");
        objectAccountingData.cAcctschemaIdr = UtilSql.getValue(result, "c_acctschema_idr");
        objectAccountingData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectAccountingData.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectAccountingData.cPeriodId = UtilSql.getValue(result, "c_period_id");
        objectAccountingData.cPeriodIdr = UtilSql.getValue(result, "c_period_idr");
        objectAccountingData.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectAccountingData.seqno = UtilSql.getValue(result, "seqno");
        objectAccountingData.accountId = UtilSql.getValue(result, "account_id");
        objectAccountingData.accountIdr = UtilSql.getValue(result, "account_idr");
        objectAccountingData.acctvalue = UtilSql.getValue(result, "acctvalue");
        objectAccountingData.acctdescription = UtilSql.getValue(result, "acctdescription");
        objectAccountingData.amtacctdr = UtilSql.getValue(result, "amtacctdr");
        objectAccountingData.amtacctcr = UtilSql.getValue(result, "amtacctcr");
        objectAccountingData.description = UtilSql.getValue(result, "description");
        objectAccountingData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectAccountingData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectAccountingData.mProductId = UtilSql.getValue(result, "m_product_id");
        objectAccountingData.mProductIdr = UtilSql.getValue(result, "m_product_idr");
        objectAccountingData.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectAccountingData.cProjectIdr = UtilSql.getValue(result, "c_project_idr");
        objectAccountingData.cCostcenterId = UtilSql.getValue(result, "c_costcenter_id");
        objectAccountingData.cCostcenterIdr = UtilSql.getValue(result, "c_costcenter_idr");
        objectAccountingData.aAssetId = UtilSql.getValue(result, "a_asset_id");
        objectAccountingData.aAssetIdr = UtilSql.getValue(result, "a_asset_idr");
        objectAccountingData.user1Id = UtilSql.getValue(result, "user1_id");
        objectAccountingData.user2Id = UtilSql.getValue(result, "user2_id");
        objectAccountingData.cSalesregionId = UtilSql.getValue(result, "c_salesregion_id");
        objectAccountingData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectAccountingData.docbasetype = UtilSql.getValue(result, "docbasetype");
        objectAccountingData.factAcctGroupId = UtilSql.getValue(result, "fact_acct_group_id");
        objectAccountingData.cLoctoId = UtilSql.getValue(result, "c_locto_id");
        objectAccountingData.amtsourcedr = UtilSql.getValue(result, "amtsourcedr");
        objectAccountingData.cTaxId = UtilSql.getValue(result, "c_tax_id");
        objectAccountingData.lineId = UtilSql.getValue(result, "line_id");
        objectAccountingData.adTableId = UtilSql.getValue(result, "ad_table_id");
        objectAccountingData.cWithholdingId = UtilSql.getValue(result, "c_withholding_id");
        objectAccountingData.amtsourcecr = UtilSql.getValue(result, "amtsourcecr");
        objectAccountingData.mLocatorId = UtilSql.getValue(result, "m_locator_id");
        objectAccountingData.adOrgtrxId = UtilSql.getValue(result, "ad_orgtrx_id");
        objectAccountingData.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectAccountingData.factAcctId = UtilSql.getValue(result, "fact_acct_id");
        objectAccountingData.recordId2 = UtilSql.getValue(result, "record_id2");
        objectAccountingData.qty = UtilSql.getValue(result, "qty");
        objectAccountingData.postingtype = UtilSql.getValue(result, "postingtype");
        objectAccountingData.cLocfromId = UtilSql.getValue(result, "c_locfrom_id");
        objectAccountingData.cActivityId = UtilSql.getValue(result, "c_activity_id");
        objectAccountingData.glCategoryId = UtilSql.getValue(result, "gl_category_id");
        objectAccountingData.cCampaignId = UtilSql.getValue(result, "c_campaign_id");
        objectAccountingData.factaccttype = UtilSql.getValue(result, "factaccttype");
        objectAccountingData.datetrx = UtilSql.getDateValue(result, "datetrx", "dd-MM-yyyy");
        objectAccountingData.language = UtilSql.getValue(result, "language");
        objectAccountingData.adUserClient = "";
        objectAccountingData.adOrgClient = "";
        objectAccountingData.createdby = "";
        objectAccountingData.trBgcolor = "";
        objectAccountingData.totalCount = "";
        objectAccountingData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountingData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountingData objectAccountingData[] = new AccountingData[vector.size()];
    vector.copyInto(objectAccountingData);
    return(objectAccountingData);
  }

/**
Create a registry
 */
  public static AccountingData[] set(String cCostcenterId, String adClientId, String adOrgId, String cAcctschemaId, String glCategoryId, String cPeriodId, String postingtype, String accountId, String accountIdr, String cCurrencyId, String amtsourcedr, String amtsourcecr, String amtacctdr, String amtacctcr, String cUomId, String qty, String mProductId, String mProductIdr, String adOrgtrxId, String cLocfromId, String cLoctoId, String cSalesregionId, String cProjectId, String user1Id, String user2Id, String cCampaignId, String cBpartnerId, String cBpartnerIdr, String factAcctId, String cDoctypeId, String cActivityId, String datetrx, String dateacct, String adTableId, String recordId, String cTaxId, String factAcctGroupId, String seqno, String factaccttype, String docbasetype, String acctvalue, String acctdescription, String recordId2, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String description, String mLocatorId, String lineId, String cWithholdingId, String aAssetId)    throws ServletException {
    AccountingData objectAccountingData[] = new AccountingData[1];
    objectAccountingData[0] = new AccountingData();
    objectAccountingData[0].created = "";
    objectAccountingData[0].createdbyr = createdbyr;
    objectAccountingData[0].updated = "";
    objectAccountingData[0].updatedTimeStamp = "";
    objectAccountingData[0].updatedby = updatedby;
    objectAccountingData[0].updatedbyr = updatedbyr;
    objectAccountingData[0].adClientId = adClientId;
    objectAccountingData[0].adOrgId = adOrgId;
    objectAccountingData[0].adOrgIdr = "";
    objectAccountingData[0].isactive = isactive;
    objectAccountingData[0].recordId = recordId;
    objectAccountingData[0].cAcctschemaId = cAcctschemaId;
    objectAccountingData[0].cAcctschemaIdr = "";
    objectAccountingData[0].cCurrencyId = cCurrencyId;
    objectAccountingData[0].cCurrencyIdr = "";
    objectAccountingData[0].cPeriodId = cPeriodId;
    objectAccountingData[0].cPeriodIdr = "";
    objectAccountingData[0].dateacct = dateacct;
    objectAccountingData[0].seqno = seqno;
    objectAccountingData[0].accountId = accountId;
    objectAccountingData[0].accountIdr = accountIdr;
    objectAccountingData[0].acctvalue = acctvalue;
    objectAccountingData[0].acctdescription = acctdescription;
    objectAccountingData[0].amtacctdr = amtacctdr;
    objectAccountingData[0].amtacctcr = amtacctcr;
    objectAccountingData[0].description = description;
    objectAccountingData[0].cBpartnerId = cBpartnerId;
    objectAccountingData[0].cBpartnerIdr = cBpartnerIdr;
    objectAccountingData[0].mProductId = mProductId;
    objectAccountingData[0].mProductIdr = mProductIdr;
    objectAccountingData[0].cProjectId = cProjectId;
    objectAccountingData[0].cProjectIdr = "";
    objectAccountingData[0].cCostcenterId = cCostcenterId;
    objectAccountingData[0].cCostcenterIdr = "";
    objectAccountingData[0].aAssetId = aAssetId;
    objectAccountingData[0].aAssetIdr = "";
    objectAccountingData[0].user1Id = user1Id;
    objectAccountingData[0].user2Id = user2Id;
    objectAccountingData[0].cSalesregionId = cSalesregionId;
    objectAccountingData[0].cDoctypeId = cDoctypeId;
    objectAccountingData[0].docbasetype = docbasetype;
    objectAccountingData[0].factAcctGroupId = factAcctGroupId;
    objectAccountingData[0].cLoctoId = cLoctoId;
    objectAccountingData[0].amtsourcedr = amtsourcedr;
    objectAccountingData[0].cTaxId = cTaxId;
    objectAccountingData[0].lineId = lineId;
    objectAccountingData[0].adTableId = adTableId;
    objectAccountingData[0].cWithholdingId = cWithholdingId;
    objectAccountingData[0].amtsourcecr = amtsourcecr;
    objectAccountingData[0].mLocatorId = mLocatorId;
    objectAccountingData[0].adOrgtrxId = adOrgtrxId;
    objectAccountingData[0].cUomId = cUomId;
    objectAccountingData[0].factAcctId = factAcctId;
    objectAccountingData[0].recordId2 = recordId2;
    objectAccountingData[0].qty = qty;
    objectAccountingData[0].postingtype = postingtype;
    objectAccountingData[0].cLocfromId = cLocfromId;
    objectAccountingData[0].cActivityId = cActivityId;
    objectAccountingData[0].glCategoryId = glCategoryId;
    objectAccountingData[0].cCampaignId = cCampaignId;
    objectAccountingData[0].factaccttype = factaccttype;
    objectAccountingData[0].datetrx = datetrx;
    objectAccountingData[0].language = "";
    return objectAccountingData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2519_0(ConnectionProvider connectionProvider, String paramLanguage, String Account_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Value), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))), '') ) as Account_ID FROM C_ElementValue left join (select C_ElementValue_ID, Value, Name from C_ElementValue) table2 on (C_ElementValue.C_ElementValue_ID = table2.C_ElementValue_ID)left join (select C_ElementValue_ID,AD_Language, Name from C_ElementValue_TRL) tableTRL2 on (table2.C_ElementValue_ID = tableTRL2.C_ElementValue_ID and tableTRL2.AD_Language = ?)  WHERE C_ElementValue.isActive='Y' AND C_ElementValue.C_ElementValue_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Account_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "account_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2527_1(ConnectionProvider connectionProvider, String paramLanguage, String M_Product_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))), '') ) as M_Product_ID FROM M_Product left join (select M_Product_ID, Name from M_Product) table2 on (M_Product.M_Product_ID = table2.M_Product_ID)left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL2 on (table2.M_Product_ID = tableTRL2.M_Product_ID and tableTRL2.AD_Language = ?)  WHERE M_Product.isActive='Y' AND M_Product.M_Product_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_Product_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_product_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2768_2(ConnectionProvider connectionProvider, String C_BPartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_BPartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_BPartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef5852_3(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef5854_4(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE Fact_Acct" +
      "        SET AD_Client_ID = (?) , AD_Org_ID = (?) , IsActive = (?) , Record_ID = (?) , C_AcctSchema_ID = (?) , C_Currency_ID = (?) , C_Period_ID = (?) , DateAcct = TO_DATE(?) , SeqNo = TO_NUMBER(?) , Account_ID = (?) , AcctValue = (?) , AcctDescription = (?) , AmtAcctDr = TO_NUMBER(?) , AmtAcctCr = TO_NUMBER(?) , Description = (?) , C_BPartner_ID = (?) , M_Product_ID = (?) , C_Project_ID = (?) , C_Costcenter_ID = (?) , A_Asset_ID = (?) , User1_ID = (?) , User2_ID = (?) , C_SalesRegion_ID = (?) , C_Doctype_ID = (?) , DocBaseType = (?) , Fact_Acct_Group_ID = (?) , C_LocTo_ID = (?) , AmtSourceDr = TO_NUMBER(?) , C_Tax_ID = (?) , Line_ID = (?) , AD_Table_ID = (?) , C_Withholding_ID = (?) , AmtSourceCr = TO_NUMBER(?) , M_Locator_ID = (?) , AD_OrgTrx_ID = (?) , C_UOM_ID = (?) , Fact_Acct_ID = (?) , Record_ID2 = (?) , Qty = TO_NUMBER(?) , PostingType = (?) , C_LocFrom_ID = (?) , C_Activity_ID = (?) , GL_Category_ID = (?) , C_Campaign_ID = (?) , FactAcctType = (?) , DateTrx = TO_DATE(?) , updated = now(), updatedby = ? " +
      "        WHERE Fact_Acct.Fact_Acct_ID = ? " +
      "        AND Fact_Acct.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND Fact_Acct.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, recordId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cAcctschemaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, seqno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, accountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctvalue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtacctdr);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtacctcr);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cSalesregionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docbasetype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factAcctGroupId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cLoctoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtsourcedr);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cWithholdingId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtsourcecr);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factAcctId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, recordId2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qty);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, postingtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cLocfromId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, glCategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factaccttype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datetrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factAcctId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO Fact_Acct " +
      "        (AD_Client_ID, AD_Org_ID, IsActive, Record_ID, C_AcctSchema_ID, C_Currency_ID, C_Period_ID, DateAcct, SeqNo, Account_ID, AcctValue, AcctDescription, AmtAcctDr, AmtAcctCr, Description, C_BPartner_ID, M_Product_ID, C_Project_ID, C_Costcenter_ID, A_Asset_ID, User1_ID, User2_ID, C_SalesRegion_ID, C_Doctype_ID, DocBaseType, Fact_Acct_Group_ID, C_LocTo_ID, AmtSourceDr, C_Tax_ID, Line_ID, AD_Table_ID, C_Withholding_ID, AmtSourceCr, M_Locator_ID, AD_OrgTrx_ID, C_UOM_ID, Fact_Acct_ID, Record_ID2, Qty, PostingType, C_LocFrom_ID, C_Activity_ID, GL_Category_ID, C_Campaign_ID, FactAcctType, DateTrx, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), TO_DATE(?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), TO_DATE(?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, recordId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cAcctschemaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, seqno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, accountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctvalue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtacctdr);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtacctcr);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cSalesregionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docbasetype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factAcctGroupId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cLoctoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtsourcedr);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cWithholdingId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtsourcecr);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factAcctId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, recordId2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qty);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, postingtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cLocfromId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, glCategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factaccttype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datetrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM Fact_Acct" +
      "        WHERE Fact_Acct.Fact_Acct_ID = ? " +
      "        AND Fact_Acct.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND Fact_Acct.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM Fact_Acct" +
      "         WHERE Fact_Acct.Fact_Acct_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM Fact_Acct" +
      "         WHERE Fact_Acct.Fact_Acct_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}

//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.FacturaCliente;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Retencion8C55E1F8274242628B244CF04FBD3CB9Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Retencion8C55E1F8274242628B244CF04FBD3CB9Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cInvoiceId;
  public String cInvoiceIdr;
  public String documentno;
  public String noAutorizacion;
  public String fechaEmision;
  public String tipoComprobanteVenta;
  public String tipoComprobanteVentar;
  public String noComprobanteVenta;
  public String cPeriodId;
  public String cPeriodIdr;
  public String totalRetencion;
  public String processed;
  public String isactive;
  public String coNoEstablecimiento;
  public String coPtoEmision;
  public String docstatus;
  public String docstatusr;
  public String dateacct;
  public String posted;
  public String postedBtn;
  public String docactionre;
  public String docactionreBtn;
  public String processing;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String cDoctypetargetId;
  public String cDoctypetargetIdr;
  public String emAteccoCOrderId;
  public String emAteccoCOrderIdr;
  public String adClientId;
  public String coRetencionVentaId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_invoice_id") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("c_invoice_idr") || fieldName.equals("cInvoiceIdr"))
      return cInvoiceIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("no_autorizacion") || fieldName.equals("noAutorizacion"))
      return noAutorizacion;
    else if (fieldName.equalsIgnoreCase("fecha_emision") || fieldName.equals("fechaEmision"))
      return fechaEmision;
    else if (fieldName.equalsIgnoreCase("tipo_comprobante_venta") || fieldName.equals("tipoComprobanteVenta"))
      return tipoComprobanteVenta;
    else if (fieldName.equalsIgnoreCase("tipo_comprobante_ventar") || fieldName.equals("tipoComprobanteVentar"))
      return tipoComprobanteVentar;
    else if (fieldName.equalsIgnoreCase("no_comprobante_venta") || fieldName.equals("noComprobanteVenta"))
      return noComprobanteVenta;
    else if (fieldName.equalsIgnoreCase("c_period_id") || fieldName.equals("cPeriodId"))
      return cPeriodId;
    else if (fieldName.equalsIgnoreCase("c_period_idr") || fieldName.equals("cPeriodIdr"))
      return cPeriodIdr;
    else if (fieldName.equalsIgnoreCase("total_retencion") || fieldName.equals("totalRetencion"))
      return totalRetencion;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("co_no_establecimiento") || fieldName.equals("coNoEstablecimiento"))
      return coNoEstablecimiento;
    else if (fieldName.equalsIgnoreCase("co_pto_emision") || fieldName.equals("coPtoEmision"))
      return coPtoEmision;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("posted_btn") || fieldName.equals("postedBtn"))
      return postedBtn;
    else if (fieldName.equalsIgnoreCase("docactionre"))
      return docactionre;
    else if (fieldName.equalsIgnoreCase("docactionre_btn") || fieldName.equals("docactionreBtn"))
      return docactionreBtn;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_id") || fieldName.equals("cDoctypetargetId"))
      return cDoctypetargetId;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_idr") || fieldName.equals("cDoctypetargetIdr"))
      return cDoctypetargetIdr;
    else if (fieldName.equalsIgnoreCase("em_atecco_c_order_id") || fieldName.equals("emAteccoCOrderId"))
      return emAteccoCOrderId;
    else if (fieldName.equalsIgnoreCase("em_atecco_c_order_idr") || fieldName.equals("emAteccoCOrderIdr"))
      return emAteccoCOrderIdr;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("co_retencion_venta_id") || fieldName.equals("coRetencionVentaId"))
      return coRetencionVentaId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Retencion8C55E1F8274242628B244CF04FBD3CB9Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Retencion8C55E1F8274242628B244CF04FBD3CB9Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(CO_RETENCION_VENTA.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = CO_RETENCION_VENTA.CreatedBy) as CreatedByR, " +
      "        to_char(CO_RETENCION_VENTA.Updated, ?) as updated, " +
      "        to_char(CO_RETENCION_VENTA.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        CO_RETENCION_VENTA.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = CO_RETENCION_VENTA.UpdatedBy) as UpdatedByR," +
      "        CO_RETENCION_VENTA.AD_Org_ID, " +
      "(CASE WHEN CO_RETENCION_VENTA.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "CO_RETENCION_VENTA.C_Invoice_ID, " +
      "(CASE WHEN CO_RETENCION_VENTA.C_Invoice_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.DocumentNo), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table2.DateInvoiced, 'DD-MM-YYYY')),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.GrandTotal), ''))),'') ) END) AS C_Invoice_IDR, " +
      "CO_RETENCION_VENTA.DocumentNo, " +
      "CO_RETENCION_VENTA.NO_Autorizacion, " +
      "CO_RETENCION_VENTA.Fecha_Emision, " +
      "CO_RETENCION_VENTA.Tipo_Comprobante_Venta, " +
      "(CASE WHEN CO_RETENCION_VENTA.Tipo_Comprobante_Venta IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Tipo_Comprobante_VentaR, " +
      "CO_RETENCION_VENTA.NO_Comprobante_Venta, " +
      "CO_RETENCION_VENTA.C_Period_ID, " +
      "(CASE WHEN CO_RETENCION_VENTA.C_Period_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS C_Period_IDR, " +
      "CO_RETENCION_VENTA.Total_Retencion, " +
      "COALESCE(CO_RETENCION_VENTA.Processed, 'N') AS Processed, " +
      "COALESCE(CO_RETENCION_VENTA.Isactive, 'N') AS Isactive, " +
      "CO_RETENCION_VENTA.CO_No_Establecimiento, " +
      "CO_RETENCION_VENTA.CO_Pto_Emision, " +
      "CO_RETENCION_VENTA.Docstatus, " +
      "(CASE WHEN CO_RETENCION_VENTA.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS DocstatusR, " +
      "CO_RETENCION_VENTA.Dateacct, " +
      "CO_RETENCION_VENTA.Posted, " +
      "list3.name as Posted_BTN, " +
      "CO_RETENCION_VENTA.Docactionre, " +
      "list4.name as Docactionre_BTN, " +
      "CO_RETENCION_VENTA.Processing, " +
      "CO_RETENCION_VENTA.C_Doctype_ID, " +
      "(CASE WHEN CO_RETENCION_VENTA.C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL4.Name IS NULL THEN TO_CHAR(table4.Name) ELSE TO_CHAR(tableTRL4.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "CO_RETENCION_VENTA.C_Doctypetarget_ID, " +
      "(CASE WHEN CO_RETENCION_VENTA.C_Doctypetarget_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL6.Name IS NULL THEN TO_CHAR(table6.Name) ELSE TO_CHAR(tableTRL6.Name) END)), ''))),'') ) END) AS C_Doctypetarget_IDR, " +
      "CO_RETENCION_VENTA.EM_Atecco_C_Order_ID, " +
      "(CASE WHEN CO_RETENCION_VENTA.EM_Atecco_C_Order_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.DocumentNo), ''))),'') ) END) AS EM_Atecco_C_Order_IDR, " +
      "CO_RETENCION_VENTA.AD_Client_ID, " +
      "CO_RETENCION_VENTA.CO_Retencion_Venta_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM CO_RETENCION_VENTA left join (select AD_Org_ID, Name from AD_Org) table1 on (CO_RETENCION_VENTA.AD_Org_ID = table1.AD_Org_ID) left join (select C_Invoice_ID, DocumentNo, DateInvoiced, GrandTotal from C_Invoice) table2 on (CO_RETENCION_VENTA.C_Invoice_ID = table2.C_Invoice_ID) left join ad_ref_list_v list1 on (CO_RETENCION_VENTA.Tipo_Comprobante_Venta = list1.value and list1.ad_reference_id = '94DD3D9C266148BEAE4E201BD84F8F76' and list1.ad_language = ?)  left join (select C_Period_ID, Name from C_Period) table3 on (CO_RETENCION_VENTA.C_Period_ID =  table3.C_Period_ID) left join ad_ref_list_v list2 on (CO_RETENCION_VENTA.Docstatus = list2.value and list2.ad_reference_id = 'CB29EF103ACC49108693B711ACEF6261' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (list3.ad_reference_id = '234' and list3.ad_language = ?  AND CO_RETENCION_VENTA.Posted = TO_CHAR(list3.value)) left join ad_ref_list_v list4 on (list4.ad_reference_id = 'CB29EF103ACC49108693B711ACEF6261' and list4.ad_language = ?  AND CO_RETENCION_VENTA.Docactionre = TO_CHAR(list4.value)) left join (select C_DocType_ID, Name from C_DocType) table4 on (CO_RETENCION_VENTA.C_Doctype_ID =  table4.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL4 on (table4.C_DocType_ID = tableTRL4.C_DocType_ID and tableTRL4.AD_Language = ?)  left join (select C_DocType_ID, Name from C_DocType) table6 on (CO_RETENCION_VENTA.C_Doctypetarget_ID =  table6.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL6 on (table6.C_DocType_ID = tableTRL6.C_DocType_ID and tableTRL6.AD_Language = ?)  left join (select C_Order_ID, DocumentNo from C_Order) table8 on (CO_RETENCION_VENTA.EM_Atecco_C_Order_ID =  table8.C_Order_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? " +
      "        AND CO_RETENCION_VENTA.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND CO_RETENCION_VENTA.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Retencion8C55E1F8274242628B244CF04FBD3CB9Data objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data = new Retencion8C55E1F8274242628B244CF04FBD3CB9Data();
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.created = UtilSql.getValue(result, "created");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.updated = UtilSql.getValue(result, "updated");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.cInvoiceIdr = UtilSql.getValue(result, "c_invoice_idr");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.documentno = UtilSql.getValue(result, "documentno");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.noAutorizacion = UtilSql.getValue(result, "no_autorizacion");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.fechaEmision = UtilSql.getDateValue(result, "fecha_emision", "dd-MM-yyyy");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.tipoComprobanteVenta = UtilSql.getValue(result, "tipo_comprobante_venta");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.tipoComprobanteVentar = UtilSql.getValue(result, "tipo_comprobante_ventar");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.noComprobanteVenta = UtilSql.getValue(result, "no_comprobante_venta");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.cPeriodId = UtilSql.getValue(result, "c_period_id");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.cPeriodIdr = UtilSql.getValue(result, "c_period_idr");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.totalRetencion = UtilSql.getValue(result, "total_retencion");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.processed = UtilSql.getValue(result, "processed");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.isactive = UtilSql.getValue(result, "isactive");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.coNoEstablecimiento = UtilSql.getValue(result, "co_no_establecimiento");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.coPtoEmision = UtilSql.getValue(result, "co_pto_emision");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.posted = UtilSql.getValue(result, "posted");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.postedBtn = UtilSql.getValue(result, "posted_btn");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.docactionre = UtilSql.getValue(result, "docactionre");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.docactionreBtn = UtilSql.getValue(result, "docactionre_btn");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.processing = UtilSql.getValue(result, "processing");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.cDoctypetargetId = UtilSql.getValue(result, "c_doctypetarget_id");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.cDoctypetargetIdr = UtilSql.getValue(result, "c_doctypetarget_idr");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.emAteccoCOrderId = UtilSql.getValue(result, "em_atecco_c_order_id");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.emAteccoCOrderIdr = UtilSql.getValue(result, "em_atecco_c_order_idr");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.coRetencionVentaId = UtilSql.getValue(result, "co_retencion_venta_id");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.language = UtilSql.getValue(result, "language");
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.adUserClient = "";
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.adOrgClient = "";
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.createdby = "";
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.trBgcolor = "";
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.totalCount = "";
        objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Retencion8C55E1F8274242628B244CF04FBD3CB9Data objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[] = new Retencion8C55E1F8274242628B244CF04FBD3CB9Data[vector.size()];
    vector.copyInto(objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data);
    return(objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data);
  }

/**
Create a registry
 */
  public static Retencion8C55E1F8274242628B244CF04FBD3CB9Data[] set(String cDoctypetargetId, String processed, String docstatus, String coNoEstablecimiento, String createdby, String createdbyr, String fechaEmision, String posted, String postedBtn, String dateacct, String noComprobanteVenta, String updatedby, String updatedbyr, String cInvoiceId, String cInvoiceIdr, String isactive, String emAteccoCOrderId, String documentno, String docactionre, String docactionreBtn, String adOrgId, String processing, String cPeriodId, String adClientId, String coRetencionVentaId, String tipoComprobanteVenta, String coPtoEmision, String totalRetencion, String noAutorizacion, String cDoctypeId)    throws ServletException {
    Retencion8C55E1F8274242628B244CF04FBD3CB9Data objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[] = new Retencion8C55E1F8274242628B244CF04FBD3CB9Data[1];
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0] = new Retencion8C55E1F8274242628B244CF04FBD3CB9Data();
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].created = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].createdbyr = createdbyr;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].updated = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].updatedTimeStamp = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].updatedby = updatedby;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].updatedbyr = updatedbyr;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].adOrgId = adOrgId;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].adOrgIdr = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].cInvoiceId = cInvoiceId;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].cInvoiceIdr = cInvoiceIdr;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].documentno = documentno;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].noAutorizacion = noAutorizacion;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].fechaEmision = fechaEmision;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].tipoComprobanteVenta = tipoComprobanteVenta;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].tipoComprobanteVentar = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].noComprobanteVenta = noComprobanteVenta;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].cPeriodId = cPeriodId;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].cPeriodIdr = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].totalRetencion = totalRetencion;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].processed = processed;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].isactive = isactive;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].coNoEstablecimiento = coNoEstablecimiento;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].coPtoEmision = coPtoEmision;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].docstatus = docstatus;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].docstatusr = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].dateacct = dateacct;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].posted = posted;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].postedBtn = postedBtn;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].docactionre = docactionre;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].docactionreBtn = docactionreBtn;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].processing = processing;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].cDoctypeId = cDoctypeId;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].cDoctypeIdr = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].cDoctypetargetId = cDoctypetargetId;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].cDoctypetargetIdr = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].emAteccoCOrderId = emAteccoCOrderId;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].emAteccoCOrderIdr = "";
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].adClientId = adClientId;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].coRetencionVentaId = coRetencionVentaId;
    objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data[0].language = "";
    return objectRetencion8C55E1F8274242628B244CF04FBD3CB9Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef180DACC7A8C64503904FDC79FAEC513E(ConnectionProvider connectionProvider, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_nro_estab,'001') AS DefaultValue from c_bpartner where c_bpartner_id in (select c_bpartner_id from c_invoice where (c_invoice_id = ?)) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef28707085D03D4CA593A06A750114DEEE_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef399EF229625B47D890FE7831F76DB253(ConnectionProvider connectionProvider, String AD_CLIENT_ID, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select coalesce(em_co_nro_estab,'') || '-' || coalesce(em_co_punto_emision,'') || '-' || documentno as defaultValue from c_invoice where ad_client_id = ? and c_invoice_id = ? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef4D49B91918F34C47854FE88F1A8C4FAD_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef672856CB1CE8445AAD110D27B811B470(ConnectionProvider connectionProvider, String c_invoice_id, String EM_ATECCO_c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select c_invoice_id AS DefaultValue from c_invoice where (c_invoice_id = ? OR c_invoice_id = ?) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, EM_ATECCO_c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef672856CB1CE8445AAD110D27B811B470_2(ConnectionProvider connectionProvider, String C_Invoice_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.DocumentNo), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table2.DateInvoiced, 'DD-MM-YYYY')), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.GrandTotal), ''))), '') ) as C_Invoice_ID FROM C_Invoice left join (select C_Invoice_ID, DocumentNo, DateInvoiced, GrandTotal from C_Invoice) table2 on (C_Invoice.C_Invoice_ID = table2.C_Invoice_ID) WHERE C_Invoice.isActive='Y' AND C_Invoice.C_Invoice_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Invoice_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_invoice_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefAC226843EA104F4BA2739C3A52AF4312(ConnectionProvider connectionProvider, String AD_ORG_ID, String AD_CLIENT_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT P.C_PERIOD_ID AS DEFAULTVALUE FROM C_PERIOD P WHERE EXISTS (SELECT * FROM C_PERIODCONTROL PC WHERE P.C_PERIOD_ID=PC.C_PERIOD_ID AND UPPER(PC.PERIODSTATUS)='O') AND EXISTS(SELECT * FROM C_CALENDAR C, C_YEAR Y WHERE Y.C_CALENDAR_ID=C.C_CALENDAR_ID AND P.C_YEAR_ID=Y.C_YEAR_ID AND AD_ISORGINCLUDED(?, C.AD_ORG_ID, ?)<> -1) AND P.AD_CLIENT_ID=? AND NOW() BETWEEN STARTDATE AND ENDDATE ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefEAB8C210B6D541E68296959AC3104EE1(ConnectionProvider connectionProvider, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_punto_emision,'001') AS DefaultValue from c_bpartner where c_bpartner_id in (select c_bpartner_id from c_invoice where c_invoice_id = ?) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefFA8A7F8EBC774ABFBCF7B50F8719DFA0(ConnectionProvider connectionProvider, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_nro_aut_rt_sri,'') AS DefaultValue from c_bpartner  where c_bpartner_id in (select c_bpartner_id from c_invoice where c_invoice_id = ?) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE CO_RETENCION_VENTA" +
      "        SET AD_Org_ID = (?) , C_Invoice_ID = (?) , DocumentNo = (?) , NO_Autorizacion = (?) , Fecha_Emision = TO_DATE(?) , Tipo_Comprobante_Venta = (?) , NO_Comprobante_Venta = (?) , C_Period_ID = (?) , Total_Retencion = TO_NUMBER(?) , Processed = (?) , Isactive = (?) , CO_No_Establecimiento = (?) , CO_Pto_Emision = (?) , Docstatus = (?) , Dateacct = TO_DATE(?) , Posted = (?) , Docactionre = (?) , Processing = (?) , C_Doctype_ID = (?) , C_Doctypetarget_ID = (?) , EM_Atecco_C_Order_ID = (?) , AD_Client_ID = (?) , CO_Retencion_Venta_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? " +
      "        AND CO_RETENCION_VENTA.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND CO_RETENCION_VENTA.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRetencion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coNoEstablecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coPtoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO CO_RETENCION_VENTA " +
      "        (AD_Org_ID, C_Invoice_ID, DocumentNo, NO_Autorizacion, Fecha_Emision, Tipo_Comprobante_Venta, NO_Comprobante_Venta, C_Period_ID, Total_Retencion, Processed, Isactive, CO_No_Establecimiento, CO_Pto_Emision, Docstatus, Dateacct, Posted, Docactionre, Processing, C_Doctype_ID, C_Doctypetarget_ID, EM_Atecco_C_Order_ID, AD_Client_ID, CO_Retencion_Venta_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), TO_DATE(?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRetencion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coNoEstablecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coPtoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM CO_RETENCION_VENTA" +
      "        WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? " +
      "        AND CO_RETENCION_VENTA.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND CO_RETENCION_VENTA.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM CO_RETENCION_VENTA" +
      "         WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM CO_RETENCION_VENTA" +
      "         WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}

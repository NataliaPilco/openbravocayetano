//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atums.decoraciones.PedidoCliente;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class PedidoCliente294F4FDD3CB849138A093832B628F92FData implements FieldProvider {
static Logger log4j = Logger.getLogger(PedidoCliente294F4FDD3CB849138A093832B628F92FData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String dateordered;
  public String emDecTipoEvento;
  public String emDecTipoEventor;
  public String cBpartnerLocationId;
  public String cBpartnerLocationIdr;
  public String description;
  public String datepromised;
  public String emDecTipoContratacion;
  public String emDecTipoContratacionr;
  public String emDecFechaMontaje;
  public String emDecHoraMontaje;
  public String emDecFechaDesmontaje;
  public String emDecHoraDesmontaje;
  public String emDecRequiereTransporte;
  public String emDecRequiereMontaje;
  public String salesrepId;
  public String salesrepIdr;
  public String dateacct;
  public String billtoId;
  public String billtoIdr;
  public String poreference;
  public String isdiscountprinted;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String paymentrule;
  public String paymentruler;
  public String cPaymenttermId;
  public String cPaymenttermIdr;
  public String invoicerule;
  public String invoiceruler;
  public String deliveryrule;
  public String deliveryruler;
  public String freightcostrule;
  public String freightcostruler;
  public String freightamt;
  public String deliveryviarule;
  public String deliveryviaruler;
  public String mShipperId;
  public String mShipperIdr;
  public String cChargeId;
  public String cChargeIdr;
  public String chargeamt;
  public String priorityrule;
  public String priorityruler;
  public String totallines;
  public String grandtotal;
  public String mWarehouseId;
  public String mPricelistId;
  public String mPricelistIdr;
  public String istaxincluded;
  public String cCampaignId;
  public String cCampaignIdr;
  public String cProjectId;
  public String cProjectIdr;
  public String cActivityId;
  public String cActivityIdr;
  public String posted;
  public String postedBtn;
  public String adUserId;
  public String adUserIdr;
  public String copyfrom;
  public String dropshipBpartnerId;
  public String dropshipLocationId;
  public String dropshipLocationIdr;
  public String dropshipUserId;
  public String dropshipUserIdr;
  public String isselfservice;
  public String adOrgtrxId;
  public String adOrgtrxIdr;
  public String user1Id;
  public String user2Id;
  public String deliverynotes;
  public String cIncotermsId;
  public String cIncotermsIdr;
  public String incotermsdescription;
  public String generatetemplate;
  public String deliveryLocationId;
  public String deliveryLocationIdr;
  public String copyfrompo;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String finPaymentPriorityId;
  public String finPaymentPriorityIdr;
  public String rmPickfromshipment;
  public String rmReceivematerials;
  public String rmCreateinvoice;
  public String cReturnReasonId;
  public String cReturnReasonIdr;
  public String rmAddorphanline;
  public String aAssetId;
  public String calculatePromotions;
  public String cCostcenterId;
  public String convertquotation;
  public String cRejectReasonId;
  public String cRejectReasonIdr;
  public String validuntil;
  public String quotationId;
  public String quotationIdr;
  public String soResStatus;
  public String soResStatusr;
  public String createPolines;
  public String iscashvat;
  public String rmPickfromreceipt;
  public String emAprmAddpayment;
  public String emAteccoFechaPago;
  public String emAteccoBorra1;
  public String emAteccoBorra2;
  public String emAteccoTransporte;
  public String emAteccoDocaction;
  public String emAteccoDocactionBtn;
  public String emAteccoImprimir;
  public String emAteccoTipoTarjeta;
  public String emAteccoTipoTarjetar;
  public String emAteccoBanco;
  public String emAteccoBancor;
  public String emAteccoDocstatus;
  public String emAteccoDocstatusr;
  public String emAteccoIniciar;
  public String emAteccoIniciarBtn;
  public String emAteccoProcesar;
  public String emAteccoProcesarBtn;
  public String emAteccoValidar;
  public String emAteccoPagodescuento;
  public String emAteccoAnticipo;
  public String emAteccoAnticipostatus;
  public String emAteccoAnticipostatusr;
  public String dateprinted;
  public String cDoctypetargetId;
  public String isdelivered;
  public String isinvoiced;
  public String isprinted;
  public String isselected;
  public String docstatus;
  public String issotrx;
  public String adClientId;
  public String isactive;
  public String documentno;
  public String adOrgId;
  public String docaction;
  public String processing;
  public String cOrderId;
  public String processed;
  public String cDoctypeId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("dateordered"))
      return dateordered;
    else if (fieldName.equalsIgnoreCase("em_dec_tipo_evento") || fieldName.equals("emDecTipoEvento"))
      return emDecTipoEvento;
    else if (fieldName.equalsIgnoreCase("em_dec_tipo_eventor") || fieldName.equals("emDecTipoEventor"))
      return emDecTipoEventor;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_idr") || fieldName.equals("cBpartnerLocationIdr"))
      return cBpartnerLocationIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("datepromised"))
      return datepromised;
    else if (fieldName.equalsIgnoreCase("em_dec_tipo_contratacion") || fieldName.equals("emDecTipoContratacion"))
      return emDecTipoContratacion;
    else if (fieldName.equalsIgnoreCase("em_dec_tipo_contratacionr") || fieldName.equals("emDecTipoContratacionr"))
      return emDecTipoContratacionr;
    else if (fieldName.equalsIgnoreCase("em_dec_fecha_montaje") || fieldName.equals("emDecFechaMontaje"))
      return emDecFechaMontaje;
    else if (fieldName.equalsIgnoreCase("em_dec_hora_montaje") || fieldName.equals("emDecHoraMontaje"))
      return emDecHoraMontaje;
    else if (fieldName.equalsIgnoreCase("em_dec_fecha_desmontaje") || fieldName.equals("emDecFechaDesmontaje"))
      return emDecFechaDesmontaje;
    else if (fieldName.equalsIgnoreCase("em_dec_hora_desmontaje") || fieldName.equals("emDecHoraDesmontaje"))
      return emDecHoraDesmontaje;
    else if (fieldName.equalsIgnoreCase("em_dec_requiere_transporte") || fieldName.equals("emDecRequiereTransporte"))
      return emDecRequiereTransporte;
    else if (fieldName.equalsIgnoreCase("em_dec_requiere_montaje") || fieldName.equals("emDecRequiereMontaje"))
      return emDecRequiereMontaje;
    else if (fieldName.equalsIgnoreCase("salesrep_id") || fieldName.equals("salesrepId"))
      return salesrepId;
    else if (fieldName.equalsIgnoreCase("salesrep_idr") || fieldName.equals("salesrepIdr"))
      return salesrepIdr;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("billto_id") || fieldName.equals("billtoId"))
      return billtoId;
    else if (fieldName.equalsIgnoreCase("billto_idr") || fieldName.equals("billtoIdr"))
      return billtoIdr;
    else if (fieldName.equalsIgnoreCase("poreference"))
      return poreference;
    else if (fieldName.equalsIgnoreCase("isdiscountprinted"))
      return isdiscountprinted;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("paymentrule"))
      return paymentrule;
    else if (fieldName.equalsIgnoreCase("paymentruler"))
      return paymentruler;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_id") || fieldName.equals("cPaymenttermId"))
      return cPaymenttermId;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_idr") || fieldName.equals("cPaymenttermIdr"))
      return cPaymenttermIdr;
    else if (fieldName.equalsIgnoreCase("invoicerule"))
      return invoicerule;
    else if (fieldName.equalsIgnoreCase("invoiceruler"))
      return invoiceruler;
    else if (fieldName.equalsIgnoreCase("deliveryrule"))
      return deliveryrule;
    else if (fieldName.equalsIgnoreCase("deliveryruler"))
      return deliveryruler;
    else if (fieldName.equalsIgnoreCase("freightcostrule"))
      return freightcostrule;
    else if (fieldName.equalsIgnoreCase("freightcostruler"))
      return freightcostruler;
    else if (fieldName.equalsIgnoreCase("freightamt"))
      return freightamt;
    else if (fieldName.equalsIgnoreCase("deliveryviarule"))
      return deliveryviarule;
    else if (fieldName.equalsIgnoreCase("deliveryviaruler"))
      return deliveryviaruler;
    else if (fieldName.equalsIgnoreCase("m_shipper_id") || fieldName.equals("mShipperId"))
      return mShipperId;
    else if (fieldName.equalsIgnoreCase("m_shipper_idr") || fieldName.equals("mShipperIdr"))
      return mShipperIdr;
    else if (fieldName.equalsIgnoreCase("c_charge_id") || fieldName.equals("cChargeId"))
      return cChargeId;
    else if (fieldName.equalsIgnoreCase("c_charge_idr") || fieldName.equals("cChargeIdr"))
      return cChargeIdr;
    else if (fieldName.equalsIgnoreCase("chargeamt"))
      return chargeamt;
    else if (fieldName.equalsIgnoreCase("priorityrule"))
      return priorityrule;
    else if (fieldName.equalsIgnoreCase("priorityruler"))
      return priorityruler;
    else if (fieldName.equalsIgnoreCase("totallines"))
      return totallines;
    else if (fieldName.equalsIgnoreCase("grandtotal"))
      return grandtotal;
    else if (fieldName.equalsIgnoreCase("m_warehouse_id") || fieldName.equals("mWarehouseId"))
      return mWarehouseId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_idr") || fieldName.equals("mPricelistIdr"))
      return mPricelistIdr;
    else if (fieldName.equalsIgnoreCase("istaxincluded"))
      return istaxincluded;
    else if (fieldName.equalsIgnoreCase("c_campaign_id") || fieldName.equals("cCampaignId"))
      return cCampaignId;
    else if (fieldName.equalsIgnoreCase("c_campaign_idr") || fieldName.equals("cCampaignIdr"))
      return cCampaignIdr;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("c_project_idr") || fieldName.equals("cProjectIdr"))
      return cProjectIdr;
    else if (fieldName.equalsIgnoreCase("c_activity_id") || fieldName.equals("cActivityId"))
      return cActivityId;
    else if (fieldName.equalsIgnoreCase("c_activity_idr") || fieldName.equals("cActivityIdr"))
      return cActivityIdr;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("posted_btn") || fieldName.equals("postedBtn"))
      return postedBtn;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("ad_user_idr") || fieldName.equals("adUserIdr"))
      return adUserIdr;
    else if (fieldName.equalsIgnoreCase("copyfrom"))
      return copyfrom;
    else if (fieldName.equalsIgnoreCase("dropship_bpartner_id") || fieldName.equals("dropshipBpartnerId"))
      return dropshipBpartnerId;
    else if (fieldName.equalsIgnoreCase("dropship_location_id") || fieldName.equals("dropshipLocationId"))
      return dropshipLocationId;
    else if (fieldName.equalsIgnoreCase("dropship_location_idr") || fieldName.equals("dropshipLocationIdr"))
      return dropshipLocationIdr;
    else if (fieldName.equalsIgnoreCase("dropship_user_id") || fieldName.equals("dropshipUserId"))
      return dropshipUserId;
    else if (fieldName.equalsIgnoreCase("dropship_user_idr") || fieldName.equals("dropshipUserIdr"))
      return dropshipUserIdr;
    else if (fieldName.equalsIgnoreCase("isselfservice"))
      return isselfservice;
    else if (fieldName.equalsIgnoreCase("ad_orgtrx_id") || fieldName.equals("adOrgtrxId"))
      return adOrgtrxId;
    else if (fieldName.equalsIgnoreCase("ad_orgtrx_idr") || fieldName.equals("adOrgtrxIdr"))
      return adOrgtrxIdr;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("deliverynotes"))
      return deliverynotes;
    else if (fieldName.equalsIgnoreCase("c_incoterms_id") || fieldName.equals("cIncotermsId"))
      return cIncotermsId;
    else if (fieldName.equalsIgnoreCase("c_incoterms_idr") || fieldName.equals("cIncotermsIdr"))
      return cIncotermsIdr;
    else if (fieldName.equalsIgnoreCase("incotermsdescription"))
      return incotermsdescription;
    else if (fieldName.equalsIgnoreCase("generatetemplate"))
      return generatetemplate;
    else if (fieldName.equalsIgnoreCase("delivery_location_id") || fieldName.equals("deliveryLocationId"))
      return deliveryLocationId;
    else if (fieldName.equalsIgnoreCase("delivery_location_idr") || fieldName.equals("deliveryLocationIdr"))
      return deliveryLocationIdr;
    else if (fieldName.equalsIgnoreCase("copyfrompo"))
      return copyfrompo;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_idr") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("fin_payment_priority_id") || fieldName.equals("finPaymentPriorityId"))
      return finPaymentPriorityId;
    else if (fieldName.equalsIgnoreCase("fin_payment_priority_idr") || fieldName.equals("finPaymentPriorityIdr"))
      return finPaymentPriorityIdr;
    else if (fieldName.equalsIgnoreCase("rm_pickfromshipment") || fieldName.equals("rmPickfromshipment"))
      return rmPickfromshipment;
    else if (fieldName.equalsIgnoreCase("rm_receivematerials") || fieldName.equals("rmReceivematerials"))
      return rmReceivematerials;
    else if (fieldName.equalsIgnoreCase("rm_createinvoice") || fieldName.equals("rmCreateinvoice"))
      return rmCreateinvoice;
    else if (fieldName.equalsIgnoreCase("c_return_reason_id") || fieldName.equals("cReturnReasonId"))
      return cReturnReasonId;
    else if (fieldName.equalsIgnoreCase("c_return_reason_idr") || fieldName.equals("cReturnReasonIdr"))
      return cReturnReasonIdr;
    else if (fieldName.equalsIgnoreCase("rm_addorphanline") || fieldName.equals("rmAddorphanline"))
      return rmAddorphanline;
    else if (fieldName.equalsIgnoreCase("a_asset_id") || fieldName.equals("aAssetId"))
      return aAssetId;
    else if (fieldName.equalsIgnoreCase("calculate_promotions") || fieldName.equals("calculatePromotions"))
      return calculatePromotions;
    else if (fieldName.equalsIgnoreCase("c_costcenter_id") || fieldName.equals("cCostcenterId"))
      return cCostcenterId;
    else if (fieldName.equalsIgnoreCase("convertquotation"))
      return convertquotation;
    else if (fieldName.equalsIgnoreCase("c_reject_reason_id") || fieldName.equals("cRejectReasonId"))
      return cRejectReasonId;
    else if (fieldName.equalsIgnoreCase("c_reject_reason_idr") || fieldName.equals("cRejectReasonIdr"))
      return cRejectReasonIdr;
    else if (fieldName.equalsIgnoreCase("validuntil"))
      return validuntil;
    else if (fieldName.equalsIgnoreCase("quotation_id") || fieldName.equals("quotationId"))
      return quotationId;
    else if (fieldName.equalsIgnoreCase("quotation_idr") || fieldName.equals("quotationIdr"))
      return quotationIdr;
    else if (fieldName.equalsIgnoreCase("so_res_status") || fieldName.equals("soResStatus"))
      return soResStatus;
    else if (fieldName.equalsIgnoreCase("so_res_statusr") || fieldName.equals("soResStatusr"))
      return soResStatusr;
    else if (fieldName.equalsIgnoreCase("create_polines") || fieldName.equals("createPolines"))
      return createPolines;
    else if (fieldName.equalsIgnoreCase("iscashvat"))
      return iscashvat;
    else if (fieldName.equalsIgnoreCase("rm_pickfromreceipt") || fieldName.equals("rmPickfromreceipt"))
      return rmPickfromreceipt;
    else if (fieldName.equalsIgnoreCase("em_aprm_addpayment") || fieldName.equals("emAprmAddpayment"))
      return emAprmAddpayment;
    else if (fieldName.equalsIgnoreCase("em_atecco_fecha_pago") || fieldName.equals("emAteccoFechaPago"))
      return emAteccoFechaPago;
    else if (fieldName.equalsIgnoreCase("em_atecco_borra_1") || fieldName.equals("emAteccoBorra1"))
      return emAteccoBorra1;
    else if (fieldName.equalsIgnoreCase("em_atecco_borra_2") || fieldName.equals("emAteccoBorra2"))
      return emAteccoBorra2;
    else if (fieldName.equalsIgnoreCase("em_atecco_transporte") || fieldName.equals("emAteccoTransporte"))
      return emAteccoTransporte;
    else if (fieldName.equalsIgnoreCase("em_atecco_docaction") || fieldName.equals("emAteccoDocaction"))
      return emAteccoDocaction;
    else if (fieldName.equalsIgnoreCase("em_atecco_docaction_btn") || fieldName.equals("emAteccoDocactionBtn"))
      return emAteccoDocactionBtn;
    else if (fieldName.equalsIgnoreCase("em_atecco_imprimir") || fieldName.equals("emAteccoImprimir"))
      return emAteccoImprimir;
    else if (fieldName.equalsIgnoreCase("em_atecco_tipo_tarjeta") || fieldName.equals("emAteccoTipoTarjeta"))
      return emAteccoTipoTarjeta;
    else if (fieldName.equalsIgnoreCase("em_atecco_tipo_tarjetar") || fieldName.equals("emAteccoTipoTarjetar"))
      return emAteccoTipoTarjetar;
    else if (fieldName.equalsIgnoreCase("em_atecco_banco") || fieldName.equals("emAteccoBanco"))
      return emAteccoBanco;
    else if (fieldName.equalsIgnoreCase("em_atecco_bancor") || fieldName.equals("emAteccoBancor"))
      return emAteccoBancor;
    else if (fieldName.equalsIgnoreCase("em_atecco_docstatus") || fieldName.equals("emAteccoDocstatus"))
      return emAteccoDocstatus;
    else if (fieldName.equalsIgnoreCase("em_atecco_docstatusr") || fieldName.equals("emAteccoDocstatusr"))
      return emAteccoDocstatusr;
    else if (fieldName.equalsIgnoreCase("em_atecco_iniciar") || fieldName.equals("emAteccoIniciar"))
      return emAteccoIniciar;
    else if (fieldName.equalsIgnoreCase("em_atecco_iniciar_btn") || fieldName.equals("emAteccoIniciarBtn"))
      return emAteccoIniciarBtn;
    else if (fieldName.equalsIgnoreCase("em_atecco_procesar") || fieldName.equals("emAteccoProcesar"))
      return emAteccoProcesar;
    else if (fieldName.equalsIgnoreCase("em_atecco_procesar_btn") || fieldName.equals("emAteccoProcesarBtn"))
      return emAteccoProcesarBtn;
    else if (fieldName.equalsIgnoreCase("em_atecco_validar") || fieldName.equals("emAteccoValidar"))
      return emAteccoValidar;
    else if (fieldName.equalsIgnoreCase("em_atecco_pagodescuento") || fieldName.equals("emAteccoPagodescuento"))
      return emAteccoPagodescuento;
    else if (fieldName.equalsIgnoreCase("em_atecco_anticipo") || fieldName.equals("emAteccoAnticipo"))
      return emAteccoAnticipo;
    else if (fieldName.equalsIgnoreCase("em_atecco_anticipostatus") || fieldName.equals("emAteccoAnticipostatus"))
      return emAteccoAnticipostatus;
    else if (fieldName.equalsIgnoreCase("em_atecco_anticipostatusr") || fieldName.equals("emAteccoAnticipostatusr"))
      return emAteccoAnticipostatusr;
    else if (fieldName.equalsIgnoreCase("dateprinted"))
      return dateprinted;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_id") || fieldName.equals("cDoctypetargetId"))
      return cDoctypetargetId;
    else if (fieldName.equalsIgnoreCase("isdelivered"))
      return isdelivered;
    else if (fieldName.equalsIgnoreCase("isinvoiced"))
      return isinvoiced;
    else if (fieldName.equalsIgnoreCase("isprinted"))
      return isprinted;
    else if (fieldName.equalsIgnoreCase("isselected"))
      return isselected;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("issotrx"))
      return issotrx;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("docaction"))
      return docaction;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("c_order_id") || fieldName.equals("cOrderId"))
      return cOrderId;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static PedidoCliente294F4FDD3CB849138A093832B628F92FData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static PedidoCliente294F4FDD3CB849138A093832B628F92FData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(C_Order.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.CreatedBy) as CreatedByR, " +
      "        to_char(C_Order.Updated, ?) as updated, " +
      "        to_char(C_Order.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        C_Order.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.UpdatedBy) as UpdatedByR," +
      "        C_Order.C_BPartner_ID, " +
      "(CASE WHEN C_Order.C_BPartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name2), ''))),'') ) END) AS C_BPartner_IDR, " +
      "C_Order.DateOrdered, " +
      "C_Order.em_dec_tipo_evento, " +
      "(CASE WHEN C_Order.em_dec_tipo_evento IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS em_dec_tipo_eventoR, " +
      "C_Order.C_BPartner_Location_ID, " +
      "(CASE WHEN C_Order.C_BPartner_Location_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS C_BPartner_Location_IDR, " +
      "C_Order.Description, " +
      "C_Order.DatePromised, " +
      "C_Order.em_dec_tipo_contratacion, " +
      "(CASE WHEN C_Order.em_dec_tipo_contratacion IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS em_dec_tipo_contratacionR, " +
      "C_Order.em_dec_fecha_montaje, " +
      "TO_CHAR(C_Order.em_dec_hora_montaje, 'HH24:MI:SS') AS em_dec_hora_montaje, " +
      "C_Order.em_dec_fecha_desmontaje, " +
      "TO_CHAR(C_Order.em_dec_hora_desmontaje, 'HH24:MI:SS') AS em_dec_hora_desmontaje, " +
      "COALESCE(C_Order.em_dec_requiere_transporte, 'N') AS em_dec_requiere_transporte, " +
      "COALESCE(C_Order.em_dec_requiere_montaje, 'N') AS em_dec_requiere_montaje, " +
      "C_Order.SalesRep_ID, " +
      "(CASE WHEN C_Order.SalesRep_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS SalesRep_IDR, " +
      "C_Order.DateAcct, " +
      "C_Order.BillTo_ID, " +
      "(CASE WHEN C_Order.BillTo_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS BillTo_IDR, " +
      "C_Order.POReference, " +
      "COALESCE(C_Order.IsDiscountPrinted, 'N') AS IsDiscountPrinted, " +
      "C_Order.C_Currency_ID, " +
      "(CASE WHEN C_Order.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "C_Order.PaymentRule, " +
      "(CASE WHEN C_Order.PaymentRule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS PaymentRuleR, " +
      "C_Order.C_PaymentTerm_ID, " +
      "(CASE WHEN C_Order.C_PaymentTerm_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL8.Name IS NULL THEN TO_CHAR(table8.Name) ELSE TO_CHAR(tableTRL8.Name) END)), ''))),'') ) END) AS C_PaymentTerm_IDR, " +
      "C_Order.InvoiceRule, " +
      "(CASE WHEN C_Order.InvoiceRule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS InvoiceRuleR, " +
      "C_Order.DeliveryRule, " +
      "(CASE WHEN C_Order.DeliveryRule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS DeliveryRuleR, " +
      "C_Order.FreightCostRule, " +
      "(CASE WHEN C_Order.FreightCostRule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list4.name),'') ) END) AS FreightCostRuleR, " +
      "C_Order.FreightAmt, " +
      "C_Order.DeliveryViaRule, " +
      "(CASE WHEN C_Order.DeliveryViaRule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list5.name),'') ) END) AS DeliveryViaRuleR, " +
      "C_Order.M_Shipper_ID, " +
      "(CASE WHEN C_Order.M_Shipper_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table10.Name), ''))),'') ) END) AS M_Shipper_IDR, " +
      "C_Order.C_Charge_ID, " +
      "(CASE WHEN C_Order.C_Charge_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table11.Name), ''))),'') ) END) AS C_Charge_IDR, " +
      "C_Order.ChargeAmt, " +
      "C_Order.PriorityRule, " +
      "(CASE WHEN C_Order.PriorityRule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list6.name),'') ) END) AS PriorityRuleR, " +
      "C_Order.TotalLines, " +
      "C_Order.GrandTotal, " +
      "C_Order.M_Warehouse_ID, " +
      "C_Order.M_PriceList_ID, " +
      "(CASE WHEN C_Order.M_PriceList_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table12.Name), ''))),'') ) END) AS M_PriceList_IDR, " +
      "COALESCE(C_Order.IsTaxIncluded, 'N') AS IsTaxIncluded, " +
      "C_Order.C_Campaign_ID, " +
      "(CASE WHEN C_Order.C_Campaign_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table13.Name), ''))),'') ) END) AS C_Campaign_IDR, " +
      "C_Order.C_Project_ID, " +
      "(CASE WHEN C_Order.C_Project_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table14.Value), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table14.Name), ''))),'') ) END) AS C_Project_IDR, " +
      "C_Order.C_Activity_ID, " +
      "(CASE WHEN C_Order.C_Activity_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table15.Name), ''))),'') ) END) AS C_Activity_IDR, " +
      "C_Order.Posted, " +
      "list7.name as Posted_BTN, " +
      "C_Order.AD_User_ID, " +
      "(CASE WHEN C_Order.AD_User_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table16.Name), ''))),'') ) END) AS AD_User_IDR, " +
      "C_Order.CopyFrom, " +
      "C_Order.DropShip_BPartner_ID, " +
      "C_Order.DropShip_Location_ID, " +
      "(CASE WHEN C_Order.DropShip_Location_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table17.Name), ''))),'') ) END) AS DropShip_Location_IDR, " +
      "C_Order.DropShip_User_ID, " +
      "(CASE WHEN C_Order.DropShip_User_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table18.Name), ''))),'') ) END) AS DropShip_User_IDR, " +
      "COALESCE(C_Order.IsSelfService, 'N') AS IsSelfService, " +
      "C_Order.AD_OrgTrx_ID, " +
      "(CASE WHEN C_Order.AD_OrgTrx_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table19.Name), ''))),'') ) END) AS AD_OrgTrx_IDR, " +
      "C_Order.User1_ID, " +
      "C_Order.User2_ID, " +
      "C_Order.Deliverynotes, " +
      "C_Order.C_Incoterms_ID, " +
      "(CASE WHEN C_Order.C_Incoterms_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table20.Name), ''))),'') ) END) AS C_Incoterms_IDR, " +
      "C_Order.Incotermsdescription, " +
      "C_Order.Generatetemplate, " +
      "C_Order.Delivery_Location_ID, " +
      "(CASE WHEN C_Order.Delivery_Location_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table21.Name), ''))),'') ) END) AS Delivery_Location_IDR, " +
      "C_Order.CopyFromPO, " +
      "C_Order.FIN_Paymentmethod_ID, " +
      "(CASE WHEN C_Order.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table22.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "C_Order.FIN_Payment_Priority_ID, " +
      "(CASE WHEN C_Order.FIN_Payment_Priority_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table23.Priority), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table23.Name), ''))),'') ) END) AS FIN_Payment_Priority_IDR, " +
      "C_Order.RM_PickFromShipment, " +
      "C_Order.RM_ReceiveMaterials, " +
      "C_Order.RM_CreateInvoice, " +
      "C_Order.C_Return_Reason_ID, " +
      "(CASE WHEN C_Order.C_Return_Reason_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table24.Name), ''))),'') ) END) AS C_Return_Reason_IDR, " +
      "C_Order.RM_AddOrphanLine, " +
      "C_Order.A_Asset_ID, " +
      "C_Order.Calculate_Promotions, " +
      "C_Order.C_Costcenter_ID, " +
      "C_Order.Convertquotation, " +
      "C_Order.C_Reject_Reason_ID, " +
      "(CASE WHEN C_Order.C_Reject_Reason_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table25.Name), ''))),'') ) END) AS C_Reject_Reason_IDR, " +
      "C_Order.validuntil, " +
      "C_Order.Quotation_ID, " +
      "(CASE WHEN C_Order.Quotation_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table26.DocumentNo), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table26.DateOrdered, 'DD-MM-YYYY')),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table26.GrandTotal), ''))),'') ) END) AS Quotation_IDR, " +
      "C_Order.SO_Res_Status, " +
      "(CASE WHEN C_Order.SO_Res_Status IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list8.name),'') ) END) AS SO_Res_StatusR, " +
      "C_Order.Create_POLines, " +
      "COALESCE(C_Order.Iscashvat, 'N') AS Iscashvat, " +
      "C_Order.RM_Pickfromreceipt, " +
      "C_Order.EM_APRM_AddPayment, " +
      "C_Order.EM_Atecco_Fecha_Pago, " +
      "C_Order.EM_Atecco_Borra_1, " +
      "C_Order.EM_Atecco_Borra_2, " +
      "COALESCE(C_Order.EM_Atecco_Transporte, 'N') AS EM_Atecco_Transporte, " +
      "C_Order.EM_Atecco_Docaction, " +
      "list9.name as EM_Atecco_Docaction_BTN, " +
      "C_Order.EM_Atecco_Imprimir, " +
      "C_Order.EM_Atecco_Tipo_Tarjeta, " +
      "(CASE WHEN C_Order.EM_Atecco_Tipo_Tarjeta IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list10.name),'') ) END) AS EM_Atecco_Tipo_TarjetaR, " +
      "C_Order.EM_Atecco_Banco, " +
      "(CASE WHEN C_Order.EM_Atecco_Banco IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list11.name),'') ) END) AS EM_Atecco_BancoR, " +
      "C_Order.EM_Atecco_Docstatus, " +
      "(CASE WHEN C_Order.EM_Atecco_Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list12.name),'') ) END) AS EM_Atecco_DocstatusR, " +
      "C_Order.EM_Atecco_Iniciar, " +
      "list13.name as EM_Atecco_Iniciar_BTN, " +
      "C_Order.EM_Atecco_Procesar, " +
      "list14.name as EM_Atecco_Procesar_BTN, " +
      "C_Order.EM_Atecco_Validar, " +
      "COALESCE(C_Order.EM_Atecco_Pagodescuento, 'N') AS EM_Atecco_Pagodescuento, " +
      "C_Order.EM_Atecco_Anticipo, " +
      "C_Order.EM_Atecco_Anticipostatus, " +
      "(CASE WHEN C_Order.EM_Atecco_Anticipostatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list15.name),'') ) END) AS EM_Atecco_AnticipostatusR, " +
      "C_Order.DatePrinted, " +
      "C_Order.C_DocTypeTarget_ID, " +
      "COALESCE(C_Order.IsDelivered, 'N') AS IsDelivered, " +
      "COALESCE(C_Order.IsInvoiced, 'N') AS IsInvoiced, " +
      "COALESCE(C_Order.IsPrinted, 'N') AS IsPrinted, " +
      "COALESCE(C_Order.IsSelected, 'N') AS IsSelected, " +
      "C_Order.DocStatus, " +
      "COALESCE(C_Order.IsSOTrx, 'N') AS IsSOTrx, " +
      "C_Order.AD_Client_ID, " +
      "COALESCE(C_Order.IsActive, 'N') AS IsActive, " +
      "C_Order.DocumentNo, " +
      "C_Order.AD_Org_ID, " +
      "C_Order.DocAction, " +
      "C_Order.Processing, " +
      "C_Order.C_Order_ID, " +
      "COALESCE(C_Order.Processed, 'N') AS Processed, " +
      "C_Order.C_DocType_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM C_Order left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table1 on (C_Order.C_BPartner_ID = table1.C_BPartner_ID) left join (select M_AttributeValue_ID, Name from M_AttributeValue) table2 on (C_Order.em_dec_tipo_evento =  table2.M_AttributeValue_ID) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table3 on (C_Order.C_BPartner_Location_ID = table3.C_BPartner_Location_ID) left join (select M_AttributeValue_ID, Name from M_AttributeValue) table4 on (C_Order.em_dec_tipo_contratacion =  table4.M_AttributeValue_ID) left join (select AD_User_ID, Name from AD_User) table5 on (C_Order.SalesRep_ID =  table5.AD_User_ID) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table6 on (C_Order.BillTo_ID =  table6.C_BPartner_Location_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table7 on (C_Order.C_Currency_ID = table7.C_Currency_ID) left join ad_ref_list_v list1 on (C_Order.PaymentRule = list1.value and list1.ad_reference_id = '195' and list1.ad_language = ?)  left join (select C_PaymentTerm_ID, Name from C_PaymentTerm) table8 on (C_Order.C_PaymentTerm_ID = table8.C_PaymentTerm_ID) left join (select C_PaymentTerm_ID,AD_Language, Name from C_PaymentTerm_TRL) tableTRL8 on (table8.C_PaymentTerm_ID = tableTRL8.C_PaymentTerm_ID and tableTRL8.AD_Language = ?)  left join ad_ref_list_v list2 on (C_Order.InvoiceRule = list2.value and list2.ad_reference_id = '150' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (C_Order.DeliveryRule = list3.value and list3.ad_reference_id = '151' and list3.ad_language = ?)  left join ad_ref_list_v list4 on (C_Order.FreightCostRule = list4.value and list4.ad_reference_id = '153' and list4.ad_language = ?)  left join ad_ref_list_v list5 on (C_Order.DeliveryViaRule = list5.value and list5.ad_reference_id = '152' and list5.ad_language = ?)  left join (select M_Shipper_ID, Name from M_Shipper) table10 on (C_Order.M_Shipper_ID = table10.M_Shipper_ID) left join (select C_Charge_ID, Name from C_Charge) table11 on (C_Order.C_Charge_ID =  table11.C_Charge_ID) left join ad_ref_list_v list6 on (C_Order.PriorityRule = list6.value and list6.ad_reference_id = '154' and list6.ad_language = ?)  left join (select M_PriceList_ID, Name from M_PriceList) table12 on (C_Order.M_PriceList_ID = table12.M_PriceList_ID) left join (select C_Campaign_ID, Name from C_Campaign) table13 on (C_Order.C_Campaign_ID = table13.C_Campaign_ID) left join (select C_Project_ID, Value, Name from C_Project) table14 on (C_Order.C_Project_ID = table14.C_Project_ID) left join (select C_Activity_ID, Name from C_Activity) table15 on (C_Order.C_Activity_ID = table15.C_Activity_ID) left join ad_ref_list_v list7 on (list7.ad_reference_id = '234' and list7.ad_language = ?  AND C_Order.Posted = TO_CHAR(list7.value)) left join (select AD_User_ID, Name from AD_User) table16 on (C_Order.AD_User_ID = table16.AD_User_ID) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table17 on (C_Order.DropShip_Location_ID =  table17.C_BPartner_Location_ID) left join (select AD_User_ID, Name from AD_User) table18 on (C_Order.DropShip_User_ID =  table18.AD_User_ID) left join (select AD_Org_ID, value, Name from AD_Org) table19 on (C_Order.AD_OrgTrx_ID =  table19.AD_Org_ID) left join (select C_Incoterms_ID, Name from C_Incoterms) table20 on (C_Order.C_Incoterms_ID = table20.C_Incoterms_ID) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table21 on (C_Order.Delivery_Location_ID =  table21.C_BPartner_Location_ID) left join (select FIN_Paymentmethod_ID, Name from FIN_Paymentmethod) table22 on (C_Order.FIN_Paymentmethod_ID = table22.FIN_Paymentmethod_ID) left join (select FIN_Payment_Priority_ID, Priority, Name from FIN_Payment_Priority) table23 on (C_Order.FIN_Payment_Priority_ID = table23.FIN_Payment_Priority_ID) left join (select C_Return_Reason_ID, Name from C_Return_Reason) table24 on (C_Order.C_Return_Reason_ID = table24.C_Return_Reason_ID) left join (select C_Reject_Reason_ID, Name from C_Reject_Reason) table25 on (C_Order.C_Reject_Reason_ID = table25.C_Reject_Reason_ID) left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table26 on (C_Order.Quotation_ID = table26.C_Order_ID) left join ad_ref_list_v list8 on (C_Order.SO_Res_Status = list8.value and list8.ad_reference_id = 'C3C19DE8AB3B42E78748E20D986FBBC9' and list8.ad_language = ?)  left join ad_ref_list_v list9 on (list9.ad_reference_id = 'FF80818130217A35013021A672400035' and list9.ad_language = ?  AND C_Order.EM_Atecco_Docaction = TO_CHAR(list9.value)) left join ad_ref_list_v list10 on (C_Order.EM_Atecco_Tipo_Tarjeta = list10.value and list10.ad_reference_id = '547BCC92B2474F6CA266E838682858C9' and list10.ad_language = ?)  left join ad_ref_list_v list11 on (C_Order.EM_Atecco_Banco = list11.value and list11.ad_reference_id = '00DE8D05B02D41CF9ED325A0552EA5EB' and list11.ad_language = ?)  left join ad_ref_list_v list12 on (C_Order.EM_Atecco_Docstatus = list12.value and list12.ad_reference_id = '2EF8750FEAE14CBD8195D4F0006A03F0' and list12.ad_language = ?)  left join ad_ref_list_v list13 on (list13.ad_reference_id = '00143645AE464184A81DA4AD0D22EF09' and list13.ad_language = ?  AND C_Order.EM_Atecco_Iniciar = TO_CHAR(list13.value)) left join ad_ref_list_v list14 on (list14.ad_reference_id = 'C2075E669BA7489CA61B85FA29883F1F' and list14.ad_language = ?  AND C_Order.EM_Atecco_Procesar = TO_CHAR(list14.value)) left join ad_ref_list_v list15 on (C_Order.EM_Atecco_Anticipostatus = list15.value and list15.ad_reference_id = 'D4A2CA1BC87B477C9253A232F6A1AE1C' and list15.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PedidoCliente294F4FDD3CB849138A093832B628F92FData objectPedidoCliente294F4FDD3CB849138A093832B628F92FData = new PedidoCliente294F4FDD3CB849138A093832B628F92FData();
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.created = UtilSql.getValue(result, "created");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.updated = UtilSql.getValue(result, "updated");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.updatedby = UtilSql.getValue(result, "updatedby");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.dateordered = UtilSql.getDateValue(result, "dateordered", "dd-MM-yyyy");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecTipoEvento = UtilSql.getValue(result, "em_dec_tipo_evento");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecTipoEventor = UtilSql.getValue(result, "em_dec_tipo_eventor");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cBpartnerLocationIdr = UtilSql.getValue(result, "c_bpartner_location_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.description = UtilSql.getValue(result, "description");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.datepromised = UtilSql.getDateValue(result, "datepromised", "dd-MM-yyyy");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecTipoContratacion = UtilSql.getValue(result, "em_dec_tipo_contratacion");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecTipoContratacionr = UtilSql.getValue(result, "em_dec_tipo_contratacionr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecFechaMontaje = UtilSql.getDateValue(result, "em_dec_fecha_montaje", "dd-MM-yyyy");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecHoraMontaje = UtilSql.getValue(result, "em_dec_hora_montaje");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecFechaDesmontaje = UtilSql.getDateValue(result, "em_dec_fecha_desmontaje", "dd-MM-yyyy");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecHoraDesmontaje = UtilSql.getValue(result, "em_dec_hora_desmontaje");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecRequiereTransporte = UtilSql.getValue(result, "em_dec_requiere_transporte");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emDecRequiereMontaje = UtilSql.getValue(result, "em_dec_requiere_montaje");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.salesrepIdr = UtilSql.getValue(result, "salesrep_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.billtoId = UtilSql.getValue(result, "billto_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.billtoIdr = UtilSql.getValue(result, "billto_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.poreference = UtilSql.getValue(result, "poreference");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.isdiscountprinted = UtilSql.getValue(result, "isdiscountprinted");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.paymentrule = UtilSql.getValue(result, "paymentrule");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.paymentruler = UtilSql.getValue(result, "paymentruler");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cPaymenttermId = UtilSql.getValue(result, "c_paymentterm_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cPaymenttermIdr = UtilSql.getValue(result, "c_paymentterm_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.invoicerule = UtilSql.getValue(result, "invoicerule");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.invoiceruler = UtilSql.getValue(result, "invoiceruler");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.deliveryrule = UtilSql.getValue(result, "deliveryrule");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.deliveryruler = UtilSql.getValue(result, "deliveryruler");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.freightcostrule = UtilSql.getValue(result, "freightcostrule");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.freightcostruler = UtilSql.getValue(result, "freightcostruler");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.freightamt = UtilSql.getValue(result, "freightamt");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.deliveryviarule = UtilSql.getValue(result, "deliveryviarule");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.deliveryviaruler = UtilSql.getValue(result, "deliveryviaruler");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.mShipperId = UtilSql.getValue(result, "m_shipper_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.mShipperIdr = UtilSql.getValue(result, "m_shipper_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cChargeId = UtilSql.getValue(result, "c_charge_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cChargeIdr = UtilSql.getValue(result, "c_charge_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.chargeamt = UtilSql.getValue(result, "chargeamt");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.priorityrule = UtilSql.getValue(result, "priorityrule");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.priorityruler = UtilSql.getValue(result, "priorityruler");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.totallines = UtilSql.getValue(result, "totallines");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.grandtotal = UtilSql.getValue(result, "grandtotal");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.mWarehouseId = UtilSql.getValue(result, "m_warehouse_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.mPricelistIdr = UtilSql.getValue(result, "m_pricelist_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.istaxincluded = UtilSql.getValue(result, "istaxincluded");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cCampaignId = UtilSql.getValue(result, "c_campaign_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cCampaignIdr = UtilSql.getValue(result, "c_campaign_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cProjectIdr = UtilSql.getValue(result, "c_project_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cActivityId = UtilSql.getValue(result, "c_activity_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cActivityIdr = UtilSql.getValue(result, "c_activity_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.posted = UtilSql.getValue(result, "posted");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.postedBtn = UtilSql.getValue(result, "posted_btn");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.adUserIdr = UtilSql.getValue(result, "ad_user_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.copyfrom = UtilSql.getValue(result, "copyfrom");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.dropshipBpartnerId = UtilSql.getValue(result, "dropship_bpartner_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.dropshipLocationId = UtilSql.getValue(result, "dropship_location_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.dropshipLocationIdr = UtilSql.getValue(result, "dropship_location_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.dropshipUserId = UtilSql.getValue(result, "dropship_user_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.dropshipUserIdr = UtilSql.getValue(result, "dropship_user_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.isselfservice = UtilSql.getValue(result, "isselfservice");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.adOrgtrxId = UtilSql.getValue(result, "ad_orgtrx_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.adOrgtrxIdr = UtilSql.getValue(result, "ad_orgtrx_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.user1Id = UtilSql.getValue(result, "user1_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.user2Id = UtilSql.getValue(result, "user2_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.deliverynotes = UtilSql.getValue(result, "deliverynotes");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cIncotermsId = UtilSql.getValue(result, "c_incoterms_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cIncotermsIdr = UtilSql.getValue(result, "c_incoterms_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.incotermsdescription = UtilSql.getValue(result, "incotermsdescription");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.generatetemplate = UtilSql.getValue(result, "generatetemplate");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.deliveryLocationId = UtilSql.getValue(result, "delivery_location_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.deliveryLocationIdr = UtilSql.getValue(result, "delivery_location_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.copyfrompo = UtilSql.getValue(result, "copyfrompo");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.finPaymentmethodIdr = UtilSql.getValue(result, "fin_paymentmethod_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.finPaymentPriorityId = UtilSql.getValue(result, "fin_payment_priority_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.finPaymentPriorityIdr = UtilSql.getValue(result, "fin_payment_priority_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.rmPickfromshipment = UtilSql.getValue(result, "rm_pickfromshipment");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.rmReceivematerials = UtilSql.getValue(result, "rm_receivematerials");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.rmCreateinvoice = UtilSql.getValue(result, "rm_createinvoice");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cReturnReasonId = UtilSql.getValue(result, "c_return_reason_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cReturnReasonIdr = UtilSql.getValue(result, "c_return_reason_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.rmAddorphanline = UtilSql.getValue(result, "rm_addorphanline");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.aAssetId = UtilSql.getValue(result, "a_asset_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.calculatePromotions = UtilSql.getValue(result, "calculate_promotions");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cCostcenterId = UtilSql.getValue(result, "c_costcenter_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.convertquotation = UtilSql.getValue(result, "convertquotation");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cRejectReasonId = UtilSql.getValue(result, "c_reject_reason_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cRejectReasonIdr = UtilSql.getValue(result, "c_reject_reason_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.validuntil = UtilSql.getDateValue(result, "validuntil", "dd-MM-yyyy");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.quotationId = UtilSql.getValue(result, "quotation_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.quotationIdr = UtilSql.getValue(result, "quotation_idr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.soResStatus = UtilSql.getValue(result, "so_res_status");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.soResStatusr = UtilSql.getValue(result, "so_res_statusr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.createPolines = UtilSql.getValue(result, "create_polines");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.iscashvat = UtilSql.getValue(result, "iscashvat");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.rmPickfromreceipt = UtilSql.getValue(result, "rm_pickfromreceipt");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAprmAddpayment = UtilSql.getValue(result, "em_aprm_addpayment");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoFechaPago = UtilSql.getDateValue(result, "em_atecco_fecha_pago", "dd-MM-yyyy");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoBorra1 = UtilSql.getValue(result, "em_atecco_borra_1");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoBorra2 = UtilSql.getValue(result, "em_atecco_borra_2");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoTransporte = UtilSql.getValue(result, "em_atecco_transporte");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoDocaction = UtilSql.getValue(result, "em_atecco_docaction");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoDocactionBtn = UtilSql.getValue(result, "em_atecco_docaction_btn");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoImprimir = UtilSql.getValue(result, "em_atecco_imprimir");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoTipoTarjeta = UtilSql.getValue(result, "em_atecco_tipo_tarjeta");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoTipoTarjetar = UtilSql.getValue(result, "em_atecco_tipo_tarjetar");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoBanco = UtilSql.getValue(result, "em_atecco_banco");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoBancor = UtilSql.getValue(result, "em_atecco_bancor");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoDocstatus = UtilSql.getValue(result, "em_atecco_docstatus");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoDocstatusr = UtilSql.getValue(result, "em_atecco_docstatusr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoIniciar = UtilSql.getValue(result, "em_atecco_iniciar");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoIniciarBtn = UtilSql.getValue(result, "em_atecco_iniciar_btn");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoProcesar = UtilSql.getValue(result, "em_atecco_procesar");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoProcesarBtn = UtilSql.getValue(result, "em_atecco_procesar_btn");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoValidar = UtilSql.getValue(result, "em_atecco_validar");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoPagodescuento = UtilSql.getValue(result, "em_atecco_pagodescuento");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoAnticipo = UtilSql.getValue(result, "em_atecco_anticipo");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoAnticipostatus = UtilSql.getValue(result, "em_atecco_anticipostatus");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.emAteccoAnticipostatusr = UtilSql.getValue(result, "em_atecco_anticipostatusr");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.dateprinted = UtilSql.getDateValue(result, "dateprinted", "dd-MM-yyyy");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cDoctypetargetId = UtilSql.getValue(result, "c_doctypetarget_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.isdelivered = UtilSql.getValue(result, "isdelivered");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.isinvoiced = UtilSql.getValue(result, "isinvoiced");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.isprinted = UtilSql.getValue(result, "isprinted");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.isselected = UtilSql.getValue(result, "isselected");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.docstatus = UtilSql.getValue(result, "docstatus");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.issotrx = UtilSql.getValue(result, "issotrx");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.isactive = UtilSql.getValue(result, "isactive");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.documentno = UtilSql.getValue(result, "documentno");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.docaction = UtilSql.getValue(result, "docaction");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.processing = UtilSql.getValue(result, "processing");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cOrderId = UtilSql.getValue(result, "c_order_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.processed = UtilSql.getValue(result, "processed");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.language = UtilSql.getValue(result, "language");
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.adUserClient = "";
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.adOrgClient = "";
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.createdby = "";
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.trBgcolor = "";
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.totalCount = "";
        objectPedidoCliente294F4FDD3CB849138A093832B628F92FData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPedidoCliente294F4FDD3CB849138A093832B628F92FData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PedidoCliente294F4FDD3CB849138A093832B628F92FData objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[] = new PedidoCliente294F4FDD3CB849138A093832B628F92FData[vector.size()];
    vector.copyInto(objectPedidoCliente294F4FDD3CB849138A093832B628F92FData);
    return(objectPedidoCliente294F4FDD3CB849138A093832B628F92FData);
  }

/**
Create a registry
 */
  public static PedidoCliente294F4FDD3CB849138A093832B628F92FData[] set(String emAteccoBorra2, String emAteccoBorra1, String convertquotation, String emDecHoraMontaje, String validuntil, String aAssetId, String emAteccoTransporte, String emAteccoDocstatus, String cOrderId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String documentno, String docstatus, String docaction, String cDoctypeId, String cDoctypetargetId, String description, String isdelivered, String isinvoiced, String isprinted, String dateordered, String datepromised, String dateacct, String salesrepId, String cPaymenttermId, String billtoId, String cCurrencyId, String invoicerule, String freightamt, String deliveryviarule, String mShipperId, String priorityrule, String totallines, String grandtotal, String mWarehouseId, String mPricelistId, String processing, String cCampaignId, String cBpartnerId, String cBpartnerIdr, String adUserId, String emDecFechaMontaje, String emDecTipoEvento, String poreference, String cChargeId, String chargeamt, String emAteccoTipoTarjeta, String processed, String cBpartnerLocationId, String cProjectId, String cProjectIdr, String cActivityId, String quotationId, String quotationIdr, String issotrx, String dateprinted, String deliveryrule, String freightcostrule, String emAteccoIniciar, String emAteccoIniciarBtn, String paymentrule, String isdiscountprinted, String posted, String postedBtn, String istaxincluded, String isselected, String emAteccoFechaPago, String emDecHoraDesmontaje, String cCostcenterId, String deliverynotes, String cIncotermsId, String incotermsdescription, String generatetemplate, String deliveryLocationId, String copyfrompo, String emAteccoPagodescuento, String finPaymentmethodId, String finPaymentPriorityId, String dropshipUserId, String dropshipBpartnerId, String copyfrom, String dropshipLocationId, String isselfservice, String emAteccoAnticipostatus, String emAteccoValidar, String adOrgtrxId, String user2Id, String user1Id, String calculatePromotions, String emAteccoDocaction, String emAteccoDocactionBtn, String emAteccoBanco, String rmPickfromshipment, String rmReceivematerials, String rmCreateinvoice, String rmPickfromreceipt, String emDecRequiereMontaje, String emAteccoImprimir, String cReturnReasonId, String emDecRequiereTransporte, String rmAddorphanline, String soResStatus, String emAteccoAnticipo, String emDecTipoContratacion, String createPolines, String cRejectReasonId, String iscashvat, String emAteccoProcesar, String emAteccoProcesarBtn, String emAprmAddpayment, String emDecFechaDesmontaje)    throws ServletException {
    PedidoCliente294F4FDD3CB849138A093832B628F92FData objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[] = new PedidoCliente294F4FDD3CB849138A093832B628F92FData[1];
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0] = new PedidoCliente294F4FDD3CB849138A093832B628F92FData();
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].created = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].createdbyr = createdbyr;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].updated = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].updatedTimeStamp = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].updatedby = updatedby;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].updatedbyr = updatedbyr;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cBpartnerId = cBpartnerId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cBpartnerIdr = cBpartnerIdr;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].dateordered = dateordered;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecTipoEvento = emDecTipoEvento;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecTipoEventor = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cBpartnerLocationId = cBpartnerLocationId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cBpartnerLocationIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].description = description;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].datepromised = datepromised;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecTipoContratacion = emDecTipoContratacion;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecTipoContratacionr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecFechaMontaje = emDecFechaMontaje;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecHoraMontaje = emDecHoraMontaje;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecFechaDesmontaje = emDecFechaDesmontaje;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecHoraDesmontaje = emDecHoraDesmontaje;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecRequiereTransporte = emDecRequiereTransporte;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emDecRequiereMontaje = emDecRequiereMontaje;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].salesrepId = salesrepId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].salesrepIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].dateacct = dateacct;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].billtoId = billtoId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].billtoIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].poreference = poreference;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].isdiscountprinted = isdiscountprinted;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cCurrencyId = cCurrencyId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cCurrencyIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].paymentrule = paymentrule;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].paymentruler = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cPaymenttermId = cPaymenttermId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cPaymenttermIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].invoicerule = invoicerule;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].invoiceruler = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].deliveryrule = deliveryrule;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].deliveryruler = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].freightcostrule = freightcostrule;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].freightcostruler = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].freightamt = freightamt;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].deliveryviarule = deliveryviarule;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].deliveryviaruler = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].mShipperId = mShipperId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].mShipperIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cChargeId = cChargeId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cChargeIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].chargeamt = chargeamt;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].priorityrule = priorityrule;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].priorityruler = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].totallines = totallines;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].grandtotal = grandtotal;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].mWarehouseId = mWarehouseId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].mPricelistId = mPricelistId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].mPricelistIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].istaxincluded = istaxincluded;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cCampaignId = cCampaignId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cCampaignIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cProjectId = cProjectId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cProjectIdr = cProjectIdr;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cActivityId = cActivityId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cActivityIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].posted = posted;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].postedBtn = postedBtn;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].adUserId = adUserId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].adUserIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].copyfrom = copyfrom;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].dropshipBpartnerId = dropshipBpartnerId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].dropshipLocationId = dropshipLocationId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].dropshipLocationIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].dropshipUserId = dropshipUserId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].dropshipUserIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].isselfservice = isselfservice;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].adOrgtrxId = adOrgtrxId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].adOrgtrxIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].user1Id = user1Id;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].user2Id = user2Id;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].deliverynotes = deliverynotes;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cIncotermsId = cIncotermsId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cIncotermsIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].incotermsdescription = incotermsdescription;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].generatetemplate = generatetemplate;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].deliveryLocationId = deliveryLocationId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].deliveryLocationIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].copyfrompo = copyfrompo;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].finPaymentmethodId = finPaymentmethodId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].finPaymentmethodIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].finPaymentPriorityId = finPaymentPriorityId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].finPaymentPriorityIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].rmPickfromshipment = rmPickfromshipment;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].rmReceivematerials = rmReceivematerials;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].rmCreateinvoice = rmCreateinvoice;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cReturnReasonId = cReturnReasonId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cReturnReasonIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].rmAddorphanline = rmAddorphanline;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].aAssetId = aAssetId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].calculatePromotions = calculatePromotions;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cCostcenterId = cCostcenterId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].convertquotation = convertquotation;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cRejectReasonId = cRejectReasonId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cRejectReasonIdr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].validuntil = validuntil;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].quotationId = quotationId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].quotationIdr = quotationIdr;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].soResStatus = soResStatus;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].soResStatusr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].createPolines = createPolines;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].iscashvat = iscashvat;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].rmPickfromreceipt = rmPickfromreceipt;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAprmAddpayment = emAprmAddpayment;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoFechaPago = emAteccoFechaPago;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoBorra1 = emAteccoBorra1;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoBorra2 = emAteccoBorra2;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoTransporte = emAteccoTransporte;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoDocaction = emAteccoDocaction;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoDocactionBtn = emAteccoDocactionBtn;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoImprimir = emAteccoImprimir;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoTipoTarjeta = emAteccoTipoTarjeta;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoTipoTarjetar = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoBanco = emAteccoBanco;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoBancor = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoDocstatus = emAteccoDocstatus;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoDocstatusr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoIniciar = emAteccoIniciar;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoIniciarBtn = emAteccoIniciarBtn;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoProcesar = emAteccoProcesar;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoProcesarBtn = emAteccoProcesarBtn;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoValidar = emAteccoValidar;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoPagodescuento = emAteccoPagodescuento;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoAnticipo = emAteccoAnticipo;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoAnticipostatus = emAteccoAnticipostatus;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].emAteccoAnticipostatusr = "";
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].dateprinted = dateprinted;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cDoctypetargetId = cDoctypetargetId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].isdelivered = isdelivered;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].isinvoiced = isinvoiced;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].isprinted = isprinted;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].isselected = isselected;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].docstatus = docstatus;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].issotrx = issotrx;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].adClientId = adClientId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].isactive = isactive;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].documentno = documentno;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].adOrgId = adOrgId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].docaction = docaction;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].processing = processing;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cOrderId = cOrderId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].processed = processed;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].cDoctypeId = cDoctypeId;
    objectPedidoCliente294F4FDD3CB849138A093832B628F92FData[0].language = "";
    return objectPedidoCliente294F4FDD3CB849138A093832B628F92FData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2166_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2168_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2762_2(ConnectionProvider connectionProvider, String C_BPartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_BPartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_BPartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3402_3(ConnectionProvider connectionProvider, String C_Project_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Value), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Project_ID FROM C_Project left join (select C_Project_ID, Value, Name from C_Project) table2 on (C_Project.C_Project_ID = table2.C_Project_ID) WHERE C_Project.isActive='Y' AND C_Project.C_Project_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Project_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_project_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef367DCAA9CF4442ADB9A76F6539102217_4(ConnectionProvider connectionProvider, String Quotation_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.DocumentNo), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table2.DateOrdered, 'DD-MM-YYYY')), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.GrandTotal), ''))), '') ) as Quotation_ID FROM C_Order left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table2 on (C_Order.C_Order_ID = table2.C_Order_ID) WHERE C_Order.isActive='Y' AND C_Order.C_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Quotation_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "quotation_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef828EE0AE802C5FA1E040007F010067C7(ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT fin_paymentmethod_id as defaultValue  FROM fin_paymentmethod WHERE upper(name) LIKE 'EFECTIVO' ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef831B272EE4364C208EFA70098EDF29B4(ConnectionProvider connectionProvider, String AD_Org_ID, String AD_Role_ID, String AD_Client_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT FIN_Payment_Priority_ID FROM FIN_Payment_Priority WHERE AD_ISORGINCLUDED( (CASE WHEN ?='0' THEN (SELECT ad_org_id FROM ad_org WHERE name = ( SELECT min(o.name) FROM ad_org o join ad_orgtype ot on (o.ad_orgtype_id=ot.ad_orgtype_id) join ad_role_orgaccess ra on (o.ad_org_id=ra.ad_org_id) join ad_role r on (ra.ad_role_id = r.ad_role_id) WHERE ot.istransactionsallowed = 'Y' and r.ad_role_id=?)) ELSE ? END), AD_Org_ID, ?) <> -1 AND Isdefault = 'Y' ORDER BY Priority ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Org_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Role_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Org_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Client_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "fin_payment_priority_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for action search
 */
  public static String selectActDefM_AttributeSetInstance_ID(ConnectionProvider connectionProvider, String M_AttributeSetInstance_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT Description FROM M_AttributeSetInstance WHERE isActive='Y' AND M_AttributeSetInstance_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_AttributeSetInstance_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "description");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_Order" +
      "        SET C_BPartner_ID = (?) , DateOrdered = TO_DATE(?) , em_dec_tipo_evento = (?) , C_BPartner_Location_ID = (?) , Description = (?) , DatePromised = TO_DATE(?) , em_dec_tipo_contratacion = (?) , em_dec_fecha_montaje = TO_DATE(?) , em_dec_hora_montaje = TO_TIMESTAMP(?,'HH24:MI:SS') , em_dec_fecha_desmontaje = TO_DATE(?) , em_dec_hora_desmontaje = TO_TIMESTAMP(?,'HH24:MI:SS') , em_dec_requiere_transporte = (?) , em_dec_requiere_montaje = (?) , SalesRep_ID = (?) , DateAcct = TO_DATE(?) , BillTo_ID = (?) , POReference = (?) , IsDiscountPrinted = (?) , C_Currency_ID = (?) , PaymentRule = (?) , C_PaymentTerm_ID = (?) , InvoiceRule = (?) , DeliveryRule = (?) , FreightCostRule = (?) , FreightAmt = TO_NUMBER(?) , DeliveryViaRule = (?) , M_Shipper_ID = (?) , C_Charge_ID = (?) , ChargeAmt = TO_NUMBER(?) , PriorityRule = (?) , TotalLines = TO_NUMBER(?) , GrandTotal = TO_NUMBER(?) , M_Warehouse_ID = (?) , M_PriceList_ID = (?) , IsTaxIncluded = (?) , C_Campaign_ID = (?) , C_Project_ID = (?) , C_Activity_ID = (?) , Posted = (?) , AD_User_ID = (?) , CopyFrom = (?) , DropShip_BPartner_ID = (?) , DropShip_Location_ID = (?) , DropShip_User_ID = (?) , IsSelfService = (?) , AD_OrgTrx_ID = (?) , User1_ID = (?) , User2_ID = (?) , Deliverynotes = (?) , C_Incoterms_ID = (?) , Incotermsdescription = (?) , Generatetemplate = (?) , Delivery_Location_ID = (?) , CopyFromPO = (?) , FIN_Paymentmethod_ID = (?) , FIN_Payment_Priority_ID = (?) , RM_PickFromShipment = (?) , RM_ReceiveMaterials = (?) , RM_CreateInvoice = (?) , C_Return_Reason_ID = (?) , RM_AddOrphanLine = (?) , A_Asset_ID = (?) , Calculate_Promotions = (?) , C_Costcenter_ID = (?) , Convertquotation = (?) , C_Reject_Reason_ID = (?) , validuntil = TO_DATE(?) , Quotation_ID = (?) , SO_Res_Status = (?) , Create_POLines = (?) , Iscashvat = (?) , RM_Pickfromreceipt = (?) , EM_APRM_AddPayment = (?) , EM_Atecco_Fecha_Pago = TO_DATE(?) , EM_Atecco_Borra_1 = (?) , EM_Atecco_Borra_2 = (?) , EM_Atecco_Transporte = (?) , EM_Atecco_Docaction = (?) , EM_Atecco_Imprimir = (?) , EM_Atecco_Tipo_Tarjeta = (?) , EM_Atecco_Banco = (?) , EM_Atecco_Docstatus = (?) , EM_Atecco_Iniciar = (?) , EM_Atecco_Procesar = (?) , EM_Atecco_Validar = (?) , EM_Atecco_Pagodescuento = (?) , EM_Atecco_Anticipo = (?) , EM_Atecco_Anticipostatus = (?) , DatePrinted = TO_DATE(?) , C_DocTypeTarget_ID = (?) , IsDelivered = (?) , IsInvoiced = (?) , IsPrinted = (?) , IsSelected = (?) , DocStatus = (?) , IsSOTrx = (?) , AD_Client_ID = (?) , IsActive = (?) , DocumentNo = (?) , AD_Org_ID = (?) , DocAction = (?) , Processing = (?) , C_Order_ID = (?) , Processed = (?) , C_DocType_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecTipoEvento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecTipoContratacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecFechaMontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecHoraMontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecFechaDesmontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecHoraDesmontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecRequiereTransporte);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecRequiereMontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentPriorityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmAddorphanline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createPolines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iscashvat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromreceipt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoFechaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBorra1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBorra2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTransporte);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoImprimir);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTipoTarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBanco);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoIniciar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoValidar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPagodescuento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAnticipostatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO C_Order " +
      "        (C_BPartner_ID, DateOrdered, em_dec_tipo_evento, C_BPartner_Location_ID, Description, DatePromised, em_dec_tipo_contratacion, em_dec_fecha_montaje, em_dec_hora_montaje, em_dec_fecha_desmontaje, em_dec_hora_desmontaje, em_dec_requiere_transporte, em_dec_requiere_montaje, SalesRep_ID, DateAcct, BillTo_ID, POReference, IsDiscountPrinted, C_Currency_ID, PaymentRule, C_PaymentTerm_ID, InvoiceRule, DeliveryRule, FreightCostRule, FreightAmt, DeliveryViaRule, M_Shipper_ID, C_Charge_ID, ChargeAmt, PriorityRule, TotalLines, GrandTotal, M_Warehouse_ID, M_PriceList_ID, IsTaxIncluded, C_Campaign_ID, C_Project_ID, C_Activity_ID, Posted, AD_User_ID, CopyFrom, DropShip_BPartner_ID, DropShip_Location_ID, DropShip_User_ID, IsSelfService, AD_OrgTrx_ID, User1_ID, User2_ID, Deliverynotes, C_Incoterms_ID, Incotermsdescription, Generatetemplate, Delivery_Location_ID, CopyFromPO, FIN_Paymentmethod_ID, FIN_Payment_Priority_ID, RM_PickFromShipment, RM_ReceiveMaterials, RM_CreateInvoice, C_Return_Reason_ID, RM_AddOrphanLine, A_Asset_ID, Calculate_Promotions, C_Costcenter_ID, Convertquotation, C_Reject_Reason_ID, validuntil, Quotation_ID, SO_Res_Status, Create_POLines, Iscashvat, RM_Pickfromreceipt, EM_APRM_AddPayment, EM_Atecco_Fecha_Pago, EM_Atecco_Borra_1, EM_Atecco_Borra_2, EM_Atecco_Transporte, EM_Atecco_Docaction, EM_Atecco_Imprimir, EM_Atecco_Tipo_Tarjeta, EM_Atecco_Banco, EM_Atecco_Docstatus, EM_Atecco_Iniciar, EM_Atecco_Procesar, EM_Atecco_Validar, EM_Atecco_Pagodescuento, EM_Atecco_Anticipo, EM_Atecco_Anticipostatus, DatePrinted, C_DocTypeTarget_ID, IsDelivered, IsInvoiced, IsPrinted, IsSelected, DocStatus, IsSOTrx, AD_Client_ID, IsActive, DocumentNo, AD_Org_ID, DocAction, Processing, C_Order_ID, Processed, C_DocType_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), TO_DATE(?), (?), (?), (?), TO_DATE(?), (?), TO_DATE(?), TO_TIMESTAMP(?, 'HH24:MI:SS'), TO_DATE(?), TO_TIMESTAMP(?, 'HH24:MI:SS'), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecTipoEvento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecTipoContratacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecFechaMontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecHoraMontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecFechaDesmontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecHoraDesmontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecRequiereTransporte);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecRequiereMontaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentPriorityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmAddorphanline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createPolines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iscashvat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromreceipt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoFechaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBorra1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBorra2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTransporte);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoImprimir);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTipoTarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBanco);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoIniciar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoValidar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPagodescuento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAnticipostatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM C_Order" +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}

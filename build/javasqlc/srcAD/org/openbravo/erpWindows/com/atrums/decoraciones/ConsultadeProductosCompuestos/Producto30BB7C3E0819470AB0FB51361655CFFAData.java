//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.decoraciones.ConsultadeProductosCompuestos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Producto30BB7C3E0819470AB0FB51361655CFFAData implements FieldProvider {
static Logger log4j = Logger.getLogger(Producto30BB7C3E0819470AB0FB51361655CFFAData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String value;
  public String adOrgId;
  public String emAteccoFamiliaId;
  public String emAteccoGenericoId;
  public String name;
  public String description;
  public String mProductCategoryId;
  public String mProductCategoryIdr;
  public String isactive;
  public String documentnote;
  public String help;
  public String upc;
  public String sku;
  public String cUomId;
  public String salesrepId;
  public String issummary;
  public String isstocked;
  public String ispurchased;
  public String issold;
  public String isbom;
  public String isinvoiceprintdetails;
  public String ispicklistprintdetails;
  public String isverified;
  public String classification;
  public String volume;
  public String weight;
  public String shelfwidth;
  public String shelfheight;
  public String shelfdepth;
  public String unitsperpallet;
  public String cTaxcategoryId;
  public String sResourceId;
  public String discontinued;
  public String discontinuedby;
  public String processing;
  public String sExpensetypeId;
  public String producttype;
  public String imageurl;
  public String descriptionurl;
  public String guaranteedays;
  public String versionno;
  public String mAttributesetId;
  public String mAttributesetinstanceId;
  public String downloadurl;
  public String mFreightcategoryId;
  public String mLocatorId;
  public String adImageId;
  public String cBpartnerId;
  public String ispriceprinted;
  public String name2;
  public String costtype;
  public String coststd;
  public String stockMin;
  public String enforceAttribute;
  public String calculated;
  public String maProcessplanId;
  public String production;
  public String capacity;
  public String delaymin;
  public String mrpPlannerId;
  public String mrpPlanningmethodId;
  public String qtymax;
  public String qtymin;
  public String qtystd;
  public String qtytype;
  public String stockmin;
  public String attrsetvaluetype;
  public String isquantityvariable;
  public String isdeferredrevenue;
  public String revplantype;
  public String periodnumber;
  public String isdeferredexpense;
  public String expplantype;
  public String periodnumberExp;
  public String defaultperiod;
  public String defaultperiodExp;
  public String bookusingpoprice;
  public String cUomWeightId;
  public String mBrandId;
  public String isgeneric;
  public String genericProductId;
  public String createvariants;
  public String characteristicDesc;
  public String updateinvariants;
  public String managevariants;
  public String prodCatSelection;
  public String productSelection;
  public String printDescription;
  public String returnable;
  public String overdueReturnDays;
  public String ispricerulebased;
  public String uniquePerDocument;
  public String emAteccoModelo;
  public String emAteccoTipo;
  public String emAteccoTextura;
  public String emAteccoCaras;
  public String islinkedtoproduct;
  public String emAteccoColor1;
  public String emAteccoColor2;
  public String emAteccoD1;
  public String relateprodcattoservice;
  public String emAteccoD2;
  public String relateprodtoservice;
  public String emAteccoEspesor;
  public String quantityRule;
  public String emAteccoM3;
  public String emAteccoCategoriaId;
  public String emAteccoSubCategoriaId;
  public String emAteccoCodigoEdimca;
  public String allowDeferredSell;
  public String emAteccoCodigoCotopaxi;
  public String deferredSellMaxDays;
  public String emDecVisible;
  public String adClientId;
  public String mProductId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("em_atecco_familia_id") || fieldName.equals("emAteccoFamiliaId"))
      return emAteccoFamiliaId;
    else if (fieldName.equalsIgnoreCase("em_atecco_generico_id") || fieldName.equals("emAteccoGenericoId"))
      return emAteccoGenericoId;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("m_product_category_id") || fieldName.equals("mProductCategoryId"))
      return mProductCategoryId;
    else if (fieldName.equalsIgnoreCase("m_product_category_idr") || fieldName.equals("mProductCategoryIdr"))
      return mProductCategoryIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("documentnote"))
      return documentnote;
    else if (fieldName.equalsIgnoreCase("help"))
      return help;
    else if (fieldName.equalsIgnoreCase("upc"))
      return upc;
    else if (fieldName.equalsIgnoreCase("sku"))
      return sku;
    else if (fieldName.equalsIgnoreCase("c_uom_id") || fieldName.equals("cUomId"))
      return cUomId;
    else if (fieldName.equalsIgnoreCase("salesrep_id") || fieldName.equals("salesrepId"))
      return salesrepId;
    else if (fieldName.equalsIgnoreCase("issummary"))
      return issummary;
    else if (fieldName.equalsIgnoreCase("isstocked"))
      return isstocked;
    else if (fieldName.equalsIgnoreCase("ispurchased"))
      return ispurchased;
    else if (fieldName.equalsIgnoreCase("issold"))
      return issold;
    else if (fieldName.equalsIgnoreCase("isbom"))
      return isbom;
    else if (fieldName.equalsIgnoreCase("isinvoiceprintdetails"))
      return isinvoiceprintdetails;
    else if (fieldName.equalsIgnoreCase("ispicklistprintdetails"))
      return ispicklistprintdetails;
    else if (fieldName.equalsIgnoreCase("isverified"))
      return isverified;
    else if (fieldName.equalsIgnoreCase("classification"))
      return classification;
    else if (fieldName.equalsIgnoreCase("volume"))
      return volume;
    else if (fieldName.equalsIgnoreCase("weight"))
      return weight;
    else if (fieldName.equalsIgnoreCase("shelfwidth"))
      return shelfwidth;
    else if (fieldName.equalsIgnoreCase("shelfheight"))
      return shelfheight;
    else if (fieldName.equalsIgnoreCase("shelfdepth"))
      return shelfdepth;
    else if (fieldName.equalsIgnoreCase("unitsperpallet"))
      return unitsperpallet;
    else if (fieldName.equalsIgnoreCase("c_taxcategory_id") || fieldName.equals("cTaxcategoryId"))
      return cTaxcategoryId;
    else if (fieldName.equalsIgnoreCase("s_resource_id") || fieldName.equals("sResourceId"))
      return sResourceId;
    else if (fieldName.equalsIgnoreCase("discontinued"))
      return discontinued;
    else if (fieldName.equalsIgnoreCase("discontinuedby"))
      return discontinuedby;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("s_expensetype_id") || fieldName.equals("sExpensetypeId"))
      return sExpensetypeId;
    else if (fieldName.equalsIgnoreCase("producttype"))
      return producttype;
    else if (fieldName.equalsIgnoreCase("imageurl"))
      return imageurl;
    else if (fieldName.equalsIgnoreCase("descriptionurl"))
      return descriptionurl;
    else if (fieldName.equalsIgnoreCase("guaranteedays"))
      return guaranteedays;
    else if (fieldName.equalsIgnoreCase("versionno"))
      return versionno;
    else if (fieldName.equalsIgnoreCase("m_attributeset_id") || fieldName.equals("mAttributesetId"))
      return mAttributesetId;
    else if (fieldName.equalsIgnoreCase("m_attributesetinstance_id") || fieldName.equals("mAttributesetinstanceId"))
      return mAttributesetinstanceId;
    else if (fieldName.equalsIgnoreCase("downloadurl"))
      return downloadurl;
    else if (fieldName.equalsIgnoreCase("m_freightcategory_id") || fieldName.equals("mFreightcategoryId"))
      return mFreightcategoryId;
    else if (fieldName.equalsIgnoreCase("m_locator_id") || fieldName.equals("mLocatorId"))
      return mLocatorId;
    else if (fieldName.equalsIgnoreCase("ad_image_id") || fieldName.equals("adImageId"))
      return adImageId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("ispriceprinted"))
      return ispriceprinted;
    else if (fieldName.equalsIgnoreCase("name2"))
      return name2;
    else if (fieldName.equalsIgnoreCase("costtype"))
      return costtype;
    else if (fieldName.equalsIgnoreCase("coststd"))
      return coststd;
    else if (fieldName.equalsIgnoreCase("stock_min") || fieldName.equals("stockMin"))
      return stockMin;
    else if (fieldName.equalsIgnoreCase("enforce_attribute") || fieldName.equals("enforceAttribute"))
      return enforceAttribute;
    else if (fieldName.equalsIgnoreCase("calculated"))
      return calculated;
    else if (fieldName.equalsIgnoreCase("ma_processplan_id") || fieldName.equals("maProcessplanId"))
      return maProcessplanId;
    else if (fieldName.equalsIgnoreCase("production"))
      return production;
    else if (fieldName.equalsIgnoreCase("capacity"))
      return capacity;
    else if (fieldName.equalsIgnoreCase("delaymin"))
      return delaymin;
    else if (fieldName.equalsIgnoreCase("mrp_planner_id") || fieldName.equals("mrpPlannerId"))
      return mrpPlannerId;
    else if (fieldName.equalsIgnoreCase("mrp_planningmethod_id") || fieldName.equals("mrpPlanningmethodId"))
      return mrpPlanningmethodId;
    else if (fieldName.equalsIgnoreCase("qtymax"))
      return qtymax;
    else if (fieldName.equalsIgnoreCase("qtymin"))
      return qtymin;
    else if (fieldName.equalsIgnoreCase("qtystd"))
      return qtystd;
    else if (fieldName.equalsIgnoreCase("qtytype"))
      return qtytype;
    else if (fieldName.equalsIgnoreCase("stockmin"))
      return stockmin;
    else if (fieldName.equalsIgnoreCase("attrsetvaluetype"))
      return attrsetvaluetype;
    else if (fieldName.equalsIgnoreCase("isquantityvariable"))
      return isquantityvariable;
    else if (fieldName.equalsIgnoreCase("isdeferredrevenue"))
      return isdeferredrevenue;
    else if (fieldName.equalsIgnoreCase("revplantype"))
      return revplantype;
    else if (fieldName.equalsIgnoreCase("periodnumber"))
      return periodnumber;
    else if (fieldName.equalsIgnoreCase("isdeferredexpense"))
      return isdeferredexpense;
    else if (fieldName.equalsIgnoreCase("expplantype"))
      return expplantype;
    else if (fieldName.equalsIgnoreCase("periodnumber_exp") || fieldName.equals("periodnumberExp"))
      return periodnumberExp;
    else if (fieldName.equalsIgnoreCase("defaultperiod"))
      return defaultperiod;
    else if (fieldName.equalsIgnoreCase("defaultperiod_exp") || fieldName.equals("defaultperiodExp"))
      return defaultperiodExp;
    else if (fieldName.equalsIgnoreCase("bookusingpoprice"))
      return bookusingpoprice;
    else if (fieldName.equalsIgnoreCase("c_uom_weight_id") || fieldName.equals("cUomWeightId"))
      return cUomWeightId;
    else if (fieldName.equalsIgnoreCase("m_brand_id") || fieldName.equals("mBrandId"))
      return mBrandId;
    else if (fieldName.equalsIgnoreCase("isgeneric"))
      return isgeneric;
    else if (fieldName.equalsIgnoreCase("generic_product_id") || fieldName.equals("genericProductId"))
      return genericProductId;
    else if (fieldName.equalsIgnoreCase("createvariants"))
      return createvariants;
    else if (fieldName.equalsIgnoreCase("characteristic_desc") || fieldName.equals("characteristicDesc"))
      return characteristicDesc;
    else if (fieldName.equalsIgnoreCase("updateinvariants"))
      return updateinvariants;
    else if (fieldName.equalsIgnoreCase("managevariants"))
      return managevariants;
    else if (fieldName.equalsIgnoreCase("prod_cat_selection") || fieldName.equals("prodCatSelection"))
      return prodCatSelection;
    else if (fieldName.equalsIgnoreCase("product_selection") || fieldName.equals("productSelection"))
      return productSelection;
    else if (fieldName.equalsIgnoreCase("print_description") || fieldName.equals("printDescription"))
      return printDescription;
    else if (fieldName.equalsIgnoreCase("returnable"))
      return returnable;
    else if (fieldName.equalsIgnoreCase("overdue_return_days") || fieldName.equals("overdueReturnDays"))
      return overdueReturnDays;
    else if (fieldName.equalsIgnoreCase("ispricerulebased"))
      return ispricerulebased;
    else if (fieldName.equalsIgnoreCase("unique_per_document") || fieldName.equals("uniquePerDocument"))
      return uniquePerDocument;
    else if (fieldName.equalsIgnoreCase("em_atecco_modelo") || fieldName.equals("emAteccoModelo"))
      return emAteccoModelo;
    else if (fieldName.equalsIgnoreCase("em_atecco_tipo") || fieldName.equals("emAteccoTipo"))
      return emAteccoTipo;
    else if (fieldName.equalsIgnoreCase("em_atecco_textura") || fieldName.equals("emAteccoTextura"))
      return emAteccoTextura;
    else if (fieldName.equalsIgnoreCase("em_atecco_caras") || fieldName.equals("emAteccoCaras"))
      return emAteccoCaras;
    else if (fieldName.equalsIgnoreCase("islinkedtoproduct"))
      return islinkedtoproduct;
    else if (fieldName.equalsIgnoreCase("em_atecco_color1") || fieldName.equals("emAteccoColor1"))
      return emAteccoColor1;
    else if (fieldName.equalsIgnoreCase("em_atecco_color2") || fieldName.equals("emAteccoColor2"))
      return emAteccoColor2;
    else if (fieldName.equalsIgnoreCase("em_atecco_d1") || fieldName.equals("emAteccoD1"))
      return emAteccoD1;
    else if (fieldName.equalsIgnoreCase("relateprodcattoservice"))
      return relateprodcattoservice;
    else if (fieldName.equalsIgnoreCase("em_atecco_d2") || fieldName.equals("emAteccoD2"))
      return emAteccoD2;
    else if (fieldName.equalsIgnoreCase("relateprodtoservice"))
      return relateprodtoservice;
    else if (fieldName.equalsIgnoreCase("em_atecco_espesor") || fieldName.equals("emAteccoEspesor"))
      return emAteccoEspesor;
    else if (fieldName.equalsIgnoreCase("quantity_rule") || fieldName.equals("quantityRule"))
      return quantityRule;
    else if (fieldName.equalsIgnoreCase("em_atecco_m3") || fieldName.equals("emAteccoM3"))
      return emAteccoM3;
    else if (fieldName.equalsIgnoreCase("em_atecco_categoria_id") || fieldName.equals("emAteccoCategoriaId"))
      return emAteccoCategoriaId;
    else if (fieldName.equalsIgnoreCase("em_atecco_sub_categoria_id") || fieldName.equals("emAteccoSubCategoriaId"))
      return emAteccoSubCategoriaId;
    else if (fieldName.equalsIgnoreCase("em_atecco_codigo_edimca") || fieldName.equals("emAteccoCodigoEdimca"))
      return emAteccoCodigoEdimca;
    else if (fieldName.equalsIgnoreCase("allow_deferred_sell") || fieldName.equals("allowDeferredSell"))
      return allowDeferredSell;
    else if (fieldName.equalsIgnoreCase("em_atecco_codigo_cotopaxi") || fieldName.equals("emAteccoCodigoCotopaxi"))
      return emAteccoCodigoCotopaxi;
    else if (fieldName.equalsIgnoreCase("deferred_sell_max_days") || fieldName.equals("deferredSellMaxDays"))
      return deferredSellMaxDays;
    else if (fieldName.equalsIgnoreCase("em_dec_visible") || fieldName.equals("emDecVisible"))
      return emDecVisible;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Producto30BB7C3E0819470AB0FB51361655CFFAData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Producto30BB7C3E0819470AB0FB51361655CFFAData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(M_Product.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.CreatedBy) as CreatedByR, " +
      "        to_char(M_Product.Updated, ?) as updated, " +
      "        to_char(M_Product.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        M_Product.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.UpdatedBy) as UpdatedByR," +
      "        M_Product.Value, " +
      "M_Product.AD_Org_ID, " +
      "M_Product.EM_Atecco_Familia_ID, " +
      "M_Product.EM_Atecco_Generico_ID, " +
      "M_Product.Name, " +
      "M_Product.Description, " +
      "M_Product.M_Product_Category_ID, " +
      "(CASE WHEN M_Product.M_Product_Category_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS M_Product_Category_IDR, " +
      "COALESCE(M_Product.IsActive, 'N') AS IsActive, " +
      "M_Product.DocumentNote, " +
      "M_Product.Help, " +
      "M_Product.UPC, " +
      "M_Product.SKU, " +
      "M_Product.C_UOM_ID, " +
      "M_Product.SalesRep_ID, " +
      "COALESCE(M_Product.IsSummary, 'N') AS IsSummary, " +
      "COALESCE(M_Product.IsStocked, 'N') AS IsStocked, " +
      "COALESCE(M_Product.IsPurchased, 'N') AS IsPurchased, " +
      "COALESCE(M_Product.IsSold, 'N') AS IsSold, " +
      "COALESCE(M_Product.IsBOM, 'N') AS IsBOM, " +
      "COALESCE(M_Product.IsInvoicePrintDetails, 'N') AS IsInvoicePrintDetails, " +
      "COALESCE(M_Product.IsPickListPrintDetails, 'N') AS IsPickListPrintDetails, " +
      "COALESCE(M_Product.IsVerified, 'N') AS IsVerified, " +
      "M_Product.Classification, " +
      "M_Product.Volume, " +
      "M_Product.Weight, " +
      "M_Product.ShelfWidth, " +
      "M_Product.ShelfHeight, " +
      "M_Product.ShelfDepth, " +
      "M_Product.UnitsPerPallet, " +
      "M_Product.C_TaxCategory_ID, " +
      "M_Product.S_Resource_ID, " +
      "COALESCE(M_Product.Discontinued, 'N') AS Discontinued, " +
      "M_Product.DiscontinuedBy, " +
      "M_Product.Processing, " +
      "M_Product.S_ExpenseType_ID, " +
      "M_Product.ProductType, " +
      "M_Product.ImageURL, " +
      "M_Product.DescriptionURL, " +
      "M_Product.GuaranteeDays, " +
      "M_Product.VersionNo, " +
      "M_Product.M_AttributeSet_ID, " +
      "M_Product.M_AttributeSetInstance_ID, " +
      "M_Product.DownloadURL, " +
      "M_Product.M_FreightCategory_ID, " +
      "M_Product.M_Locator_ID, " +
      "M_Product.AD_Image_ID, " +
      "M_Product.C_BPartner_ID, " +
      "COALESCE(M_Product.Ispriceprinted, 'N') AS Ispriceprinted, " +
      "M_Product.Name2, " +
      "M_Product.Costtype, " +
      "M_Product.Coststd, " +
      "M_Product.Stock_Min, " +
      "COALESCE(M_Product.Enforce_Attribute, 'N') AS Enforce_Attribute, " +
      "COALESCE(M_Product.Calculated, 'N') AS Calculated, " +
      "M_Product.MA_Processplan_ID, " +
      "COALESCE(M_Product.Production, 'N') AS Production, " +
      "M_Product.Capacity, " +
      "M_Product.Delaymin, " +
      "M_Product.MRP_Planner_ID, " +
      "M_Product.MRP_Planningmethod_ID, " +
      "M_Product.Qtymax, " +
      "M_Product.Qtymin, " +
      "M_Product.Qtystd, " +
      "COALESCE(M_Product.Qtytype, 'N') AS Qtytype, " +
      "M_Product.Stockmin, " +
      "M_Product.Attrsetvaluetype, " +
      "COALESCE(M_Product.Isquantityvariable, 'N') AS Isquantityvariable, " +
      "COALESCE(M_Product.Isdeferredrevenue, 'N') AS Isdeferredrevenue, " +
      "M_Product.Revplantype, " +
      "M_Product.Periodnumber, " +
      "COALESCE(M_Product.Isdeferredexpense, 'N') AS Isdeferredexpense, " +
      "M_Product.Expplantype, " +
      "M_Product.Periodnumber_Exp, " +
      "M_Product.DefaultPeriod, " +
      "M_Product.DefaultPeriod_Exp, " +
      "COALESCE(M_Product.Bookusingpoprice, 'N') AS Bookusingpoprice, " +
      "M_Product.C_Uom_Weight_ID, " +
      "M_Product.M_Brand_ID, " +
      "COALESCE(M_Product.IsGeneric, 'N') AS IsGeneric, " +
      "M_Product.Generic_Product_ID, " +
      "M_Product.CreateVariants, " +
      "M_Product.Characteristic_Desc, " +
      "M_Product.Updateinvariants, " +
      "M_Product.ManageVariants, " +
      "M_Product.Prod_Cat_Selection, " +
      "M_Product.Product_Selection, " +
      "COALESCE(M_Product.Print_Description, 'N') AS Print_Description, " +
      "COALESCE(M_Product.Returnable, 'N') AS Returnable, " +
      "M_Product.Overdue_Return_Days, " +
      "COALESCE(M_Product.Ispricerulebased, 'N') AS Ispricerulebased, " +
      "COALESCE(M_Product.Unique_Per_Document, 'N') AS Unique_Per_Document, " +
      "M_Product.EM_Atecco_Modelo, " +
      "M_Product.EM_Atecco_Tipo, " +
      "M_Product.EM_Atecco_Textura, " +
      "M_Product.EM_Atecco_Caras, " +
      "COALESCE(M_Product.Islinkedtoproduct, 'N') AS Islinkedtoproduct, " +
      "M_Product.EM_Atecco_Color1, " +
      "M_Product.EM_Atecco_Color2, " +
      "M_Product.EM_Atecco_D1, " +
      "M_Product.Relateprodcattoservice, " +
      "M_Product.EM_Atecco_D2, " +
      "M_Product.Relateprodtoservice, " +
      "M_Product.EM_Atecco_Espesor, " +
      "M_Product.Quantity_Rule, " +
      "M_Product.EM_Atecco_M3, " +
      "M_Product.EM_Atecco_Categoria_ID, " +
      "M_Product.EM_Atecco_Sub_Categoria_ID, " +
      "M_Product.EM_Atecco_Codigo_Edimca, " +
      "COALESCE(M_Product.Allow_Deferred_Sell, 'N') AS Allow_Deferred_Sell, " +
      "M_Product.EM_Atecco_Codigo_Cotopaxi, " +
      "M_Product.Deferred_Sell_Max_Days, " +
      "COALESCE(M_Product.em_dec_visible, 'N') AS em_dec_visible, " +
      "M_Product.AD_Client_ID, " +
      "M_Product.M_Product_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM M_Product left join (select M_Product_Category_ID, Name from M_Product_Category) table1 on (M_Product.M_Product_Category_ID = table1.M_Product_Category_ID) left join (select M_Product_Category_ID,AD_Language, Name from M_Product_Category_TRL) tableTRL1 on (table1.M_Product_Category_ID = tableTRL1.M_Product_Category_ID and tableTRL1.AD_Language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND M_Product.M_Product_ID = ? " +
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Producto30BB7C3E0819470AB0FB51361655CFFAData objectProducto30BB7C3E0819470AB0FB51361655CFFAData = new Producto30BB7C3E0819470AB0FB51361655CFFAData();
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.created = UtilSql.getValue(result, "created");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updated = UtilSql.getValue(result, "updated");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updatedby = UtilSql.getValue(result, "updatedby");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.value = UtilSql.getValue(result, "value");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoFamiliaId = UtilSql.getValue(result, "em_atecco_familia_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoGenericoId = UtilSql.getValue(result, "em_atecco_generico_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.name = UtilSql.getValue(result, "name");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.description = UtilSql.getValue(result, "description");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mProductCategoryId = UtilSql.getValue(result, "m_product_category_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mProductCategoryIdr = UtilSql.getValue(result, "m_product_category_idr");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isactive = UtilSql.getValue(result, "isactive");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.documentnote = UtilSql.getValue(result, "documentnote");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.help = UtilSql.getValue(result, "help");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.upc = UtilSql.getValue(result, "upc");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.sku = UtilSql.getValue(result, "sku");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.issummary = UtilSql.getValue(result, "issummary");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isstocked = UtilSql.getValue(result, "isstocked");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.ispurchased = UtilSql.getValue(result, "ispurchased");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.issold = UtilSql.getValue(result, "issold");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isbom = UtilSql.getValue(result, "isbom");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isinvoiceprintdetails = UtilSql.getValue(result, "isinvoiceprintdetails");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.ispicklistprintdetails = UtilSql.getValue(result, "ispicklistprintdetails");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isverified = UtilSql.getValue(result, "isverified");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.classification = UtilSql.getValue(result, "classification");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.volume = UtilSql.getValue(result, "volume");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.weight = UtilSql.getValue(result, "weight");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.shelfwidth = UtilSql.getValue(result, "shelfwidth");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.shelfheight = UtilSql.getValue(result, "shelfheight");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.shelfdepth = UtilSql.getValue(result, "shelfdepth");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.unitsperpallet = UtilSql.getValue(result, "unitsperpallet");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.cTaxcategoryId = UtilSql.getValue(result, "c_taxcategory_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.sResourceId = UtilSql.getValue(result, "s_resource_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.discontinued = UtilSql.getValue(result, "discontinued");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.discontinuedby = UtilSql.getDateValue(result, "discontinuedby", "dd-MM-yyyy");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.processing = UtilSql.getValue(result, "processing");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.sExpensetypeId = UtilSql.getValue(result, "s_expensetype_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.producttype = UtilSql.getValue(result, "producttype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.imageurl = UtilSql.getValue(result, "imageurl");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.descriptionurl = UtilSql.getValue(result, "descriptionurl");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.guaranteedays = UtilSql.getValue(result, "guaranteedays");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.versionno = UtilSql.getValue(result, "versionno");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mAttributesetId = UtilSql.getValue(result, "m_attributeset_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mAttributesetinstanceId = UtilSql.getValue(result, "m_attributesetinstance_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.downloadurl = UtilSql.getValue(result, "downloadurl");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mFreightcategoryId = UtilSql.getValue(result, "m_freightcategory_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mLocatorId = UtilSql.getValue(result, "m_locator_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adImageId = UtilSql.getValue(result, "ad_image_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.ispriceprinted = UtilSql.getValue(result, "ispriceprinted");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.name2 = UtilSql.getValue(result, "name2");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.costtype = UtilSql.getValue(result, "costtype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.coststd = UtilSql.getValue(result, "coststd");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.stockMin = UtilSql.getValue(result, "stock_min");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.enforceAttribute = UtilSql.getValue(result, "enforce_attribute");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.calculated = UtilSql.getValue(result, "calculated");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.maProcessplanId = UtilSql.getValue(result, "ma_processplan_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.production = UtilSql.getValue(result, "production");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.capacity = UtilSql.getValue(result, "capacity");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.delaymin = UtilSql.getValue(result, "delaymin");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mrpPlannerId = UtilSql.getValue(result, "mrp_planner_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mrpPlanningmethodId = UtilSql.getValue(result, "mrp_planningmethod_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.qtymax = UtilSql.getValue(result, "qtymax");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.qtymin = UtilSql.getValue(result, "qtymin");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.qtystd = UtilSql.getValue(result, "qtystd");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.qtytype = UtilSql.getValue(result, "qtytype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.stockmin = UtilSql.getValue(result, "stockmin");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.attrsetvaluetype = UtilSql.getValue(result, "attrsetvaluetype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isquantityvariable = UtilSql.getValue(result, "isquantityvariable");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isdeferredrevenue = UtilSql.getValue(result, "isdeferredrevenue");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.revplantype = UtilSql.getValue(result, "revplantype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.periodnumber = UtilSql.getValue(result, "periodnumber");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isdeferredexpense = UtilSql.getValue(result, "isdeferredexpense");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.expplantype = UtilSql.getValue(result, "expplantype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.periodnumberExp = UtilSql.getValue(result, "periodnumber_exp");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.defaultperiod = UtilSql.getValue(result, "defaultperiod");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.defaultperiodExp = UtilSql.getValue(result, "defaultperiod_exp");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.bookusingpoprice = UtilSql.getValue(result, "bookusingpoprice");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.cUomWeightId = UtilSql.getValue(result, "c_uom_weight_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mBrandId = UtilSql.getValue(result, "m_brand_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isgeneric = UtilSql.getValue(result, "isgeneric");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.genericProductId = UtilSql.getValue(result, "generic_product_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.createvariants = UtilSql.getValue(result, "createvariants");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.characteristicDesc = UtilSql.getValue(result, "characteristic_desc");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updateinvariants = UtilSql.getValue(result, "updateinvariants");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.managevariants = UtilSql.getValue(result, "managevariants");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.prodCatSelection = UtilSql.getValue(result, "prod_cat_selection");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.productSelection = UtilSql.getValue(result, "product_selection");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.printDescription = UtilSql.getValue(result, "print_description");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.returnable = UtilSql.getValue(result, "returnable");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.overdueReturnDays = UtilSql.getValue(result, "overdue_return_days");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.ispricerulebased = UtilSql.getValue(result, "ispricerulebased");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.uniquePerDocument = UtilSql.getValue(result, "unique_per_document");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoModelo = UtilSql.getValue(result, "em_atecco_modelo");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoTipo = UtilSql.getValue(result, "em_atecco_tipo");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoTextura = UtilSql.getValue(result, "em_atecco_textura");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoCaras = UtilSql.getValue(result, "em_atecco_caras");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.islinkedtoproduct = UtilSql.getValue(result, "islinkedtoproduct");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoColor1 = UtilSql.getValue(result, "em_atecco_color1");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoColor2 = UtilSql.getValue(result, "em_atecco_color2");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoD1 = UtilSql.getValue(result, "em_atecco_d1");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.relateprodcattoservice = UtilSql.getValue(result, "relateprodcattoservice");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoD2 = UtilSql.getValue(result, "em_atecco_d2");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.relateprodtoservice = UtilSql.getValue(result, "relateprodtoservice");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoEspesor = UtilSql.getValue(result, "em_atecco_espesor");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.quantityRule = UtilSql.getValue(result, "quantity_rule");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoM3 = UtilSql.getValue(result, "em_atecco_m3");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoCategoriaId = UtilSql.getValue(result, "em_atecco_categoria_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoSubCategoriaId = UtilSql.getValue(result, "em_atecco_sub_categoria_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoCodigoEdimca = UtilSql.getValue(result, "em_atecco_codigo_edimca");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.allowDeferredSell = UtilSql.getValue(result, "allow_deferred_sell");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoCodigoCotopaxi = UtilSql.getValue(result, "em_atecco_codigo_cotopaxi");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.deferredSellMaxDays = UtilSql.getValue(result, "deferred_sell_max_days");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emDecVisible = UtilSql.getValue(result, "em_dec_visible");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mProductId = UtilSql.getValue(result, "m_product_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.language = UtilSql.getValue(result, "language");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adUserClient = "";
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adOrgClient = "";
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.createdby = "";
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.trBgcolor = "";
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.totalCount = "";
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProducto30BB7C3E0819470AB0FB51361655CFFAData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Producto30BB7C3E0819470AB0FB51361655CFFAData objectProducto30BB7C3E0819470AB0FB51361655CFFAData[] = new Producto30BB7C3E0819470AB0FB51361655CFFAData[vector.size()];
    vector.copyInto(objectProducto30BB7C3E0819470AB0FB51361655CFFAData);
    return(objectProducto30BB7C3E0819470AB0FB51361655CFFAData);
  }

/**
Select for relation
 */
  public static Producto30BB7C3E0819470AB0FB51361655CFFAData[] select(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String parValue, String parName, String parM_Product_Category_ID, String parProductType, String parC_TaxCategory_ID, String parSessionDate, String parSessionUser, String adUserClient, String adOrgClient, String orderbyclause)    throws ServletException {
    return select(connectionProvider, dateTimeFormat, paramLanguage, parValue, parName, parM_Product_Category_ID, parProductType, parC_TaxCategory_ID, parSessionDate, parSessionUser, adUserClient, adOrgClient, orderbyclause, 0, 0);
  }

/**
Select for relation
 */
  public static Producto30BB7C3E0819470AB0FB51361655CFFAData[] select(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String parValue, String parName, String parM_Product_Category_ID, String parProductType, String parC_TaxCategory_ID, String parSessionDate, String parSessionUser, String adUserClient, String adOrgClient, String orderbyclause, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(M_Product.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.CreatedBy) as CreatedByR, " +
      "        to_char(M_Product.Updated, ?) as updated, " +
      "        to_char(M_Product.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        M_Product.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.UpdatedBy) as UpdatedByR, " +
      "        M_Product.Value, " +
      "M_Product.AD_Org_ID, " +
      "M_Product.EM_Atecco_Familia_ID, " +
      "M_Product.EM_Atecco_Generico_ID, " +
      "M_Product.Name, " +
      "M_Product.Description, " +
      "M_Product.M_Product_Category_ID, " +
      "(CASE WHEN M_Product.M_Product_Category_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS M_Product_Category_IDR, " +
      "COALESCE(M_Product.IsActive, 'N') AS IsActive, " +
      "M_Product.DocumentNote, " +
      "M_Product.Help, " +
      "M_Product.UPC, " +
      "M_Product.SKU, " +
      "M_Product.C_UOM_ID, " +
      "M_Product.SalesRep_ID, " +
      "COALESCE(M_Product.IsSummary, 'N') AS IsSummary, " +
      "COALESCE(M_Product.IsStocked, 'N') AS IsStocked, " +
      "COALESCE(M_Product.IsPurchased, 'N') AS IsPurchased, " +
      "COALESCE(M_Product.IsSold, 'N') AS IsSold, " +
      "COALESCE(M_Product.IsBOM, 'N') AS IsBOM, " +
      "COALESCE(M_Product.IsInvoicePrintDetails, 'N') AS IsInvoicePrintDetails, " +
      "COALESCE(M_Product.IsPickListPrintDetails, 'N') AS IsPickListPrintDetails, " +
      "COALESCE(M_Product.IsVerified, 'N') AS IsVerified, " +
      "M_Product.Classification, " +
      "M_Product.Volume, " +
      "M_Product.Weight, " +
      "M_Product.ShelfWidth, " +
      "M_Product.ShelfHeight, " +
      "M_Product.ShelfDepth, " +
      "M_Product.UnitsPerPallet, " +
      "M_Product.C_TaxCategory_ID, " +
      "M_Product.S_Resource_ID, " +
      "COALESCE(M_Product.Discontinued, 'N') AS Discontinued, " +
      "M_Product.DiscontinuedBy, " +
      "M_Product.Processing, " +
      "M_Product.S_ExpenseType_ID, " +
      "M_Product.ProductType, " +
      "M_Product.ImageURL, " +
      "M_Product.DescriptionURL, " +
      "M_Product.GuaranteeDays, " +
      "M_Product.VersionNo, " +
      "M_Product.M_AttributeSet_ID, " +
      "M_Product.M_AttributeSetInstance_ID, " +
      "M_Product.DownloadURL, " +
      "M_Product.M_FreightCategory_ID, " +
      "M_Product.M_Locator_ID, " +
      "M_Product.AD_Image_ID, " +
      "M_Product.C_BPartner_ID, " +
      "COALESCE(M_Product.Ispriceprinted, 'N') AS Ispriceprinted, " +
      "M_Product.Name2, " +
      "M_Product.Costtype, " +
      "M_Product.Coststd, " +
      "M_Product.Stock_Min, " +
      "COALESCE(M_Product.Enforce_Attribute, 'N') AS Enforce_Attribute, " +
      "COALESCE(M_Product.Calculated, 'N') AS Calculated, " +
      "M_Product.MA_Processplan_ID, " +
      "COALESCE(M_Product.Production, 'N') AS Production, " +
      "M_Product.Capacity, " +
      "M_Product.Delaymin, " +
      "M_Product.MRP_Planner_ID, " +
      "M_Product.MRP_Planningmethod_ID, " +
      "M_Product.Qtymax, " +
      "M_Product.Qtymin, " +
      "M_Product.Qtystd, " +
      "COALESCE(M_Product.Qtytype, 'N') AS Qtytype, " +
      "M_Product.Stockmin, " +
      "M_Product.Attrsetvaluetype, " +
      "COALESCE(M_Product.Isquantityvariable, 'N') AS Isquantityvariable, " +
      "COALESCE(M_Product.Isdeferredrevenue, 'N') AS Isdeferredrevenue, " +
      "M_Product.Revplantype, " +
      "M_Product.Periodnumber, " +
      "COALESCE(M_Product.Isdeferredexpense, 'N') AS Isdeferredexpense, " +
      "M_Product.Expplantype, " +
      "M_Product.Periodnumber_Exp, " +
      "M_Product.DefaultPeriod, " +
      "M_Product.DefaultPeriod_Exp, " +
      "COALESCE(M_Product.Bookusingpoprice, 'N') AS Bookusingpoprice, " +
      "M_Product.C_Uom_Weight_ID, " +
      "M_Product.M_Brand_ID, " +
      "COALESCE(M_Product.IsGeneric, 'N') AS IsGeneric, " +
      "M_Product.Generic_Product_ID, " +
      "M_Product.CreateVariants, " +
      "M_Product.Characteristic_Desc, " +
      "M_Product.Updateinvariants, " +
      "M_Product.ManageVariants, " +
      "M_Product.Prod_Cat_Selection, " +
      "M_Product.Product_Selection, " +
      "COALESCE(M_Product.Print_Description, 'N') AS Print_Description, " +
      "COALESCE(M_Product.Returnable, 'N') AS Returnable, " +
      "M_Product.Overdue_Return_Days, " +
      "COALESCE(M_Product.Ispricerulebased, 'N') AS Ispricerulebased, " +
      "COALESCE(M_Product.Unique_Per_Document, 'N') AS Unique_Per_Document, " +
      "M_Product.EM_Atecco_Modelo, " +
      "M_Product.EM_Atecco_Tipo, " +
      "M_Product.EM_Atecco_Textura, " +
      "M_Product.EM_Atecco_Caras, " +
      "COALESCE(M_Product.Islinkedtoproduct, 'N') AS Islinkedtoproduct, " +
      "M_Product.EM_Atecco_Color1, " +
      "M_Product.EM_Atecco_Color2, " +
      "M_Product.EM_Atecco_D1, " +
      "M_Product.Relateprodcattoservice, " +
      "M_Product.EM_Atecco_D2, " +
      "M_Product.Relateprodtoservice, " +
      "M_Product.EM_Atecco_Espesor, " +
      "M_Product.Quantity_Rule, " +
      "M_Product.EM_Atecco_M3, " +
      "M_Product.EM_Atecco_Categoria_ID, " +
      "M_Product.EM_Atecco_Sub_Categoria_ID, " +
      "M_Product.EM_Atecco_Codigo_Edimca, " +
      "COALESCE(M_Product.Allow_Deferred_Sell, 'N') AS Allow_Deferred_Sell, " +
      "M_Product.EM_Atecco_Codigo_Cotopaxi, " +
      "M_Product.Deferred_Sell_Max_Days, " +
      "COALESCE(M_Product.em_dec_visible, 'N') AS em_dec_visible, " +
      "M_Product.AD_Client_ID, " +
      "M_Product.M_Product_ID, " +
      "        '' AS TR_BGCOLOR, '' as total_count," +
      "        ? AS LANGUAGE, '' AS AD_USER_CLIENT, '' AS AD_ORG_CLIENT" +
      "        FROM M_Product left join (select M_Product_Category_ID, Name from M_Product_Category) table1 on (M_Product.M_Product_Category_ID = table1.M_Product_Category_ID) left join (select M_Product_Category_ID,AD_Language, Name from M_Product_Category_TRL) tableTRL1 on (table1.M_Product_Category_ID = tableTRL1.M_Product_Category_ID and tableTRL1.AD_Language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((parValue==null || parValue.equals(""))?"":"  AND (M_Product.Value) LIKE (?) ");
    strSql = strSql + ((parName==null || parName.equals(""))?"":"  AND C_IGNORE_ACCENT(M_Product.Name) LIKE C_IGNORE_ACCENT(?) ");
    strSql = strSql + ((parM_Product_Category_ID==null || parM_Product_Category_ID.equals(""))?"":"  AND (M_Product.M_Product_Category_ID) = (?) ");
    strSql = strSql + ((parProductType==null || parProductType.equals(""))?"":"  AND (M_Product.ProductType) = (?) ");
    strSql = strSql + ((parC_TaxCategory_ID==null || parC_TaxCategory_ID.equals(""))?"":"  AND (M_Product.C_TaxCategory_ID) = (?) ");
    strSql = strSql + ((parSessionDate.equals("parSessionDate"))?"  AND 1 = 1 ":"");
    strSql = strSql + ((parSessionUser.equals("parSessionUser"))?"  AND 1 = 1 ":"");
    strSql = strSql + 
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "        ORDER BY ";
    strSql = strSql + ((orderbyclause==null || orderbyclause.equals(""))?"":orderbyclause);
    strSql = strSql + 
      ", 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (parValue != null && !(parValue.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parValue);
      }
      if (parName != null && !(parName.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parName);
      }
      if (parM_Product_Category_ID != null && !(parM_Product_Category_ID.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parM_Product_Category_ID);
      }
      if (parProductType != null && !(parProductType.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parProductType);
      }
      if (parC_TaxCategory_ID != null && !(parC_TaxCategory_ID.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parC_TaxCategory_ID);
      }
      if (parSessionDate != null && !(parSessionDate.equals(""))) {
        }
      if (parSessionUser != null && !(parSessionUser.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (orderbyclause != null && !(orderbyclause.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Producto30BB7C3E0819470AB0FB51361655CFFAData objectProducto30BB7C3E0819470AB0FB51361655CFFAData = new Producto30BB7C3E0819470AB0FB51361655CFFAData();
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.created = UtilSql.getValue(result, "created");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updated = UtilSql.getValue(result, "updated");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updatedby = UtilSql.getValue(result, "updatedby");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.value = UtilSql.getValue(result, "value");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoFamiliaId = UtilSql.getValue(result, "em_atecco_familia_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoGenericoId = UtilSql.getValue(result, "em_atecco_generico_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.name = UtilSql.getValue(result, "name");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.description = UtilSql.getValue(result, "description");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mProductCategoryId = UtilSql.getValue(result, "m_product_category_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mProductCategoryIdr = UtilSql.getValue(result, "m_product_category_idr");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isactive = UtilSql.getValue(result, "isactive");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.documentnote = UtilSql.getValue(result, "documentnote");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.help = UtilSql.getValue(result, "help");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.upc = UtilSql.getValue(result, "upc");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.sku = UtilSql.getValue(result, "sku");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.issummary = UtilSql.getValue(result, "issummary");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isstocked = UtilSql.getValue(result, "isstocked");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.ispurchased = UtilSql.getValue(result, "ispurchased");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.issold = UtilSql.getValue(result, "issold");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isbom = UtilSql.getValue(result, "isbom");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isinvoiceprintdetails = UtilSql.getValue(result, "isinvoiceprintdetails");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.ispicklistprintdetails = UtilSql.getValue(result, "ispicklistprintdetails");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isverified = UtilSql.getValue(result, "isverified");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.classification = UtilSql.getValue(result, "classification");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.volume = UtilSql.getValue(result, "volume");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.weight = UtilSql.getValue(result, "weight");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.shelfwidth = UtilSql.getValue(result, "shelfwidth");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.shelfheight = UtilSql.getValue(result, "shelfheight");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.shelfdepth = UtilSql.getValue(result, "shelfdepth");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.unitsperpallet = UtilSql.getValue(result, "unitsperpallet");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.cTaxcategoryId = UtilSql.getValue(result, "c_taxcategory_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.sResourceId = UtilSql.getValue(result, "s_resource_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.discontinued = UtilSql.getValue(result, "discontinued");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.discontinuedby = UtilSql.getDateValue(result, "discontinuedby", "dd-MM-yyyy");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.processing = UtilSql.getValue(result, "processing");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.sExpensetypeId = UtilSql.getValue(result, "s_expensetype_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.producttype = UtilSql.getValue(result, "producttype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.imageurl = UtilSql.getValue(result, "imageurl");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.descriptionurl = UtilSql.getValue(result, "descriptionurl");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.guaranteedays = UtilSql.getValue(result, "guaranteedays");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.versionno = UtilSql.getValue(result, "versionno");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mAttributesetId = UtilSql.getValue(result, "m_attributeset_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mAttributesetinstanceId = UtilSql.getValue(result, "m_attributesetinstance_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.downloadurl = UtilSql.getValue(result, "downloadurl");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mFreightcategoryId = UtilSql.getValue(result, "m_freightcategory_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mLocatorId = UtilSql.getValue(result, "m_locator_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adImageId = UtilSql.getValue(result, "ad_image_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.ispriceprinted = UtilSql.getValue(result, "ispriceprinted");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.name2 = UtilSql.getValue(result, "name2");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.costtype = UtilSql.getValue(result, "costtype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.coststd = UtilSql.getValue(result, "coststd");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.stockMin = UtilSql.getValue(result, "stock_min");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.enforceAttribute = UtilSql.getValue(result, "enforce_attribute");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.calculated = UtilSql.getValue(result, "calculated");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.maProcessplanId = UtilSql.getValue(result, "ma_processplan_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.production = UtilSql.getValue(result, "production");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.capacity = UtilSql.getValue(result, "capacity");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.delaymin = UtilSql.getValue(result, "delaymin");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mrpPlannerId = UtilSql.getValue(result, "mrp_planner_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mrpPlanningmethodId = UtilSql.getValue(result, "mrp_planningmethod_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.qtymax = UtilSql.getValue(result, "qtymax");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.qtymin = UtilSql.getValue(result, "qtymin");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.qtystd = UtilSql.getValue(result, "qtystd");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.qtytype = UtilSql.getValue(result, "qtytype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.stockmin = UtilSql.getValue(result, "stockmin");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.attrsetvaluetype = UtilSql.getValue(result, "attrsetvaluetype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isquantityvariable = UtilSql.getValue(result, "isquantityvariable");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isdeferredrevenue = UtilSql.getValue(result, "isdeferredrevenue");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.revplantype = UtilSql.getValue(result, "revplantype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.periodnumber = UtilSql.getValue(result, "periodnumber");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isdeferredexpense = UtilSql.getValue(result, "isdeferredexpense");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.expplantype = UtilSql.getValue(result, "expplantype");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.periodnumberExp = UtilSql.getValue(result, "periodnumber_exp");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.defaultperiod = UtilSql.getValue(result, "defaultperiod");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.defaultperiodExp = UtilSql.getValue(result, "defaultperiod_exp");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.bookusingpoprice = UtilSql.getValue(result, "bookusingpoprice");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.cUomWeightId = UtilSql.getValue(result, "c_uom_weight_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mBrandId = UtilSql.getValue(result, "m_brand_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.isgeneric = UtilSql.getValue(result, "isgeneric");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.genericProductId = UtilSql.getValue(result, "generic_product_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.createvariants = UtilSql.getValue(result, "createvariants");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.characteristicDesc = UtilSql.getValue(result, "characteristic_desc");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.updateinvariants = UtilSql.getValue(result, "updateinvariants");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.managevariants = UtilSql.getValue(result, "managevariants");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.prodCatSelection = UtilSql.getValue(result, "prod_cat_selection");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.productSelection = UtilSql.getValue(result, "product_selection");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.printDescription = UtilSql.getValue(result, "print_description");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.returnable = UtilSql.getValue(result, "returnable");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.overdueReturnDays = UtilSql.getValue(result, "overdue_return_days");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.ispricerulebased = UtilSql.getValue(result, "ispricerulebased");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.uniquePerDocument = UtilSql.getValue(result, "unique_per_document");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoModelo = UtilSql.getValue(result, "em_atecco_modelo");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoTipo = UtilSql.getValue(result, "em_atecco_tipo");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoTextura = UtilSql.getValue(result, "em_atecco_textura");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoCaras = UtilSql.getValue(result, "em_atecco_caras");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.islinkedtoproduct = UtilSql.getValue(result, "islinkedtoproduct");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoColor1 = UtilSql.getValue(result, "em_atecco_color1");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoColor2 = UtilSql.getValue(result, "em_atecco_color2");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoD1 = UtilSql.getValue(result, "em_atecco_d1");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.relateprodcattoservice = UtilSql.getValue(result, "relateprodcattoservice");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoD2 = UtilSql.getValue(result, "em_atecco_d2");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.relateprodtoservice = UtilSql.getValue(result, "relateprodtoservice");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoEspesor = UtilSql.getValue(result, "em_atecco_espesor");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.quantityRule = UtilSql.getValue(result, "quantity_rule");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoM3 = UtilSql.getValue(result, "em_atecco_m3");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoCategoriaId = UtilSql.getValue(result, "em_atecco_categoria_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoSubCategoriaId = UtilSql.getValue(result, "em_atecco_sub_categoria_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoCodigoEdimca = UtilSql.getValue(result, "em_atecco_codigo_edimca");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.allowDeferredSell = UtilSql.getValue(result, "allow_deferred_sell");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emAteccoCodigoCotopaxi = UtilSql.getValue(result, "em_atecco_codigo_cotopaxi");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.deferredSellMaxDays = UtilSql.getValue(result, "deferred_sell_max_days");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.emDecVisible = UtilSql.getValue(result, "em_dec_visible");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.mProductId = UtilSql.getValue(result, "m_product_id");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.trBgcolor = UtilSql.getValue(result, "tr_bgcolor");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.totalCount = UtilSql.getValue(result, "total_count");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.language = UtilSql.getValue(result, "language");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adUserClient = UtilSql.getValue(result, "ad_user_client");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.adOrgClient = UtilSql.getValue(result, "ad_org_client");
        objectProducto30BB7C3E0819470AB0FB51361655CFFAData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProducto30BB7C3E0819470AB0FB51361655CFFAData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Producto30BB7C3E0819470AB0FB51361655CFFAData objectProducto30BB7C3E0819470AB0FB51361655CFFAData[] = new Producto30BB7C3E0819470AB0FB51361655CFFAData[vector.size()];
    vector.copyInto(objectProducto30BB7C3E0819470AB0FB51361655CFFAData);
    return(objectProducto30BB7C3E0819470AB0FB51361655CFFAData);
  }

/**
Create a registry
 */
  public static Producto30BB7C3E0819470AB0FB51361655CFFAData[] set(String mProductId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String name, String description, String issummary, String cUomId, String isstocked, String ispurchased, String issold, String volume, String weight, String revplantype, String quantityRule, String emAteccoModelo, String value, String mProductCategoryId, String cTaxcategoryId, String upc, String sku, String shelfwidth, String shelfheight, String shelfdepth, String unitsperpallet, String discontinued, String discontinuedby, String defaultperiod, String allowDeferredSell, String relateprodtoservice, String documentnote, String help, String classification, String salesrepId, String bookusingpoprice, String emAteccoGenericoId, String isbom, String isinvoiceprintdetails, String ispicklistprintdetails, String isverified, String processing, String islinkedtoproduct, String emAteccoTextura, String capacity, String delaymin, String mrpPlannerId, String mrpPlanningmethodId, String qtymax, String qtymin, String qtystd, String qtytype, String stockmin, String productSelection, String uniquePerDocument, String overdueReturnDays, String emAteccoSubCategoriaId, String relateprodcattoservice, String sExpensetypeId, String sResourceId, String emAteccoColor1, String emAteccoCaras, String emAteccoD1, String emAteccoCodigoEdimca, String ispricerulebased, String producttype, String imageurl, String descriptionurl, String versionno, String guaranteedays, String deferredSellMaxDays, String attrsetvaluetype, String adImageId, String cBpartnerId, String ispriceprinted, String name2, String costtype, String coststd, String stockMin, String enforceAttribute, String calculated, String maProcessplanId, String production, String isdeferredrevenue, String emAteccoD2, String mAttributesetId, String mAttributesetinstanceId, String emAteccoCodigoCotopaxi, String downloadurl, String mFreightcategoryId, String mLocatorId, String emDecVisible, String emAteccoTipo, String isdeferredexpense, String emAteccoCategoriaId, String printDescription, String prodCatSelection, String defaultperiodExp, String expplantype, String periodnumberExp, String emAteccoM3, String updateinvariants, String returnable, String emAteccoFamiliaId, String emAteccoColor2, String mBrandId, String isgeneric, String genericProductId, String createvariants, String characteristicDesc, String managevariants, String cUomWeightId, String isquantityvariable, String emAteccoEspesor, String periodnumber)    throws ServletException {
    Producto30BB7C3E0819470AB0FB51361655CFFAData objectProducto30BB7C3E0819470AB0FB51361655CFFAData[] = new Producto30BB7C3E0819470AB0FB51361655CFFAData[1];
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0] = new Producto30BB7C3E0819470AB0FB51361655CFFAData();
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].created = "";
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].createdbyr = createdbyr;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].updated = "";
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].updatedTimeStamp = "";
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].updatedby = updatedby;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].updatedbyr = updatedbyr;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].value = value;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].adOrgId = adOrgId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoFamiliaId = emAteccoFamiliaId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoGenericoId = emAteccoGenericoId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].name = name;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].description = description;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mProductCategoryId = mProductCategoryId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mProductCategoryIdr = "";
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].isactive = isactive;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].documentnote = documentnote;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].help = help;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].upc = upc;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].sku = sku;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].cUomId = cUomId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].salesrepId = salesrepId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].issummary = issummary;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].isstocked = isstocked;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].ispurchased = ispurchased;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].issold = issold;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].isbom = isbom;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].isinvoiceprintdetails = isinvoiceprintdetails;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].ispicklistprintdetails = ispicklistprintdetails;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].isverified = isverified;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].classification = classification;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].volume = volume;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].weight = weight;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].shelfwidth = shelfwidth;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].shelfheight = shelfheight;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].shelfdepth = shelfdepth;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].unitsperpallet = unitsperpallet;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].cTaxcategoryId = cTaxcategoryId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].sResourceId = sResourceId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].discontinued = discontinued;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].discontinuedby = discontinuedby;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].processing = processing;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].sExpensetypeId = sExpensetypeId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].producttype = producttype;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].imageurl = imageurl;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].descriptionurl = descriptionurl;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].guaranteedays = guaranteedays;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].versionno = versionno;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mAttributesetId = mAttributesetId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mAttributesetinstanceId = mAttributesetinstanceId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].downloadurl = downloadurl;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mFreightcategoryId = mFreightcategoryId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mLocatorId = mLocatorId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].adImageId = adImageId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].cBpartnerId = cBpartnerId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].ispriceprinted = ispriceprinted;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].name2 = name2;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].costtype = costtype;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].coststd = coststd;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].stockMin = stockMin;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].enforceAttribute = enforceAttribute;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].calculated = calculated;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].maProcessplanId = maProcessplanId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].production = production;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].capacity = capacity;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].delaymin = delaymin;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mrpPlannerId = mrpPlannerId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mrpPlanningmethodId = mrpPlanningmethodId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].qtymax = qtymax;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].qtymin = qtymin;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].qtystd = qtystd;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].qtytype = qtytype;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].stockmin = stockmin;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].attrsetvaluetype = attrsetvaluetype;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].isquantityvariable = isquantityvariable;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].isdeferredrevenue = isdeferredrevenue;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].revplantype = revplantype;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].periodnumber = periodnumber;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].isdeferredexpense = isdeferredexpense;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].expplantype = expplantype;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].periodnumberExp = periodnumberExp;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].defaultperiod = defaultperiod;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].defaultperiodExp = defaultperiodExp;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].bookusingpoprice = bookusingpoprice;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].cUomWeightId = cUomWeightId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mBrandId = mBrandId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].isgeneric = isgeneric;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].genericProductId = genericProductId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].createvariants = createvariants;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].characteristicDesc = characteristicDesc;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].updateinvariants = updateinvariants;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].managevariants = managevariants;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].prodCatSelection = prodCatSelection;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].productSelection = productSelection;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].printDescription = printDescription;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].returnable = returnable;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].overdueReturnDays = overdueReturnDays;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].ispricerulebased = ispricerulebased;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].uniquePerDocument = uniquePerDocument;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoModelo = emAteccoModelo;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoTipo = emAteccoTipo;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoTextura = emAteccoTextura;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoCaras = emAteccoCaras;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].islinkedtoproduct = islinkedtoproduct;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoColor1 = emAteccoColor1;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoColor2 = emAteccoColor2;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoD1 = emAteccoD1;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].relateprodcattoservice = relateprodcattoservice;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoD2 = emAteccoD2;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].relateprodtoservice = relateprodtoservice;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoEspesor = emAteccoEspesor;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].quantityRule = quantityRule;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoM3 = emAteccoM3;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoCategoriaId = emAteccoCategoriaId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoSubCategoriaId = emAteccoSubCategoriaId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoCodigoEdimca = emAteccoCodigoEdimca;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].allowDeferredSell = allowDeferredSell;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emAteccoCodigoCotopaxi = emAteccoCodigoCotopaxi;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].deferredSellMaxDays = deferredSellMaxDays;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].emDecVisible = emDecVisible;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].adClientId = adClientId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].mProductId = mProductId;
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].trBgcolor = "";
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].totalCount = "";
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].language = "";
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].adUserClient = "";
    objectProducto30BB7C3E0819470AB0FB51361655CFFAData[0].adOrgClient = "";
    return objectProducto30BB7C3E0819470AB0FB51361655CFFAData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef1407_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef1409_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2012(ConnectionProvider connectionProvider, String AD_ORG_ID, String AD_CLIENT_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT MAX(M_PRODUCT_CATEGORY_ID) FROM M_PRODUCT_CATEGORY WHERE AD_ISORGINCLUDED(?, AD_ORG_ID, ?) <> -1 AND ISDEFAULT = 'Y' AND AD_CLIENT_ID = ? AND ISSUMMARY='N' ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "max");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE M_Product" +
      "        SET Value = (?) , AD_Org_ID = (?) , EM_Atecco_Familia_ID = (?) , EM_Atecco_Generico_ID = (?) , Name = (?) , Description = (?) , M_Product_Category_ID = (?) , IsActive = (?) , DocumentNote = (?) , Help = (?) , UPC = (?) , SKU = (?) , C_UOM_ID = (?) , SalesRep_ID = (?) , IsSummary = (?) , IsStocked = (?) , IsPurchased = (?) , IsSold = (?) , IsBOM = (?) , IsInvoicePrintDetails = (?) , IsPickListPrintDetails = (?) , IsVerified = (?) , Classification = (?) , Volume = TO_NUMBER(?) , Weight = TO_NUMBER(?) , ShelfWidth = TO_NUMBER(?) , ShelfHeight = TO_NUMBER(?) , ShelfDepth = TO_NUMBER(?) , UnitsPerPallet = TO_NUMBER(?) , C_TaxCategory_ID = (?) , S_Resource_ID = (?) , Discontinued = (?) , DiscontinuedBy = TO_DATE(?) , Processing = (?) , S_ExpenseType_ID = (?) , ProductType = (?) , ImageURL = (?) , DescriptionURL = (?) , GuaranteeDays = TO_NUMBER(?) , VersionNo = (?) , M_AttributeSet_ID = (?) , M_AttributeSetInstance_ID = (?) , DownloadURL = (?) , M_FreightCategory_ID = (?) , M_Locator_ID = (?) , AD_Image_ID = (?) , C_BPartner_ID = (?) , Ispriceprinted = (?) , Name2 = (?) , Costtype = (?) , Coststd = TO_NUMBER(?) , Stock_Min = TO_NUMBER(?) , Enforce_Attribute = (?) , Calculated = (?) , MA_Processplan_ID = (?) , Production = (?) , Capacity = TO_NUMBER(?) , Delaymin = TO_NUMBER(?) , MRP_Planner_ID = (?) , MRP_Planningmethod_ID = (?) , Qtymax = TO_NUMBER(?) , Qtymin = TO_NUMBER(?) , Qtystd = TO_NUMBER(?) , Qtytype = (?) , Stockmin = TO_NUMBER(?) , Attrsetvaluetype = (?) , Isquantityvariable = (?) , Isdeferredrevenue = (?) , Revplantype = (?) , Periodnumber = TO_NUMBER(?) , Isdeferredexpense = (?) , Expplantype = (?) , Periodnumber_Exp = TO_NUMBER(?) , DefaultPeriod = (?) , DefaultPeriod_Exp = (?) , Bookusingpoprice = (?) , C_Uom_Weight_ID = (?) , M_Brand_ID = (?) , IsGeneric = (?) , Generic_Product_ID = (?) , CreateVariants = (?) , Characteristic_Desc = (?) , Updateinvariants = (?) , ManageVariants = (?) , Prod_Cat_Selection = (?) , Product_Selection = (?) , Print_Description = (?) , Returnable = (?) , Overdue_Return_Days = TO_NUMBER(?) , Ispricerulebased = (?) , Unique_Per_Document = (?) , EM_Atecco_Modelo = (?) , EM_Atecco_Tipo = (?) , EM_Atecco_Textura = (?) , EM_Atecco_Caras = (?) , Islinkedtoproduct = (?) , EM_Atecco_Color1 = (?) , EM_Atecco_Color2 = (?) , EM_Atecco_D1 = TO_NUMBER(?) , Relateprodcattoservice = (?) , EM_Atecco_D2 = TO_NUMBER(?) , Relateprodtoservice = (?) , EM_Atecco_Espesor = TO_NUMBER(?) , Quantity_Rule = (?) , EM_Atecco_M3 = TO_NUMBER(?) , EM_Atecco_Categoria_ID = (?) , EM_Atecco_Sub_Categoria_ID = (?) , EM_Atecco_Codigo_Edimca = (?) , Allow_Deferred_Sell = (?) , EM_Atecco_Codigo_Cotopaxi = (?) , Deferred_Sell_Max_Days = TO_NUMBER(?) , em_dec_visible = (?) , AD_Client_ID = (?) , M_Product_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE M_Product.M_Product_ID = ? " +
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoFamiliaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoGenericoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentnote);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, help);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, upc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sku);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issummary);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isstocked);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispurchased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issold);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isbom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiceprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispicklistprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isverified);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, classification);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, volume);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, weight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfwidth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfheight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfdepth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, unitsperpallet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinued);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinuedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sExpensetypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, producttype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, imageurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descriptionurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, guaranteedays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, versionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, downloadurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mFreightcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adImageId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispriceprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coststd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockMin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, enforceAttribute);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculated);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, maProcessplanId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, production);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, capacity);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, delaymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlannerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlanningmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymax);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtystd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtytype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, attrsetvaluetype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isquantityvariable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredrevenue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, revplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumber);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredexpense);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, expplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumberExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiodExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bookusingpoprice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomWeightId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mBrandId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isgeneric);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, genericProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, characteristicDesc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updateinvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managevariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, prodCatSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, productSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, returnable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispricerulebased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, uniquePerDocument);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoModelo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTextura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCaras);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, islinkedtoproduct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoColor1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoColor2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoD1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodcattoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoD2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodtoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoEspesor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityRule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoSubCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCodigoEdimca);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, allowDeferredSell);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCodigoCotopaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deferredSellMaxDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecVisible);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO M_Product " +
      "        (Value, AD_Org_ID, EM_Atecco_Familia_ID, EM_Atecco_Generico_ID, Name, Description, M_Product_Category_ID, IsActive, DocumentNote, Help, UPC, SKU, C_UOM_ID, SalesRep_ID, IsSummary, IsStocked, IsPurchased, IsSold, IsBOM, IsInvoicePrintDetails, IsPickListPrintDetails, IsVerified, Classification, Volume, Weight, ShelfWidth, ShelfHeight, ShelfDepth, UnitsPerPallet, C_TaxCategory_ID, S_Resource_ID, Discontinued, DiscontinuedBy, Processing, S_ExpenseType_ID, ProductType, ImageURL, DescriptionURL, GuaranteeDays, VersionNo, M_AttributeSet_ID, M_AttributeSetInstance_ID, DownloadURL, M_FreightCategory_ID, M_Locator_ID, AD_Image_ID, C_BPartner_ID, Ispriceprinted, Name2, Costtype, Coststd, Stock_Min, Enforce_Attribute, Calculated, MA_Processplan_ID, Production, Capacity, Delaymin, MRP_Planner_ID, MRP_Planningmethod_ID, Qtymax, Qtymin, Qtystd, Qtytype, Stockmin, Attrsetvaluetype, Isquantityvariable, Isdeferredrevenue, Revplantype, Periodnumber, Isdeferredexpense, Expplantype, Periodnumber_Exp, DefaultPeriod, DefaultPeriod_Exp, Bookusingpoprice, C_Uom_Weight_ID, M_Brand_ID, IsGeneric, Generic_Product_ID, CreateVariants, Characteristic_Desc, Updateinvariants, ManageVariants, Prod_Cat_Selection, Product_Selection, Print_Description, Returnable, Overdue_Return_Days, Ispricerulebased, Unique_Per_Document, EM_Atecco_Modelo, EM_Atecco_Tipo, EM_Atecco_Textura, EM_Atecco_Caras, Islinkedtoproduct, EM_Atecco_Color1, EM_Atecco_Color2, EM_Atecco_D1, Relateprodcattoservice, EM_Atecco_D2, Relateprodtoservice, EM_Atecco_Espesor, Quantity_Rule, EM_Atecco_M3, EM_Atecco_Categoria_ID, EM_Atecco_Sub_Categoria_ID, EM_Atecco_Codigo_Edimca, Allow_Deferred_Sell, EM_Atecco_Codigo_Cotopaxi, Deferred_Sell_Max_Days, em_dec_visible, AD_Client_ID, M_Product_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoFamiliaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoGenericoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentnote);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, help);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, upc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sku);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issummary);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isstocked);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispurchased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issold);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isbom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiceprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispicklistprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isverified);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, classification);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, volume);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, weight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfwidth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfheight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfdepth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, unitsperpallet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinued);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinuedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sExpensetypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, producttype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, imageurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descriptionurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, guaranteedays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, versionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, downloadurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mFreightcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adImageId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispriceprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coststd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockMin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, enforceAttribute);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculated);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, maProcessplanId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, production);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, capacity);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, delaymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlannerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlanningmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymax);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtystd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtytype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, attrsetvaluetype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isquantityvariable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredrevenue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, revplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumber);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredexpense);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, expplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumberExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiodExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bookusingpoprice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomWeightId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mBrandId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isgeneric);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, genericProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, characteristicDesc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updateinvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managevariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, prodCatSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, productSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, returnable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispricerulebased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, uniquePerDocument);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoModelo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTextura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCaras);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, islinkedtoproduct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoColor1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoColor2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoD1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodcattoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoD2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodtoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoEspesor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityRule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoSubCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCodigoEdimca);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, allowDeferredSell);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCodigoCotopaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deferredSellMaxDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDecVisible);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM M_Product" +
      "        WHERE M_Product.M_Product_ID = ? " +
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM M_Product" +
      "         WHERE M_Product.M_Product_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM M_Product" +
      "         WHERE M_Product.M_Product_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}

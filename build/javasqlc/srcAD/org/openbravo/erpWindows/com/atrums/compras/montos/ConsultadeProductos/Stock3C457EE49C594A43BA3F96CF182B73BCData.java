//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.ConsultadeProductos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Stock3C457EE49C594A43BA3F96CF182B73BCData implements FieldProvider {
static Logger log4j = Logger.getLogger(Stock3C457EE49C594A43BA3F96CF182B73BCData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String mProductId;
  public String mAttributesetinstanceId;
  public String mLocatorId;
  public String mLocatorIdr;
  public String mProductUomId;
  public String cUomId;
  public String cUomIdr;
  public String qtyonhand;
  public String qtyorderonhand;
  public String datelastinventory;
  public String preqtyonhand;
  public String preqtyorderonhand;
  public String isactive;
  public String reservedqty;
  public String allocatedqty;
  public String mStorageDetailId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("m_attributesetinstance_id") || fieldName.equals("mAttributesetinstanceId"))
      return mAttributesetinstanceId;
    else if (fieldName.equalsIgnoreCase("m_locator_id") || fieldName.equals("mLocatorId"))
      return mLocatorId;
    else if (fieldName.equalsIgnoreCase("m_locator_idr") || fieldName.equals("mLocatorIdr"))
      return mLocatorIdr;
    else if (fieldName.equalsIgnoreCase("m_product_uom_id") || fieldName.equals("mProductUomId"))
      return mProductUomId;
    else if (fieldName.equalsIgnoreCase("c_uom_id") || fieldName.equals("cUomId"))
      return cUomId;
    else if (fieldName.equalsIgnoreCase("c_uom_idr") || fieldName.equals("cUomIdr"))
      return cUomIdr;
    else if (fieldName.equalsIgnoreCase("qtyonhand"))
      return qtyonhand;
    else if (fieldName.equalsIgnoreCase("qtyorderonhand"))
      return qtyorderonhand;
    else if (fieldName.equalsIgnoreCase("datelastinventory"))
      return datelastinventory;
    else if (fieldName.equalsIgnoreCase("preqtyonhand"))
      return preqtyonhand;
    else if (fieldName.equalsIgnoreCase("preqtyorderonhand"))
      return preqtyorderonhand;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("reservedqty"))
      return reservedqty;
    else if (fieldName.equalsIgnoreCase("allocatedqty"))
      return allocatedqty;
    else if (fieldName.equalsIgnoreCase("m_storage_detail_id") || fieldName.equals("mStorageDetailId"))
      return mStorageDetailId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Stock3C457EE49C594A43BA3F96CF182B73BCData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String mProductId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, mProductId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Stock3C457EE49C594A43BA3F96CF182B73BCData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String mProductId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(M_Storage_Detail.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Storage_Detail.CreatedBy) as CreatedByR, " +
      "        to_char(M_Storage_Detail.Updated, ?) as updated, " +
      "        to_char(M_Storage_Detail.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        M_Storage_Detail.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Storage_Detail.UpdatedBy) as UpdatedByR," +
      "        M_Storage_Detail.AD_Org_ID, " +
      "M_Storage_Detail.M_Product_ID, " +
      "M_Storage_Detail.M_AttributeSetInstance_ID, " +
      "M_Storage_Detail.M_Locator_ID, " +
      "(CASE WHEN M_Storage_Detail.M_Locator_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Value), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.X), ''))),'') ) END) AS M_Locator_IDR, " +
      "M_Storage_Detail.M_Product_Uom_Id, " +
      "M_Storage_Detail.C_UOM_ID, " +
      "(CASE WHEN M_Storage_Detail.C_UOM_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_UOM_IDR, " +
      "M_Storage_Detail.QtyOnHand, " +
      "M_Storage_Detail.QtyOrderOnHand, " +
      "M_Storage_Detail.DateLastInventory, " +
      "M_Storage_Detail.PreQtyOnHand, " +
      "M_Storage_Detail.PreQtyOrderOnHand, " +
      "COALESCE(M_Storage_Detail.IsActive, 'N') AS IsActive, " +
      "M_Storage_Detail.ReservedQty, " +
      "M_Storage_Detail.AllocatedQty, " +
      "M_Storage_Detail.M_Storage_Detail_ID, " +
      "M_Storage_Detail.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM M_Storage_Detail left join (select M_Locator_ID, Value, X from M_Locator) table1 on (M_Storage_Detail.M_Locator_ID = table1.M_Locator_ID) left join (select C_UOM_ID, Name from C_UOM) table2 on (M_Storage_Detail.C_UOM_ID = table2.C_UOM_ID) left join (select C_UOM_ID,AD_Language, Name from C_UOM_TRL) tableTRL2 on (table2.C_UOM_ID = tableTRL2.C_UOM_ID and tableTRL2.AD_Language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((mProductId==null || mProductId.equals(""))?"":"  AND M_Storage_Detail.M_Product_ID = ?  ");
    strSql = strSql + 
      "        AND M_Storage_Detail.M_Storage_Detail_ID = ? " +
      "        AND M_Storage_Detail.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND M_Storage_Detail.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (mProductId != null && !(mProductId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Stock3C457EE49C594A43BA3F96CF182B73BCData objectStock3C457EE49C594A43BA3F96CF182B73BCData = new Stock3C457EE49C594A43BA3F96CF182B73BCData();
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.created = UtilSql.getValue(result, "created");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.updated = UtilSql.getValue(result, "updated");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.updatedby = UtilSql.getValue(result, "updatedby");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.mProductId = UtilSql.getValue(result, "m_product_id");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.mAttributesetinstanceId = UtilSql.getValue(result, "m_attributesetinstance_id");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.mLocatorId = UtilSql.getValue(result, "m_locator_id");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.mLocatorIdr = UtilSql.getValue(result, "m_locator_idr");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.mProductUomId = UtilSql.getValue(result, "m_product_uom_id");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.cUomIdr = UtilSql.getValue(result, "c_uom_idr");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.qtyonhand = UtilSql.getValue(result, "qtyonhand");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.qtyorderonhand = UtilSql.getValue(result, "qtyorderonhand");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.datelastinventory = UtilSql.getDateValue(result, "datelastinventory", "dd-MM-yyyy");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.preqtyonhand = UtilSql.getValue(result, "preqtyonhand");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.preqtyorderonhand = UtilSql.getValue(result, "preqtyorderonhand");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.isactive = UtilSql.getValue(result, "isactive");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.reservedqty = UtilSql.getValue(result, "reservedqty");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.allocatedqty = UtilSql.getValue(result, "allocatedqty");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.mStorageDetailId = UtilSql.getValue(result, "m_storage_detail_id");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.language = UtilSql.getValue(result, "language");
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.adUserClient = "";
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.adOrgClient = "";
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.createdby = "";
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.trBgcolor = "";
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.totalCount = "";
        objectStock3C457EE49C594A43BA3F96CF182B73BCData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectStock3C457EE49C594A43BA3F96CF182B73BCData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Stock3C457EE49C594A43BA3F96CF182B73BCData objectStock3C457EE49C594A43BA3F96CF182B73BCData[] = new Stock3C457EE49C594A43BA3F96CF182B73BCData[vector.size()];
    vector.copyInto(objectStock3C457EE49C594A43BA3F96CF182B73BCData);
    return(objectStock3C457EE49C594A43BA3F96CF182B73BCData);
  }

/**
Create a registry
 */
  public static Stock3C457EE49C594A43BA3F96CF182B73BCData[] set(String mProductId, String mLocatorId, String mLocatorIdr, String mAttributesetinstanceId, String cUomId, String mProductUomId, String qtyonhand, String qtyorderonhand, String datelastinventory, String preqtyonhand, String preqtyorderonhand, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String mStorageDetailId, String allocatedqty, String reservedqty)    throws ServletException {
    Stock3C457EE49C594A43BA3F96CF182B73BCData objectStock3C457EE49C594A43BA3F96CF182B73BCData[] = new Stock3C457EE49C594A43BA3F96CF182B73BCData[1];
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0] = new Stock3C457EE49C594A43BA3F96CF182B73BCData();
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].created = "";
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].createdbyr = createdbyr;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].updated = "";
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].updatedTimeStamp = "";
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].updatedby = updatedby;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].updatedbyr = updatedbyr;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].adOrgId = adOrgId;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].mProductId = mProductId;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].mAttributesetinstanceId = mAttributesetinstanceId;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].mLocatorId = mLocatorId;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].mLocatorIdr = mLocatorIdr;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].mProductUomId = mProductUomId;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].cUomId = cUomId;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].cUomIdr = "";
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].qtyonhand = qtyonhand;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].qtyorderonhand = qtyorderonhand;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].datelastinventory = datelastinventory;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].preqtyonhand = preqtyonhand;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].preqtyorderonhand = preqtyorderonhand;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].isactive = isactive;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].reservedqty = reservedqty;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].allocatedqty = allocatedqty;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].mStorageDetailId = mStorageDetailId;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].adClientId = adClientId;
    objectStock3C457EE49C594A43BA3F96CF182B73BCData[0].language = "";
    return objectStock3C457EE49C594A43BA3F96CF182B73BCData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef800634_0(ConnectionProvider connectionProvider, String M_Locator_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Value), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.X), ''))), '') ) as M_Locator_ID FROM M_Locator left join (select M_Locator_ID, Value, X from M_Locator) table2 on (M_Locator.M_Locator_ID = table2.M_Locator_ID) WHERE M_Locator.isActive='Y' AND M_Locator.M_Locator_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_Locator_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_locator_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef800647_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef800649_2(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT M_Storage_Detail.M_Product_ID AS NAME" +
      "        FROM M_Storage_Detail" +
      "        WHERE M_Storage_Detail.M_Storage_Detail_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String paramLanguage, String mProductId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))) AS NAME FROM M_Product left join (select M_Product_ID, Name from M_Product) table1 on (M_Product.M_Product_ID = table1.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL1 on (table1.M_Product_ID = tableTRL1.M_Product_ID and tableTRL1.AD_Language = ?)  WHERE M_Product.M_Product_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String paramLanguage, String mProductId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))) AS NAME FROM M_Product left join (select M_Product_ID, Name from M_Product) table1 on (M_Product.M_Product_ID = table1.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL1 on (table1.M_Product_ID = tableTRL1.M_Product_ID and tableTRL1.AD_Language = ?)  WHERE M_Product.M_Product_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE M_Storage_Detail" +
      "        SET AD_Org_ID = (?) , M_Product_ID = (?) , M_AttributeSetInstance_ID = (?) , M_Locator_ID = (?) , M_Product_Uom_Id = (?) , C_UOM_ID = (?) , QtyOnHand = TO_NUMBER(?) , QtyOrderOnHand = TO_NUMBER(?) , DateLastInventory = TO_DATE(?) , PreQtyOnHand = TO_NUMBER(?) , PreQtyOrderOnHand = TO_NUMBER(?) , IsActive = (?) , ReservedQty = TO_NUMBER(?) , AllocatedQty = TO_NUMBER(?) , M_Storage_Detail_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE M_Storage_Detail.M_Storage_Detail_ID = ? " +
      "                 AND M_Storage_Detail.M_Product_ID = ? " +
      "        AND M_Storage_Detail.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Storage_Detail.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyonhand);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyorderonhand);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datelastinventory);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, preqtyonhand);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, preqtyorderonhand);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reservedqty);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, allocatedqty);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mStorageDetailId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mStorageDetailId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO M_Storage_Detail " +
      "        (AD_Org_ID, M_Product_ID, M_AttributeSetInstance_ID, M_Locator_ID, M_Product_Uom_Id, C_UOM_ID, QtyOnHand, QtyOrderOnHand, DateLastInventory, PreQtyOnHand, PreQtyOrderOnHand, IsActive, ReservedQty, AllocatedQty, M_Storage_Detail_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_DATE(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyonhand);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyorderonhand);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datelastinventory);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, preqtyonhand);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, preqtyorderonhand);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reservedqty);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, allocatedqty);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mStorageDetailId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String mProductId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM M_Storage_Detail" +
      "        WHERE M_Storage_Detail.M_Storage_Detail_ID = ? " +
      "                 AND M_Storage_Detail.M_Product_ID = ? " +
      "        AND M_Storage_Detail.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Storage_Detail.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM M_Storage_Detail" +
      "         WHERE M_Storage_Detail.M_Storage_Detail_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM M_Storage_Detail" +
      "         WHERE M_Storage_Detail.M_Storage_Detail_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}

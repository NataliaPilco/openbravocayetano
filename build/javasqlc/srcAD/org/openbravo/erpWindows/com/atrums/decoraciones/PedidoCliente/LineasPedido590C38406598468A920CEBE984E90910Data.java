//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.decoraciones.PedidoCliente;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class LineasPedido590C38406598468A920CEBE984E90910Data implements FieldProvider {
static Logger log4j = Logger.getLogger(LineasPedido590C38406598468A920CEBE984E90910Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String isactive;
  public String cOrderId;
  public String line;
  public String cBpartnerId;
  public String mProductId;
  public String mProductIdr;
  public String cBpartnerLocationId;
  public String dateordered;
  public String datepromised;
  public String datedelivered;
  public String dateinvoiced;
  public String description;
  public String mWarehouseId;
  public String directship;
  public String cUomId;
  public String cUomIdr;
  public String qtyordered;
  public String qtyreserved;
  public String qtydelivered;
  public String qtyinvoiced;
  public String mShipperId;
  public String cCurrencyId;
  public String pricelist;
  public String priceactual;
  public String pricelimit;
  public String linenetamt;
  public String discount;
  public String freightamt;
  public String cChargeId;
  public String chargeamt;
  public String cTaxId;
  public String cTaxIdr;
  public String sResourceassignmentId;
  public String refOrderlineId;
  public String mAttributesetinstanceId;
  public String isdescription;
  public String quantityorder;
  public String mProductUomId;
  public String mOfferId;
  public String pricestd;
  public String cancelpricead;
  public String cOrderDiscountId;
  public String cOrderDiscountIdr;
  public String iseditlinenetamt;
  public String taxbaseamt;
  public String mInoutlineId;
  public String cReturnReasonId;
  public String cReturnReasonIdr;
  public String grossUnitPrice;
  public String lineGrossAmount;
  public String grosspricelist;
  public String cCostcenterId;
  public String grosspricestd;
  public String aAssetId;
  public String mWarehouseRuleId;
  public String mWarehouseRuleIdr;
  public String user1Id;
  public String quotationlineId;
  public String quotationlineIdr;
  public String user2Id;
  public String createReservation;
  public String createReservationr;
  public String cProjectId;
  public String cProjectIdr;
  public String soResStatus;
  public String soResStatusr;
  public String manageReservation;
  public String managePrereservation;
  public String explode;
  public String bomParentId;
  public String bomParentIdr;
  public String printDescription;
  public String overdueReturnDays;
  public String relateOrderline;
  public String emAteccoStocktienda;
  public String emAteccoStocktablero;
  public String emAteccoPrecioefectivo;
  public String emAteccoTotalefectivo;
  public String emAteccoPreciotarjeta;
  public String emAteccoTotaltarjeta;
  public String emAteccoCInvoiceId;
  public String emAteccoCInvoiceIdr;
  public String cOrderlineId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("c_order_id") || fieldName.equals("cOrderId"))
      return cOrderId;
    else if (fieldName.equalsIgnoreCase("line"))
      return line;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("m_product_idr") || fieldName.equals("mProductIdr"))
      return mProductIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("dateordered"))
      return dateordered;
    else if (fieldName.equalsIgnoreCase("datepromised"))
      return datepromised;
    else if (fieldName.equalsIgnoreCase("datedelivered"))
      return datedelivered;
    else if (fieldName.equalsIgnoreCase("dateinvoiced"))
      return dateinvoiced;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("m_warehouse_id") || fieldName.equals("mWarehouseId"))
      return mWarehouseId;
    else if (fieldName.equalsIgnoreCase("directship"))
      return directship;
    else if (fieldName.equalsIgnoreCase("c_uom_id") || fieldName.equals("cUomId"))
      return cUomId;
    else if (fieldName.equalsIgnoreCase("c_uom_idr") || fieldName.equals("cUomIdr"))
      return cUomIdr;
    else if (fieldName.equalsIgnoreCase("qtyordered"))
      return qtyordered;
    else if (fieldName.equalsIgnoreCase("qtyreserved"))
      return qtyreserved;
    else if (fieldName.equalsIgnoreCase("qtydelivered"))
      return qtydelivered;
    else if (fieldName.equalsIgnoreCase("qtyinvoiced"))
      return qtyinvoiced;
    else if (fieldName.equalsIgnoreCase("m_shipper_id") || fieldName.equals("mShipperId"))
      return mShipperId;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("pricelist"))
      return pricelist;
    else if (fieldName.equalsIgnoreCase("priceactual"))
      return priceactual;
    else if (fieldName.equalsIgnoreCase("pricelimit"))
      return pricelimit;
    else if (fieldName.equalsIgnoreCase("linenetamt"))
      return linenetamt;
    else if (fieldName.equalsIgnoreCase("discount"))
      return discount;
    else if (fieldName.equalsIgnoreCase("freightamt"))
      return freightamt;
    else if (fieldName.equalsIgnoreCase("c_charge_id") || fieldName.equals("cChargeId"))
      return cChargeId;
    else if (fieldName.equalsIgnoreCase("chargeamt"))
      return chargeamt;
    else if (fieldName.equalsIgnoreCase("c_tax_id") || fieldName.equals("cTaxId"))
      return cTaxId;
    else if (fieldName.equalsIgnoreCase("c_tax_idr") || fieldName.equals("cTaxIdr"))
      return cTaxIdr;
    else if (fieldName.equalsIgnoreCase("s_resourceassignment_id") || fieldName.equals("sResourceassignmentId"))
      return sResourceassignmentId;
    else if (fieldName.equalsIgnoreCase("ref_orderline_id") || fieldName.equals("refOrderlineId"))
      return refOrderlineId;
    else if (fieldName.equalsIgnoreCase("m_attributesetinstance_id") || fieldName.equals("mAttributesetinstanceId"))
      return mAttributesetinstanceId;
    else if (fieldName.equalsIgnoreCase("isdescription"))
      return isdescription;
    else if (fieldName.equalsIgnoreCase("quantityorder"))
      return quantityorder;
    else if (fieldName.equalsIgnoreCase("m_product_uom_id") || fieldName.equals("mProductUomId"))
      return mProductUomId;
    else if (fieldName.equalsIgnoreCase("m_offer_id") || fieldName.equals("mOfferId"))
      return mOfferId;
    else if (fieldName.equalsIgnoreCase("pricestd"))
      return pricestd;
    else if (fieldName.equalsIgnoreCase("cancelpricead"))
      return cancelpricead;
    else if (fieldName.equalsIgnoreCase("c_order_discount_id") || fieldName.equals("cOrderDiscountId"))
      return cOrderDiscountId;
    else if (fieldName.equalsIgnoreCase("c_order_discount_idr") || fieldName.equals("cOrderDiscountIdr"))
      return cOrderDiscountIdr;
    else if (fieldName.equalsIgnoreCase("iseditlinenetamt"))
      return iseditlinenetamt;
    else if (fieldName.equalsIgnoreCase("taxbaseamt"))
      return taxbaseamt;
    else if (fieldName.equalsIgnoreCase("m_inoutline_id") || fieldName.equals("mInoutlineId"))
      return mInoutlineId;
    else if (fieldName.equalsIgnoreCase("c_return_reason_id") || fieldName.equals("cReturnReasonId"))
      return cReturnReasonId;
    else if (fieldName.equalsIgnoreCase("c_return_reason_idr") || fieldName.equals("cReturnReasonIdr"))
      return cReturnReasonIdr;
    else if (fieldName.equalsIgnoreCase("gross_unit_price") || fieldName.equals("grossUnitPrice"))
      return grossUnitPrice;
    else if (fieldName.equalsIgnoreCase("line_gross_amount") || fieldName.equals("lineGrossAmount"))
      return lineGrossAmount;
    else if (fieldName.equalsIgnoreCase("grosspricelist"))
      return grosspricelist;
    else if (fieldName.equalsIgnoreCase("c_costcenter_id") || fieldName.equals("cCostcenterId"))
      return cCostcenterId;
    else if (fieldName.equalsIgnoreCase("grosspricestd"))
      return grosspricestd;
    else if (fieldName.equalsIgnoreCase("a_asset_id") || fieldName.equals("aAssetId"))
      return aAssetId;
    else if (fieldName.equalsIgnoreCase("m_warehouse_rule_id") || fieldName.equals("mWarehouseRuleId"))
      return mWarehouseRuleId;
    else if (fieldName.equalsIgnoreCase("m_warehouse_rule_idr") || fieldName.equals("mWarehouseRuleIdr"))
      return mWarehouseRuleIdr;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("quotationline_id") || fieldName.equals("quotationlineId"))
      return quotationlineId;
    else if (fieldName.equalsIgnoreCase("quotationline_idr") || fieldName.equals("quotationlineIdr"))
      return quotationlineIdr;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("create_reservation") || fieldName.equals("createReservation"))
      return createReservation;
    else if (fieldName.equalsIgnoreCase("create_reservationr") || fieldName.equals("createReservationr"))
      return createReservationr;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("c_project_idr") || fieldName.equals("cProjectIdr"))
      return cProjectIdr;
    else if (fieldName.equalsIgnoreCase("so_res_status") || fieldName.equals("soResStatus"))
      return soResStatus;
    else if (fieldName.equalsIgnoreCase("so_res_statusr") || fieldName.equals("soResStatusr"))
      return soResStatusr;
    else if (fieldName.equalsIgnoreCase("manage_reservation") || fieldName.equals("manageReservation"))
      return manageReservation;
    else if (fieldName.equalsIgnoreCase("manage_prereservation") || fieldName.equals("managePrereservation"))
      return managePrereservation;
    else if (fieldName.equalsIgnoreCase("explode"))
      return explode;
    else if (fieldName.equalsIgnoreCase("bom_parent_id") || fieldName.equals("bomParentId"))
      return bomParentId;
    else if (fieldName.equalsIgnoreCase("bom_parent_idr") || fieldName.equals("bomParentIdr"))
      return bomParentIdr;
    else if (fieldName.equalsIgnoreCase("print_description") || fieldName.equals("printDescription"))
      return printDescription;
    else if (fieldName.equalsIgnoreCase("overdue_return_days") || fieldName.equals("overdueReturnDays"))
      return overdueReturnDays;
    else if (fieldName.equalsIgnoreCase("relate_orderline") || fieldName.equals("relateOrderline"))
      return relateOrderline;
    else if (fieldName.equalsIgnoreCase("em_atecco_stocktienda") || fieldName.equals("emAteccoStocktienda"))
      return emAteccoStocktienda;
    else if (fieldName.equalsIgnoreCase("em_atecco_stocktablero") || fieldName.equals("emAteccoStocktablero"))
      return emAteccoStocktablero;
    else if (fieldName.equalsIgnoreCase("em_atecco_precioefectivo") || fieldName.equals("emAteccoPrecioefectivo"))
      return emAteccoPrecioefectivo;
    else if (fieldName.equalsIgnoreCase("em_atecco_totalefectivo") || fieldName.equals("emAteccoTotalefectivo"))
      return emAteccoTotalefectivo;
    else if (fieldName.equalsIgnoreCase("em_atecco_preciotarjeta") || fieldName.equals("emAteccoPreciotarjeta"))
      return emAteccoPreciotarjeta;
    else if (fieldName.equalsIgnoreCase("em_atecco_totaltarjeta") || fieldName.equals("emAteccoTotaltarjeta"))
      return emAteccoTotaltarjeta;
    else if (fieldName.equalsIgnoreCase("em_atecco_c_invoice_id") || fieldName.equals("emAteccoCInvoiceId"))
      return emAteccoCInvoiceId;
    else if (fieldName.equalsIgnoreCase("em_atecco_c_invoice_idr") || fieldName.equals("emAteccoCInvoiceIdr"))
      return emAteccoCInvoiceIdr;
    else if (fieldName.equalsIgnoreCase("c_orderline_id") || fieldName.equals("cOrderlineId"))
      return cOrderlineId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static LineasPedido590C38406598468A920CEBE984E90910Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cOrderId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, cOrderId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static LineasPedido590C38406598468A920CEBE984E90910Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cOrderId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(C_OrderLine.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_OrderLine.CreatedBy) as CreatedByR, " +
      "        to_char(C_OrderLine.Updated, ?) as updated, " +
      "        to_char(C_OrderLine.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        C_OrderLine.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_OrderLine.UpdatedBy) as UpdatedByR," +
      "        C_OrderLine.AD_Org_ID, " +
      "COALESCE(C_OrderLine.IsActive, 'N') AS IsActive, " +
      "C_OrderLine.C_Order_ID, " +
      "C_OrderLine.Line, " +
      "C_OrderLine.C_BPartner_ID, " +
      "C_OrderLine.M_Product_ID, " +
      "(CASE WHEN C_OrderLine.M_Product_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS M_Product_IDR, " +
      "C_OrderLine.C_BPartner_Location_ID, " +
      "C_OrderLine.DateOrdered, " +
      "C_OrderLine.DatePromised, " +
      "C_OrderLine.DateDelivered, " +
      "C_OrderLine.DateInvoiced, " +
      "C_OrderLine.Description, " +
      "C_OrderLine.M_Warehouse_ID, " +
      "COALESCE(C_OrderLine.DirectShip, 'N') AS DirectShip, " +
      "C_OrderLine.C_UOM_ID, " +
      "(CASE WHEN C_OrderLine.C_UOM_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL3.Name IS NULL THEN TO_CHAR(table3.Name) ELSE TO_CHAR(tableTRL3.Name) END)), ''))),'') ) END) AS C_UOM_IDR, " +
      "C_OrderLine.QtyOrdered, " +
      "C_OrderLine.QtyReserved, " +
      "C_OrderLine.QtyDelivered, " +
      "C_OrderLine.QtyInvoiced, " +
      "C_OrderLine.M_Shipper_ID, " +
      "C_OrderLine.C_Currency_ID, " +
      "C_OrderLine.PriceList, " +
      "C_OrderLine.PriceActual, " +
      "C_OrderLine.PriceLimit, " +
      "C_OrderLine.LineNetAmt, " +
      "C_OrderLine.Discount, " +
      "C_OrderLine.FreightAmt, " +
      "C_OrderLine.C_Charge_ID, " +
      "C_OrderLine.ChargeAmt, " +
      "C_OrderLine.C_Tax_ID, " +
      "(CASE WHEN C_OrderLine.C_Tax_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL5.Name IS NULL THEN TO_CHAR(table5.Name) ELSE TO_CHAR(tableTRL5.Name) END)), ''))),'') ) END) AS C_Tax_IDR, " +
      "C_OrderLine.S_ResourceAssignment_ID, " +
      "C_OrderLine.Ref_OrderLine_ID, " +
      "C_OrderLine.M_AttributeSetInstance_ID, " +
      "COALESCE(C_OrderLine.IsDescription, 'N') AS IsDescription, " +
      "C_OrderLine.QuantityOrder, " +
      "C_OrderLine.M_Product_Uom_Id, " +
      "C_OrderLine.M_Offer_ID, " +
      "C_OrderLine.PriceStd, " +
      "COALESCE(C_OrderLine.CANCELPRICEAD, 'N') AS CANCELPRICEAD, " +
      "C_OrderLine.C_Order_Discount_ID, " +
      "(CASE WHEN C_OrderLine.C_Order_Discount_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.DocumentNo), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table8.DateOrdered, 'DD-MM-YYYY')),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.GrandTotal), ''))),'') ) END) AS C_Order_Discount_IDR, " +
      "COALESCE(C_OrderLine.Iseditlinenetamt, 'N') AS Iseditlinenetamt, " +
      "C_OrderLine.Taxbaseamt, " +
      "C_OrderLine.M_Inoutline_ID, " +
      "C_OrderLine.C_Return_Reason_ID, " +
      "(CASE WHEN C_OrderLine.C_Return_Reason_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table9.Name), ''))),'') ) END) AS C_Return_Reason_IDR, " +
      "C_OrderLine.Gross_Unit_Price, " +
      "C_OrderLine.Line_Gross_Amount, " +
      "C_OrderLine.GrossPriceList, " +
      "C_OrderLine.C_Costcenter_ID, " +
      "C_OrderLine.grosspricestd, " +
      "C_OrderLine.A_Asset_ID, " +
      "C_OrderLine.M_Warehouse_Rule_ID, " +
      "(CASE WHEN C_OrderLine.M_Warehouse_Rule_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table10.Name), ''))),'') ) END) AS M_Warehouse_Rule_IDR, " +
      "C_OrderLine.User1_ID, " +
      "C_OrderLine.Quotationline_ID, " +
      "(CASE WHEN C_OrderLine.Quotationline_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table12.DocumentNo), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table12.DateOrdered, 'DD-MM-YYYY')),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table12.GrandTotal), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table11.Line), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table11.LineNetAmt), ''))),'') ) END) AS Quotationline_IDR, " +
      "C_OrderLine.User2_ID, " +
      "C_OrderLine.Create_Reservation, " +
      "(CASE WHEN C_OrderLine.Create_Reservation IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Create_ReservationR, " +
      "C_OrderLine.C_Project_ID, " +
      "(CASE WHEN C_OrderLine.C_Project_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table13.Value), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table13.Name), ''))),'') ) END) AS C_Project_IDR, " +
      "C_OrderLine.SO_Res_Status, " +
      "(CASE WHEN C_OrderLine.SO_Res_Status IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS SO_Res_StatusR, " +
      "C_OrderLine.Manage_Reservation, " +
      "C_OrderLine.Manage_Prereservation, " +
      "C_OrderLine.Explode, " +
      "C_OrderLine.BOM_Parent_ID, " +
      "(CASE WHEN C_OrderLine.BOM_Parent_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table15.DocumentNo), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table15.DateOrdered, 'DD-MM-YYYY')),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table15.GrandTotal), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table14.Line), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table14.LineNetAmt), ''))),'') ) END) AS BOM_Parent_IDR, " +
      "COALESCE(C_OrderLine.Print_Description, 'N') AS Print_Description, " +
      "C_OrderLine.Overdue_Return_Days, " +
      "C_OrderLine.Relate_Orderline, " +
      "C_OrderLine.EM_Atecco_Stocktienda, " +
      "C_OrderLine.EM_Atecco_Stocktablero, " +
      "C_OrderLine.EM_Atecco_Precioefectivo, " +
      "C_OrderLine.EM_Atecco_Totalefectivo, " +
      "C_OrderLine.EM_Atecco_Preciotarjeta, " +
      "C_OrderLine.EM_Atecco_Totaltarjeta, " +
      "C_OrderLine.EM_Atecco_C_Invoice_ID, " +
      "(CASE WHEN C_OrderLine.EM_Atecco_C_Invoice_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table16.DocumentNo), ''))),'') ) END) AS EM_Atecco_C_Invoice_IDR, " +
      "C_OrderLine.C_OrderLine_ID, " +
      "C_OrderLine.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM C_OrderLine left join (select M_Product_ID, Name from M_Product) table1 on (C_OrderLine.M_Product_ID = table1.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL1 on (table1.M_Product_ID = tableTRL1.M_Product_ID and tableTRL1.AD_Language = ?)  left join (select C_UOM_ID, Name from C_UOM) table3 on (C_OrderLine.C_UOM_ID = table3.C_UOM_ID) left join (select C_UOM_ID,AD_Language, Name from C_UOM_TRL) tableTRL3 on (table3.C_UOM_ID = tableTRL3.C_UOM_ID and tableTRL3.AD_Language = ?)  left join (select C_Tax_ID, Name from C_Tax) table5 on (C_OrderLine.C_Tax_ID =  table5.C_Tax_ID) left join (select C_Tax_ID,AD_Language, Name from C_Tax_TRL) tableTRL5 on (table5.C_Tax_ID = tableTRL5.C_Tax_ID and tableTRL5.AD_Language = ?)  left join (select C_Order_Discount_ID, C_Order_ID from C_Order_Discount) table7 on (C_OrderLine.C_Order_Discount_ID = table7.C_Order_Discount_ID) left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table8 on (table7.C_Order_ID = table8.C_Order_ID) left join (select C_Return_Reason_ID, Name from C_Return_Reason) table9 on (C_OrderLine.C_Return_Reason_ID = table9.C_Return_Reason_ID) left join (select M_Warehouse_Rule_ID, Name from M_Warehouse_Rule) table10 on (C_OrderLine.M_Warehouse_Rule_ID = table10.M_Warehouse_Rule_ID) left join (select C_OrderLine_ID, C_Order_ID, Line, LineNetAmt from C_OrderLine) table11 on (C_OrderLine.Quotationline_ID = table11.C_OrderLine_ID) left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table12 on (table11.C_Order_ID = table12.C_Order_ID) left join ad_ref_list_v list1 on (C_OrderLine.Create_Reservation = list1.value and list1.ad_reference_id = '1852D69AB3FD453F8F031813501B26F0' and list1.ad_language = ?)  left join (select C_Project_ID, Value, Name from C_Project) table13 on (C_OrderLine.C_Project_ID = table13.C_Project_ID) left join ad_ref_list_v list2 on (C_OrderLine.SO_Res_Status = list2.value and list2.ad_reference_id = 'C3C19DE8AB3B42E78748E20D986FBBC9' and list2.ad_language = ?)  left join (select C_OrderLine_ID, C_Order_ID, Line, LineNetAmt from C_OrderLine) table14 on (C_OrderLine.BOM_Parent_ID = table14.C_OrderLine_ID) left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table15 on (table14.C_Order_ID = table15.C_Order_ID) left join (select C_Invoice_ID, DocumentNo from C_Invoice) table16 on (C_OrderLine.EM_Atecco_C_Invoice_ID =  table16.C_Invoice_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((cOrderId==null || cOrderId.equals(""))?"":"  AND C_OrderLine.C_Order_ID = ?  ");
    strSql = strSql + 
      "        AND C_OrderLine.C_OrderLine_ID = ? " +
      "        AND C_OrderLine.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND C_OrderLine.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (cOrderId != null && !(cOrderId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LineasPedido590C38406598468A920CEBE984E90910Data objectLineasPedido590C38406598468A920CEBE984E90910Data = new LineasPedido590C38406598468A920CEBE984E90910Data();
        objectLineasPedido590C38406598468A920CEBE984E90910Data.created = UtilSql.getValue(result, "created");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.updated = UtilSql.getValue(result, "updated");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.isactive = UtilSql.getValue(result, "isactive");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cOrderId = UtilSql.getValue(result, "c_order_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.line = UtilSql.getValue(result, "line");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mProductId = UtilSql.getValue(result, "m_product_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mProductIdr = UtilSql.getValue(result, "m_product_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.dateordered = UtilSql.getDateValue(result, "dateordered", "dd-MM-yyyy");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.datepromised = UtilSql.getDateValue(result, "datepromised", "dd-MM-yyyy");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.datedelivered = UtilSql.getDateValue(result, "datedelivered", "dd-MM-yyyy");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.dateinvoiced = UtilSql.getDateValue(result, "dateinvoiced", "dd-MM-yyyy");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.description = UtilSql.getValue(result, "description");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mWarehouseId = UtilSql.getValue(result, "m_warehouse_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.directship = UtilSql.getValue(result, "directship");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cUomIdr = UtilSql.getValue(result, "c_uom_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.qtyordered = UtilSql.getValue(result, "qtyordered");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.qtyreserved = UtilSql.getValue(result, "qtyreserved");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.qtydelivered = UtilSql.getValue(result, "qtydelivered");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.qtyinvoiced = UtilSql.getValue(result, "qtyinvoiced");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mShipperId = UtilSql.getValue(result, "m_shipper_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.pricelist = UtilSql.getValue(result, "pricelist");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.priceactual = UtilSql.getValue(result, "priceactual");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.pricelimit = UtilSql.getValue(result, "pricelimit");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.linenetamt = UtilSql.getValue(result, "linenetamt");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.discount = UtilSql.getValue(result, "discount");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.freightamt = UtilSql.getValue(result, "freightamt");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cChargeId = UtilSql.getValue(result, "c_charge_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.chargeamt = UtilSql.getValue(result, "chargeamt");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cTaxId = UtilSql.getValue(result, "c_tax_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cTaxIdr = UtilSql.getValue(result, "c_tax_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.sResourceassignmentId = UtilSql.getValue(result, "s_resourceassignment_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.refOrderlineId = UtilSql.getValue(result, "ref_orderline_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mAttributesetinstanceId = UtilSql.getValue(result, "m_attributesetinstance_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.isdescription = UtilSql.getValue(result, "isdescription");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.quantityorder = UtilSql.getValue(result, "quantityorder");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mProductUomId = UtilSql.getValue(result, "m_product_uom_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mOfferId = UtilSql.getValue(result, "m_offer_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.pricestd = UtilSql.getValue(result, "pricestd");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cancelpricead = UtilSql.getValue(result, "cancelpricead");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cOrderDiscountId = UtilSql.getValue(result, "c_order_discount_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cOrderDiscountIdr = UtilSql.getValue(result, "c_order_discount_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.iseditlinenetamt = UtilSql.getValue(result, "iseditlinenetamt");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.taxbaseamt = UtilSql.getValue(result, "taxbaseamt");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mInoutlineId = UtilSql.getValue(result, "m_inoutline_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cReturnReasonId = UtilSql.getValue(result, "c_return_reason_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cReturnReasonIdr = UtilSql.getValue(result, "c_return_reason_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.grossUnitPrice = UtilSql.getValue(result, "gross_unit_price");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.lineGrossAmount = UtilSql.getValue(result, "line_gross_amount");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.grosspricelist = UtilSql.getValue(result, "grosspricelist");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cCostcenterId = UtilSql.getValue(result, "c_costcenter_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.grosspricestd = UtilSql.getValue(result, "grosspricestd");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.aAssetId = UtilSql.getValue(result, "a_asset_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mWarehouseRuleId = UtilSql.getValue(result, "m_warehouse_rule_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.mWarehouseRuleIdr = UtilSql.getValue(result, "m_warehouse_rule_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.user1Id = UtilSql.getValue(result, "user1_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.quotationlineId = UtilSql.getValue(result, "quotationline_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.quotationlineIdr = UtilSql.getValue(result, "quotationline_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.user2Id = UtilSql.getValue(result, "user2_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.createReservation = UtilSql.getValue(result, "create_reservation");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.createReservationr = UtilSql.getValue(result, "create_reservationr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cProjectIdr = UtilSql.getValue(result, "c_project_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.soResStatus = UtilSql.getValue(result, "so_res_status");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.soResStatusr = UtilSql.getValue(result, "so_res_statusr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.manageReservation = UtilSql.getValue(result, "manage_reservation");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.managePrereservation = UtilSql.getValue(result, "manage_prereservation");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.explode = UtilSql.getValue(result, "explode");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.bomParentId = UtilSql.getValue(result, "bom_parent_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.bomParentIdr = UtilSql.getValue(result, "bom_parent_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.printDescription = UtilSql.getValue(result, "print_description");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.overdueReturnDays = UtilSql.getValue(result, "overdue_return_days");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.relateOrderline = UtilSql.getValue(result, "relate_orderline");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.emAteccoStocktienda = UtilSql.getValue(result, "em_atecco_stocktienda");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.emAteccoStocktablero = UtilSql.getValue(result, "em_atecco_stocktablero");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.emAteccoPrecioefectivo = UtilSql.getValue(result, "em_atecco_precioefectivo");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.emAteccoTotalefectivo = UtilSql.getValue(result, "em_atecco_totalefectivo");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.emAteccoPreciotarjeta = UtilSql.getValue(result, "em_atecco_preciotarjeta");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.emAteccoTotaltarjeta = UtilSql.getValue(result, "em_atecco_totaltarjeta");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.emAteccoCInvoiceId = UtilSql.getValue(result, "em_atecco_c_invoice_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.emAteccoCInvoiceIdr = UtilSql.getValue(result, "em_atecco_c_invoice_idr");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.cOrderlineId = UtilSql.getValue(result, "c_orderline_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.language = UtilSql.getValue(result, "language");
        objectLineasPedido590C38406598468A920CEBE984E90910Data.adUserClient = "";
        objectLineasPedido590C38406598468A920CEBE984E90910Data.adOrgClient = "";
        objectLineasPedido590C38406598468A920CEBE984E90910Data.createdby = "";
        objectLineasPedido590C38406598468A920CEBE984E90910Data.trBgcolor = "";
        objectLineasPedido590C38406598468A920CEBE984E90910Data.totalCount = "";
        objectLineasPedido590C38406598468A920CEBE984E90910Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLineasPedido590C38406598468A920CEBE984E90910Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LineasPedido590C38406598468A920CEBE984E90910Data objectLineasPedido590C38406598468A920CEBE984E90910Data[] = new LineasPedido590C38406598468A920CEBE984E90910Data[vector.size()];
    vector.copyInto(objectLineasPedido590C38406598468A920CEBE984E90910Data);
    return(objectLineasPedido590C38406598468A920CEBE984E90910Data);
  }

/**
Create a registry
 */
  public static LineasPedido590C38406598468A920CEBE984E90910Data[] set(String cOrderId, String lineGrossAmount, String cancelpricead, String cOrderlineId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String line, String dateordered, String datepromised, String datedelivered, String dateinvoiced, String description, String mProductId, String mProductIdr, String cUomId, String mWarehouseId, String qtyordered, String qtyreserved, String qtydelivered, String qtyinvoiced, String mShipperId, String cCurrencyId, String pricelist, String priceactual, String cTaxId, String cBpartnerId, String directship, String freightamt, String cChargeId, String chargeamt, String emAteccoTotaltarjeta, String relateOrderline, String cBpartnerLocationId, String linenetamt, String pricelimit, String discount, String cCostcenterId, String emAteccoPrecioefectivo, String printDescription, String emAteccoTotalefectivo, String cReturnReasonId, String sResourceassignmentId, String cOrderDiscountId, String emAteccoCInvoiceId, String user2Id, String aAssetId, String overdueReturnDays, String refOrderlineId, String iseditlinenetamt, String taxbaseamt, String mProductUomId, String quantityorder, String mOfferId, String pricestd, String mAttributesetinstanceId, String user1Id, String isdescription, String bomParentId, String bomParentIdr, String grosspricestd, String explode, String mInoutlineId, String cProjectId, String cProjectIdr, String grosspricelist, String mWarehouseRuleId, String createReservation, String soResStatus, String manageReservation, String managePrereservation, String emAteccoPreciotarjeta, String quotationlineId, String quotationlineIdr, String grossUnitPrice, String emAteccoStocktablero, String emAteccoStocktienda)    throws ServletException {
    LineasPedido590C38406598468A920CEBE984E90910Data objectLineasPedido590C38406598468A920CEBE984E90910Data[] = new LineasPedido590C38406598468A920CEBE984E90910Data[1];
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0] = new LineasPedido590C38406598468A920CEBE984E90910Data();
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].created = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].createdbyr = createdbyr;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].updated = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].updatedTimeStamp = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].updatedby = updatedby;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].updatedbyr = updatedbyr;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].adOrgId = adOrgId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].isactive = isactive;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cOrderId = cOrderId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].line = line;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cBpartnerId = cBpartnerId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mProductId = mProductId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mProductIdr = mProductIdr;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cBpartnerLocationId = cBpartnerLocationId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].dateordered = dateordered;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].datepromised = datepromised;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].datedelivered = datedelivered;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].dateinvoiced = dateinvoiced;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].description = description;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mWarehouseId = mWarehouseId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].directship = directship;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cUomId = cUomId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cUomIdr = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].qtyordered = qtyordered;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].qtyreserved = qtyreserved;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].qtydelivered = qtydelivered;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].qtyinvoiced = qtyinvoiced;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mShipperId = mShipperId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cCurrencyId = cCurrencyId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].pricelist = pricelist;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].priceactual = priceactual;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].pricelimit = pricelimit;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].linenetamt = linenetamt;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].discount = discount;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].freightamt = freightamt;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cChargeId = cChargeId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].chargeamt = chargeamt;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cTaxId = cTaxId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cTaxIdr = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].sResourceassignmentId = sResourceassignmentId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].refOrderlineId = refOrderlineId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mAttributesetinstanceId = mAttributesetinstanceId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].isdescription = isdescription;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].quantityorder = quantityorder;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mProductUomId = mProductUomId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mOfferId = mOfferId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].pricestd = pricestd;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cancelpricead = cancelpricead;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cOrderDiscountId = cOrderDiscountId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cOrderDiscountIdr = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].iseditlinenetamt = iseditlinenetamt;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].taxbaseamt = taxbaseamt;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mInoutlineId = mInoutlineId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cReturnReasonId = cReturnReasonId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cReturnReasonIdr = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].grossUnitPrice = grossUnitPrice;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].lineGrossAmount = lineGrossAmount;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].grosspricelist = grosspricelist;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cCostcenterId = cCostcenterId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].grosspricestd = grosspricestd;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].aAssetId = aAssetId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mWarehouseRuleId = mWarehouseRuleId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].mWarehouseRuleIdr = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].user1Id = user1Id;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].quotationlineId = quotationlineId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].quotationlineIdr = quotationlineIdr;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].user2Id = user2Id;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].createReservation = createReservation;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].createReservationr = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cProjectId = cProjectId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cProjectIdr = cProjectIdr;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].soResStatus = soResStatus;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].soResStatusr = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].manageReservation = manageReservation;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].managePrereservation = managePrereservation;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].explode = explode;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].bomParentId = bomParentId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].bomParentIdr = bomParentIdr;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].printDescription = printDescription;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].overdueReturnDays = overdueReturnDays;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].relateOrderline = relateOrderline;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].emAteccoStocktienda = emAteccoStocktienda;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].emAteccoStocktablero = emAteccoStocktablero;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].emAteccoPrecioefectivo = emAteccoPrecioefectivo;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].emAteccoTotalefectivo = emAteccoTotalefectivo;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].emAteccoPreciotarjeta = emAteccoPreciotarjeta;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].emAteccoTotaltarjeta = emAteccoTotaltarjeta;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].emAteccoCInvoiceId = emAteccoCInvoiceId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].emAteccoCInvoiceIdr = "";
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].cOrderlineId = cOrderlineId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].adClientId = adClientId;
    objectLineasPedido590C38406598468A920CEBE984E90910Data[0].language = "";
    return objectLineasPedido590C38406598468A920CEBE984E90910Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2210_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2212_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2214(ConnectionProvider connectionProvider, String C_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(MAX(Line),0)+10 AS DefaultValue FROM C_OrderLine WHERE C_Order_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2221_2(ConnectionProvider connectionProvider, String paramLanguage, String M_Product_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))), '') ) as M_Product_ID FROM M_Product left join (select M_Product_ID, Name from M_Product) table2 on (M_Product.M_Product_ID = table2.M_Product_ID)left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL2 on (table2.M_Product_ID = tableTRL2.M_Product_ID and tableTRL2.AD_Language = ?)  WHERE M_Product.isActive='Y' AND M_Product.M_Product_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_Product_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_product_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2764(ConnectionProvider connectionProvider, String C_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT C_BPartner_ID AS DefaultValue FROM C_Order WHERE C_Order_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef9E6D689864154F91B98ACB746EB9C507_3(ConnectionProvider connectionProvider, String BOM_Parent_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.DocumentNo), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table3.DateOrdered, 'DD-MM-YYYY')), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.GrandTotal), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Line), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.LineNetAmt), ''))), '') ) as BOM_Parent_ID FROM C_OrderLine left join (select C_OrderLine_ID, C_Order_ID, Line, LineNetAmt from C_OrderLine) table2 on (C_OrderLine.C_OrderLine_ID = table2.C_OrderLine_ID)left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table3 on (table2.C_Order_ID = table3.C_Order_ID) WHERE C_OrderLine.isActive='Y' AND C_OrderLine.C_OrderLine_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, BOM_Parent_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "bom_parent_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefB2023461A08649D58E5D144D4FC2C507_4(ConnectionProvider connectionProvider, String C_Project_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Value), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Project_ID FROM C_Project left join (select C_Project_ID, Value, Name from C_Project) table2 on (C_Project.C_Project_ID = table2.C_Project_ID) WHERE C_Project.isActive='Y' AND C_Project.C_Project_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Project_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_project_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefE9612846B6F5472A8F64CF729E0B25AE_5(ConnectionProvider connectionProvider, String Quotationline_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.DocumentNo), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table3.DateOrdered, 'DD-MM-YYYY')), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.GrandTotal), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Line), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.LineNetAmt), ''))), '') ) as Quotationline_ID FROM C_OrderLine left join (select C_OrderLine_ID, C_Order_ID, Line, LineNetAmt from C_OrderLine) table2 on (C_OrderLine.C_OrderLine_ID = table2.C_OrderLine_ID)left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table3 on (table2.C_Order_ID = table3.C_Order_ID) WHERE C_OrderLine.isActive='Y' AND C_OrderLine.C_OrderLine_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Quotationline_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "quotationline_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT C_OrderLine.C_Order_ID AS NAME" +
      "        FROM C_OrderLine" +
      "        WHERE C_OrderLine.C_OrderLine_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateOrdered, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Order left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table1 on (C_Order.C_Order_ID = table1.C_Order_ID) WHERE C_Order.C_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateOrdered, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Order left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table1 on (C_Order.C_Order_ID = table1.C_Order_ID) WHERE C_Order.C_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_OrderLine" +
      "        SET AD_Org_ID = (?) , IsActive = (?) , C_Order_ID = (?) , Line = TO_NUMBER(?) , C_BPartner_ID = (?) , M_Product_ID = (?) , C_BPartner_Location_ID = (?) , DateOrdered = TO_DATE(?) , DatePromised = TO_DATE(?) , DateDelivered = TO_DATE(?) , DateInvoiced = TO_DATE(?) , Description = (?) , M_Warehouse_ID = (?) , DirectShip = (?) , C_UOM_ID = (?) , QtyOrdered = TO_NUMBER(?) , QtyReserved = TO_NUMBER(?) , QtyDelivered = TO_NUMBER(?) , QtyInvoiced = TO_NUMBER(?) , M_Shipper_ID = (?) , C_Currency_ID = (?) , PriceList = TO_NUMBER(?) , PriceActual = TO_NUMBER(?) , PriceLimit = TO_NUMBER(?) , LineNetAmt = TO_NUMBER(?) , Discount = TO_NUMBER(?) , FreightAmt = TO_NUMBER(?) , C_Charge_ID = (?) , ChargeAmt = TO_NUMBER(?) , C_Tax_ID = (?) , S_ResourceAssignment_ID = (?) , Ref_OrderLine_ID = (?) , M_AttributeSetInstance_ID = (?) , IsDescription = (?) , QuantityOrder = TO_NUMBER(?) , M_Product_Uom_Id = (?) , M_Offer_ID = (?) , PriceStd = TO_NUMBER(?) , CANCELPRICEAD = (?) , C_Order_Discount_ID = (?) , Iseditlinenetamt = (?) , Taxbaseamt = TO_NUMBER(?) , M_Inoutline_ID = (?) , C_Return_Reason_ID = (?) , Gross_Unit_Price = TO_NUMBER(?) , Line_Gross_Amount = TO_NUMBER(?) , GrossPriceList = TO_NUMBER(?) , C_Costcenter_ID = (?) , grosspricestd = TO_NUMBER(?) , A_Asset_ID = (?) , M_Warehouse_Rule_ID = (?) , User1_ID = (?) , Quotationline_ID = (?) , User2_ID = (?) , Create_Reservation = (?) , C_Project_ID = (?) , SO_Res_Status = (?) , Manage_Reservation = (?) , Manage_Prereservation = (?) , Explode = (?) , BOM_Parent_ID = (?) , Print_Description = (?) , Overdue_Return_Days = TO_NUMBER(?) , Relate_Orderline = (?) , EM_Atecco_Stocktienda = TO_NUMBER(?) , EM_Atecco_Stocktablero = TO_NUMBER(?) , EM_Atecco_Precioefectivo = TO_NUMBER(?) , EM_Atecco_Totalefectivo = TO_NUMBER(?) , EM_Atecco_Preciotarjeta = TO_NUMBER(?) , EM_Atecco_Totaltarjeta = TO_NUMBER(?) , EM_Atecco_C_Invoice_ID = (?) , C_OrderLine_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE C_OrderLine.C_OrderLine_ID = ? " +
      "                 AND C_OrderLine.C_Order_ID = ? " +
      "        AND C_OrderLine.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_OrderLine.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datedelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, directship);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyreserved);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtydelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricelist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priceactual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricelimit);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, linenetamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceassignmentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, refOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityorder);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mOfferId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricestd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cancelpricead);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderDiscountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iseditlinenetamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, taxbaseamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grossUnitPrice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lineGrossAmount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grosspricelist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grosspricestd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseRuleId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createReservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, manageReservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managePrereservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, explode);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bomParentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateOrderline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoStocktienda);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoStocktablero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPrecioefectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTotalefectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPreciotarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTotaltarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO C_OrderLine " +
      "        (AD_Org_ID, IsActive, C_Order_ID, Line, C_BPartner_ID, M_Product_ID, C_BPartner_Location_ID, DateOrdered, DatePromised, DateDelivered, DateInvoiced, Description, M_Warehouse_ID, DirectShip, C_UOM_ID, QtyOrdered, QtyReserved, QtyDelivered, QtyInvoiced, M_Shipper_ID, C_Currency_ID, PriceList, PriceActual, PriceLimit, LineNetAmt, Discount, FreightAmt, C_Charge_ID, ChargeAmt, C_Tax_ID, S_ResourceAssignment_ID, Ref_OrderLine_ID, M_AttributeSetInstance_ID, IsDescription, QuantityOrder, M_Product_Uom_Id, M_Offer_ID, PriceStd, CANCELPRICEAD, C_Order_Discount_ID, Iseditlinenetamt, Taxbaseamt, M_Inoutline_ID, C_Return_Reason_ID, Gross_Unit_Price, Line_Gross_Amount, GrossPriceList, C_Costcenter_ID, grosspricestd, A_Asset_ID, M_Warehouse_Rule_ID, User1_ID, Quotationline_ID, User2_ID, Create_Reservation, C_Project_ID, SO_Res_Status, Manage_Reservation, Manage_Prereservation, Explode, BOM_Parent_ID, Print_Description, Overdue_Return_Days, Relate_Orderline, EM_Atecco_Stocktienda, EM_Atecco_Stocktablero, EM_Atecco_Precioefectivo, EM_Atecco_Totalefectivo, EM_Atecco_Preciotarjeta, EM_Atecco_Totaltarjeta, EM_Atecco_C_Invoice_ID, C_OrderLine_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_NUMBER(?), (?), (?), (?), TO_DATE(?), TO_DATE(?), TO_DATE(?), TO_DATE(?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datedelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, directship);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyreserved);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtydelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricelist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priceactual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricelimit);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, linenetamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceassignmentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, refOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityorder);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mOfferId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricestd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cancelpricead);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderDiscountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iseditlinenetamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, taxbaseamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grossUnitPrice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lineGrossAmount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grosspricelist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grosspricestd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseRuleId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createReservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, manageReservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managePrereservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, explode);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bomParentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateOrderline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoStocktienda);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoStocktablero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPrecioefectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTotalefectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPreciotarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTotaltarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String cOrderId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM C_OrderLine" +
      "        WHERE C_OrderLine.C_OrderLine_ID = ? " +
      "                 AND C_OrderLine.C_Order_ID = ? " +
      "        AND C_OrderLine.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_OrderLine.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM C_OrderLine" +
      "         WHERE C_OrderLine.C_OrderLine_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM C_OrderLine" +
      "         WHERE C_OrderLine.C_OrderLine_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}

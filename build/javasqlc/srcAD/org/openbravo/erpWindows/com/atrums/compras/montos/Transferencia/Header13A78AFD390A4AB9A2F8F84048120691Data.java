//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.Transferencia;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Header13A78AFD390A4AB9A2F8F84048120691Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Header13A78AFD390A4AB9A2F8F84048120691Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String fechaTransferencia;
  public String isactive;
  public String amount;
  public String finFinancialAccountId;
  public String finFinancialAccountIdr;
  public String finFinancialAccounttoId;
  public String finFinancialAccounttoIdr;
  public String cGlitemId;
  public String cGlitemIdr;
  public String description;
  public String finPaymentId;
  public String finPaymentIdr;
  public String finPaymenttoId;
  public String finPaymenttoIdr;
  public String transDocstatus;
  public String transProcesar;
  public String transProcesarBtn;
  public String adClientId;
  public String ateccoTransferenciaId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("fecha_transferencia") || fieldName.equals("fechaTransferencia"))
      return fechaTransferencia;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("amount"))
      return amount;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_id") || fieldName.equals("finFinancialAccountId"))
      return finFinancialAccountId;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_idr") || fieldName.equals("finFinancialAccountIdr"))
      return finFinancialAccountIdr;
    else if (fieldName.equalsIgnoreCase("fin_financial_accountto_id") || fieldName.equals("finFinancialAccounttoId"))
      return finFinancialAccounttoId;
    else if (fieldName.equalsIgnoreCase("fin_financial_accountto_idr") || fieldName.equals("finFinancialAccounttoIdr"))
      return finFinancialAccounttoIdr;
    else if (fieldName.equalsIgnoreCase("c_glitem_id") || fieldName.equals("cGlitemId"))
      return cGlitemId;
    else if (fieldName.equalsIgnoreCase("c_glitem_idr") || fieldName.equals("cGlitemIdr"))
      return cGlitemIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("fin_payment_id") || fieldName.equals("finPaymentId"))
      return finPaymentId;
    else if (fieldName.equalsIgnoreCase("fin_payment_idr") || fieldName.equals("finPaymentIdr"))
      return finPaymentIdr;
    else if (fieldName.equalsIgnoreCase("fin_paymentto_id") || fieldName.equals("finPaymenttoId"))
      return finPaymenttoId;
    else if (fieldName.equalsIgnoreCase("fin_paymentto_idr") || fieldName.equals("finPaymenttoIdr"))
      return finPaymenttoIdr;
    else if (fieldName.equalsIgnoreCase("trans_docstatus") || fieldName.equals("transDocstatus"))
      return transDocstatus;
    else if (fieldName.equalsIgnoreCase("trans_procesar") || fieldName.equals("transProcesar"))
      return transProcesar;
    else if (fieldName.equalsIgnoreCase("trans_procesar_btn") || fieldName.equals("transProcesarBtn"))
      return transProcesarBtn;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("atecco_transferencia_id") || fieldName.equals("ateccoTransferenciaId"))
      return ateccoTransferenciaId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Header13A78AFD390A4AB9A2F8F84048120691Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Header13A78AFD390A4AB9A2F8F84048120691Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(atecco_transferencia.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atecco_transferencia.CreatedBy) as CreatedByR, " +
      "        to_char(atecco_transferencia.Updated, ?) as updated, " +
      "        to_char(atecco_transferencia.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        atecco_transferencia.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atecco_transferencia.UpdatedBy) as UpdatedByR," +
      "        atecco_transferencia.AD_Org_ID, " +
      "(CASE WHEN atecco_transferencia.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "atecco_transferencia.Fecha_Transferencia, " +
      "COALESCE(atecco_transferencia.Isactive, 'N') AS Isactive, " +
      "atecco_transferencia.Amount, " +
      "atecco_transferencia.FIN_Financial_Account_ID, " +
      "(CASE WHEN atecco_transferencia.FIN_Financial_Account_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.ISO_Code), ''))),'') ) END) AS FIN_Financial_Account_IDR, " +
      "atecco_transferencia.FIN_Financial_Accountto_ID, " +
      "(CASE WHEN atecco_transferencia.FIN_Financial_Accountto_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS FIN_Financial_Accountto_IDR, " +
      "atecco_transferencia.C_Glitem_ID, " +
      "(CASE WHEN atecco_transferencia.C_Glitem_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS C_Glitem_IDR, " +
      "atecco_transferencia.Description, " +
      "atecco_transferencia.FIN_Payment_ID, " +
      "(CASE WHEN atecco_transferencia.FIN_Payment_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.DocumentNo), ''))),'') ) END) AS FIN_Payment_IDR, " +
      "atecco_transferencia.FIN_Paymentto_ID, " +
      "(CASE WHEN atecco_transferencia.FIN_Paymentto_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.DocumentNo), ''))),'') ) END) AS FIN_Paymentto_IDR, " +
      "atecco_transferencia.Trans_Docstatus, " +
      "atecco_transferencia.Trans_Procesar, " +
      "list1.name as Trans_Procesar_BTN, " +
      "atecco_transferencia.AD_Client_ID, " +
      "atecco_transferencia.Atecco_Transferencia_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM atecco_transferencia left join (select AD_Org_ID, Name from AD_Org) table1 on (atecco_transferencia.AD_Org_ID = table1.AD_Org_ID) left join (select FIN_Financial_Account_ID, Name, C_Currency_ID from FIN_Financial_Account) table2 on (atecco_transferencia.FIN_Financial_Account_ID = table2.FIN_Financial_Account_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table3 on (table2.C_Currency_ID = table3.C_Currency_ID) left join (select Fin_Financial_Account_ID, Name from FIN_Financial_Account) table4 on (atecco_transferencia.FIN_Financial_Accountto_ID =  table4.Fin_Financial_Account_ID) left join (select C_Glitem_ID, Name from C_Glitem) table5 on (atecco_transferencia.C_Glitem_ID = table5.C_Glitem_ID) left join (select Fin_Payment_ID, DocumentNo from FIN_Payment) table6 on (atecco_transferencia.FIN_Payment_ID =  table6.Fin_Payment_ID) left join (select Fin_Payment_ID, DocumentNo from FIN_Payment) table7 on (atecco_transferencia.FIN_Paymentto_ID =  table7.Fin_Payment_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = '90723DBD73334CF8960543A849A3038B' and list1.ad_language = ?  AND atecco_transferencia.Trans_Procesar = TO_CHAR(list1.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND atecco_transferencia.Atecco_Transferencia_ID = ? " +
      "        AND atecco_transferencia.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND atecco_transferencia.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Header13A78AFD390A4AB9A2F8F84048120691Data objectHeader13A78AFD390A4AB9A2F8F84048120691Data = new Header13A78AFD390A4AB9A2F8F84048120691Data();
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.created = UtilSql.getValue(result, "created");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.updated = UtilSql.getValue(result, "updated");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.fechaTransferencia = UtilSql.getDateValue(result, "fecha_transferencia", "dd-MM-yyyy");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.isactive = UtilSql.getValue(result, "isactive");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.amount = UtilSql.getValue(result, "amount");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.finFinancialAccountId = UtilSql.getValue(result, "fin_financial_account_id");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.finFinancialAccountIdr = UtilSql.getValue(result, "fin_financial_account_idr");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.finFinancialAccounttoId = UtilSql.getValue(result, "fin_financial_accountto_id");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.finFinancialAccounttoIdr = UtilSql.getValue(result, "fin_financial_accountto_idr");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.cGlitemId = UtilSql.getValue(result, "c_glitem_id");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.cGlitemIdr = UtilSql.getValue(result, "c_glitem_idr");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.description = UtilSql.getValue(result, "description");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.finPaymentId = UtilSql.getValue(result, "fin_payment_id");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.finPaymentIdr = UtilSql.getValue(result, "fin_payment_idr");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.finPaymenttoId = UtilSql.getValue(result, "fin_paymentto_id");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.finPaymenttoIdr = UtilSql.getValue(result, "fin_paymentto_idr");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.transDocstatus = UtilSql.getValue(result, "trans_docstatus");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.transProcesar = UtilSql.getValue(result, "trans_procesar");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.transProcesarBtn = UtilSql.getValue(result, "trans_procesar_btn");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.ateccoTransferenciaId = UtilSql.getValue(result, "atecco_transferencia_id");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.language = UtilSql.getValue(result, "language");
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.adUserClient = "";
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.adOrgClient = "";
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.createdby = "";
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.trBgcolor = "";
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.totalCount = "";
        objectHeader13A78AFD390A4AB9A2F8F84048120691Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectHeader13A78AFD390A4AB9A2F8F84048120691Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Header13A78AFD390A4AB9A2F8F84048120691Data objectHeader13A78AFD390A4AB9A2F8F84048120691Data[] = new Header13A78AFD390A4AB9A2F8F84048120691Data[vector.size()];
    vector.copyInto(objectHeader13A78AFD390A4AB9A2F8F84048120691Data);
    return(objectHeader13A78AFD390A4AB9A2F8F84048120691Data);
  }

/**
Create a registry
 */
  public static Header13A78AFD390A4AB9A2F8F84048120691Data[] set(String ateccoTransferenciaId, String adOrgId, String description, String cGlitemId, String transProcesar, String transProcesarBtn, String finFinancialAccountId, String createdby, String createdbyr, String finFinancialAccounttoId, String adClientId, String updatedby, String updatedbyr, String amount, String finPaymentId, String transDocstatus, String fechaTransferencia, String finPaymenttoId, String isactive)    throws ServletException {
    Header13A78AFD390A4AB9A2F8F84048120691Data objectHeader13A78AFD390A4AB9A2F8F84048120691Data[] = new Header13A78AFD390A4AB9A2F8F84048120691Data[1];
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0] = new Header13A78AFD390A4AB9A2F8F84048120691Data();
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].created = "";
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].createdbyr = createdbyr;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].updated = "";
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].updatedTimeStamp = "";
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].updatedby = updatedby;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].updatedbyr = updatedbyr;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].adOrgId = adOrgId;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].adOrgIdr = "";
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].fechaTransferencia = fechaTransferencia;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].isactive = isactive;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].amount = amount;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].finFinancialAccountId = finFinancialAccountId;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].finFinancialAccountIdr = "";
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].finFinancialAccounttoId = finFinancialAccounttoId;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].finFinancialAccounttoIdr = "";
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].cGlitemId = cGlitemId;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].cGlitemIdr = "";
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].description = description;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].finPaymentId = finPaymentId;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].finPaymentIdr = "";
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].finPaymenttoId = finPaymenttoId;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].finPaymenttoIdr = "";
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].transDocstatus = transDocstatus;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].transProcesar = transProcesar;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].transProcesarBtn = transProcesarBtn;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].adClientId = adClientId;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].ateccoTransferenciaId = ateccoTransferenciaId;
    objectHeader13A78AFD390A4AB9A2F8F84048120691Data[0].language = "";
    return objectHeader13A78AFD390A4AB9A2F8F84048120691Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef72892083F6E4444CB7F7DC1AAD99CCAD_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef9ACDD3C119B842D4A853461A75E37073_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE atecco_transferencia" +
      "        SET AD_Org_ID = (?) , Fecha_Transferencia = TO_DATE(?) , Isactive = (?) , Amount = TO_NUMBER(?) , FIN_Financial_Account_ID = (?) , FIN_Financial_Accountto_ID = (?) , C_Glitem_ID = (?) , Description = (?) , FIN_Payment_ID = (?) , FIN_Paymentto_ID = (?) , Trans_Docstatus = (?) , Trans_Procesar = (?) , AD_Client_ID = (?) , Atecco_Transferencia_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE atecco_transferencia.Atecco_Transferencia_ID = ? " +
      "        AND atecco_transferencia.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atecco_transferencia.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaTransferencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccounttoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymenttoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, transDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, transProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoTransferenciaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoTransferenciaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO atecco_transferencia " +
      "        (AD_Org_ID, Fecha_Transferencia, Isactive, Amount, FIN_Financial_Account_ID, FIN_Financial_Accountto_ID, C_Glitem_ID, Description, FIN_Payment_ID, FIN_Paymentto_ID, Trans_Docstatus, Trans_Procesar, AD_Client_ID, Atecco_Transferencia_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), TO_DATE(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaTransferencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccounttoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymenttoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, transDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, transProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoTransferenciaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM atecco_transferencia" +
      "        WHERE atecco_transferencia.Atecco_Transferencia_ID = ? " +
      "        AND atecco_transferencia.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atecco_transferencia.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM atecco_transferencia" +
      "         WHERE atecco_transferencia.Atecco_Transferencia_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM atecco_transferencia" +
      "         WHERE atecco_transferencia.Atecco_Transferencia_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
